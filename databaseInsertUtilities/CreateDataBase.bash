#!/bin/bash

DBName="test"

echo "Dropping previous DB if exists"

echo "DROP DATABASE IF EXISTS $DBName;" | mysql -u root

echo "Creating new DB"

echo "CREATE DATABASE $DBName;" | mysql -u root

echo "Granting privileges"

echo "GRANT ALL PRIVILEGES ON test.* to testuser@localhost;" | mysql -u root

echo "SET PASSWORD FOR testuser@localhost = password('TestPasswd9');" | mysql -u root


echo "Creating MentoringProgram (ProgramID, Start, End, RegDeadline)"

echo "CREATE TABLE $DBName.MentoringProgram (
	ProgramID INT NOT NULL PRIMARY KEY,
	Start DATE,
	End DATE,
	RegDeadline DATE
);" | mysql -u root

#												!!!!!NOT NEEDED NOW!!!!!
echo "Creating File (FileID, Link)"

echo "CREATE TABLE $DBName.File (
	FileID INT NOT NULL,
	Link VARCHAR(100) NOT NULL,

	PRIMARY KEY (FileID, Link)
);" | mysql -u root

echo "Creating Role (ID, Name, Description) "

echo "CREATE TABLE $DBName.Role (
	RoleID INT NOT NULL PRIMARY KEY,
	Name VARCHAR(20),
	Description MEDIUMTEXT
);" | mysql -u root


echo "Creating User section..."


echo "Creating User (UserID, Name, SurName, Sex, PhotoURL, BirthDate, University, Phone, Email, IsConfirmed)"

echo "CREATE TABLE $DBName.User (
	UserID INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
	Name VARCHAR(40),
	SurName VARCHAR(40),
	Sex VARCHAR(10),
	PhotoURL VARCHAR(100) NOT NULL DEFAULT 'images/profilePhoto/NoPhoto.jpeg',
	BirthDate DATE,
	University TINYTEXT,
	Phone VARCHAR(18),
	Job VARCHAR(20),
	Email VARCHAR(45),
	IsConfirmed BOOL NOT NULL DEFAULT false
);" | mysql -u root

echo "Creating PasswordHash (UserID, Hash)"

echo "CREATE TABLE $DBName.PasswordHash (
	UserID INT NOT NULL PRIMARY KEY,
	Salt VARCHAR(10),
	Hash VARCHAR(32),

	FOREIGN KEY (UserID) REFERENCES $DBName.User(UserID)
	ON DELETE CASCADE ON UPDATE CASCADE
);" | mysql -u root

echo "Creating Application (UserID, ProfessionalInterests, Hobbies, About) "

echo "CREATE TABLE $DBName.Application (
	UserID INT NOT NULL PRIMARY KEY,
	ProfessionalInterests MEDIUMTEXT,
	Hobbies MEDIUMTEXT,
	About LONGTEXT,

	FOREIGN KEY (UserID) REFERENCES $DBName.User(UserID)
	ON DELETE CASCADE ON UPDATE CASCADE
);" | mysql -u root

echo "Creating MentorApplication (UserID, PreferredUniversity, PreferredSex, PreferredInterests, LectureWish, ReadyForTwo)"

echo "CREATE TABLE $DBName.MentorApplication (
	UserID INT NOT NULL PRIMARY KEY,
	PreferredUniversity TINYTEXT,
	PreferredSex TINYTEXT,
	PreferredInterests MEDIUMTEXT,
	LectureWish MEDIUMTEXT,
	ReadyForTwo BOOL,

	FOREIGN KEY (UserID) REFERENCES $DBName.User(UserID)
	ON DELETE CASCADE ON UPDATE CASCADE
	);" | mysql -u root

echo "Creating ProgramMentorMenteeMap (MentorID, MenteeID, ProgramID)"

echo "CREATE TABLE $DBName.ProgramMentorMenteeMap (
	ProgramID INT NOT NULL,
	MentorID INT NOT NULL,
	MenteeID INT NOT NULL,

	PRIMARY KEY (MentorID, MenteeID, ProgramID),

	FOREIGN KEY (MentorID) REFERENCES $DBName.User(UserID)
	ON DELETE CASCADE ON UPDATE CASCADE,
	FOREIGN KEY (MenteeID) REFERENCES $DBName.User(UserID)
	ON DELETE CASCADE ON UPDATE CASCADE,
	FOREIGN KEY (ProgramID) REFERENCES $DBName.MentoringProgram(ProgramID)
	ON DELETE CASCADE ON UPDATE CASCADE
);" | mysql -u root

echo "Creating ProgramUserRoleMap (ProgramID, UserID, RoleID, IsActive)"

echo "CREATE TABLE $DBName.ProgramUserRoleMap (
	ProgramID INT NOT NULL,
	UserID INT NOT NULL,
	RoleID INT NOT NULL,

	PRIMARY KEY (ProgramID, UserID, RoleID),

	FOREIGN KEY (ProgramID) REFERENCES $DBName.MentoringProgram(ProgramID)
	ON DELETE CASCADE ON UPDATE CASCADE,
	FOREIGN KEY (UserID) REFERENCES $DBName.User(UserID)
	ON DELETE CASCADE ON UPDATE CASCADE,
	FOREIGN KEY (RoleID) REFERENCES $DBName.Role(RoleID)
	ON DELETE CASCADE ON UPDATE CASCADE
);" | mysql -u root


#												!!!!!NOT NEEDED NOW SECTION!!!!!
echo "Creating Event section..."


echo "Creating Event (EventID, Date, Subject, FromID, Body, LectorID, ImageURL, VoteJSON, ProgramID, EventType)"

echo "CREATE TABLE $DBName.Event (
	EventID INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
	Date DATE,
	AddDate TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	Subject TINYTEXT,
	FromID INT NOT NULL,
	Body LONGTEXT,
	LectorID INT NULL,
	ImageURL VARCHAR(100),
	VoteJSON VARCHAR(100),
	ProgramID INT NOT NULL,
	EventType INT NOT NULL,
	Pinned BOOL DEFAULT 0,

	FOREIGN KEY (FromID) REFERENCES $DBName.User(UserID)
	ON DELETE CASCADE ON UPDATE CASCADE,
	FOREIGN KEY (ProgramID) REFERENCES $DBName.MentoringProgram(ProgramID)
	ON DELETE CASCADE ON UPDATE CASCADE 
);" | mysql -u root

echo "Creating triggers section"


echo "DELIMITER |
	CREATE TRIGGER $DBName.insert_nulls AFTER INSERT ON $DBName.User
	FOR EACH ROW BEGIN
		INSERT INTO $DBName.PasswordHash (UserID) VALUES (NEW.UserID);
		INSERT INTO $DBName.Application (userID) VALUES (NEW.UserID);
		INSERT INTO $DBName.MentorApplication (UserID) VALUES (NEW.UserID);
	END; |
	DELIMITER ;" | mysql -u root

echo "DELIMITER |
	CREATE TRIGGER $DBName.delete_user AFTER DELETE ON $DBName.User
	FOR EACH ROW BEGIN
		DELETE FROM $DBName.PasswordHash WHERE UserID = OLD.UserID;
		DELETE FROM $DBName.Application WHERE UserID = OLD.UserID;
		DELETE FROM $DBName.MentorApplication WHERE UserID = OLD.UserID;
	END; |
	DELIMITER ;" | mysql -u root


echo "Creating default records..."


echo "Creating MentoringProgram with ProgramID of 0 and other field of NULL"

echo "INSERT INTO $DBName.MentoringProgram (ProgramID) VALUES (0)" | mysql -u root

echo "Creating Roles in Role tables"

echo "INSERT INTO $DBName.Role (RoleID, Name, Description)
	VALUES (0, 'Admin', 'Admin is an employee who have rigths to manage MLM')" | mysql -u root
echo "INSERT INTO $DBName.Role (RoleID, Name, Description)
	VALUES (1, 'Student', 'Student is a guy studying in university and whose name has just been sent from university for program')" | mysql -u root
echo "INSERT INTO $DBName.Role (RoleID, Name, Description)
	VALUES (2, 'Candidate', 'Candidate is a student who has a scoolarship but not in MLM yet')" | mysql -u root
echo "INSERT INTO $DBName.Role (RoleID, Name, Description)
	VALUES (3, 'Mentee', 'Mentee is a student who has a scoolatship and mentor. This guy is in MLM now')" | mysql -u root
echo "INSERT INTO $DBName.Role (RoleID, Name, Description)
	VALUES (4, 'Employee', 'Employee is just a guy working in EMC')" | mysql -u root
echo "INSERT INTO $DBName.Role (RoleID, Name, Description)
	VALUES (5, 'Lector', 'Lector is an employee who reads some lectures in EMC for mentees')" | mysql -u root
echo "INSERT INTO $DBName.Role (RoleID, Name, Description)
	VALUES (6, 'Consultant', 'Consultant is an employee who can answer some questions and help sometimes')" | mysql -u root
echo "INSERT INTO $DBName.Role (RoleID, Name, Description)
	VALUES (7, 'Mentor', 'Mentor is an employee who can have mentors and be guru')" | mysql -u root
echo "INSERT INTO $DBName.Role (RoleID, Name, Description)
	VALUES (10, 'Inactive', 'Inactive is a guy who escaped Mentoring Program by some reason such as declining')" | mysql -u root

echo "Done!"
