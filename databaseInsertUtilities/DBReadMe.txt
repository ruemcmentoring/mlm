There is .bash script in directory. It was madefor easy inserting applications into database

"insert.bash"

If you run this with 3 params, it will be interpretated like this:
1st is Email, 2nd is Password and 3rd is Role.
Remember, that role means role ID (0 - Admin, 1 - Mentor, 2 - Consultant, 3 - Candidate, 4 - Mentee)
If there is less or more params then you will be asked to enter it once more in the script.
After entering you will have to check entered data. If you agree, type Y or y (actually it works with Yes, yes and YES also)
If there is no mistakes, you will have been told "record successfully added". Else you will have error message

================================================

How to run?
Open terminal and come to databaseInsertUtilities folder using cd command. After that just type ./insert.bash "params"
("params" is Email, Password and Role)

================================================

PLS DONT USE IT ITS NOT REDONE :D