#!/bin/bash

email="$1"
password="$2"
role="$3"
extra="$4"

if [[ "$email" != "" && "$password" != "" && "$role" != "" && "$extra" == ""]]
then
	echo "Check if entered information is correct: "
	echo "Email is $email, Password is $password, Role is $role"
	echo "Remember roles: 0 - Admin, 1 - Mentor, 2 - Consultant, 3 - Candidate, 4 - Mentee"
	echo "If it's allright, enter Y. Another string will be interpreted as No"
	read agreement
	if [[ "$agreement" == "Y" || "$agreement" == "y" || "$agreement" == "Yes" || "$agreement" == "yes" || "$agreement" == "YES" ]]
	then
		echo "Let me go run"
		go run goFiles/insert.go "$email" "$password" "$role"
		exit
	 else
		echo "Ok, lets repeat"
	fi
fi

echo "Please, enter email: "
read email
echo "Now enter password: "
read password
echo "And now enter role. 0 - Admin, 1 - Mentor, 2 - Consultant, 3 - Candidate, 4 - Mentee: "
read role
echo "Email is $email, Password is $password, Role is $role"

echo "If it's allright, enter Y. Another string will be interpretedas No"
read agreement
if [[ "$agreement" == "Y" || "$agreement" == "y" || "$agreement" == "Yes" || "$agreement" == "yes" || "$agreement" == "YES" ]]
then
	echo "Let me go run"
	go run goFiles/insert.go "$email" "$password" "$role"
	exit
else
	echo "Ok, finished with no inserting"
fi
exit
