package main

import (
    "database/sql"                              //sql
    _ "github.com/go-sql-driver/mysql"          //sql driver
    "fmt"
    "crypto/md5"
    "encoding/hex"
    "strconv"
    "crypto/rand"
    "math/big"
    "time"
)

/*
ROLES:
    0   Admin
    1   Student
    2   Candidate
    3   Mentee
    4   Employee
    5   Lector
    6   Consultant
    7   Mentor
    10  Inactive
*/

const DBIndentify = "testuser:TestPasswd9@/test"

func MD5(pass, salt string) string {
    hasher := md5.New()
    hash := pass + salt
    for i:=0; i<5; i++ {
        hasher.Write([]byte(hash))
        hash = hex.EncodeToString(hasher.Sum(nil))
    }
    return hash
}

const letterBytes = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"

func GetRandomInt(max *big.Int) int {
    i, _ := rand.Int(rand.Reader, max)
    index := int(i.Int64())
    return index
}

func GenerateSalt(n int) string {
    b := make([]byte, n)
    max := big.NewInt(int64(len(letterBytes)-1))
    for i := range b {
        b[i] = letterBytes[GetRandomInt(max)]
    }
    return string(b)
}


func main() {
    amount := 17
    Emails := []string      {"test@emc.com",        "john.kennedy@emc.com",             "clark.kent@yandex.ru",         "sarah.connor@emc.com",     "i.am.the@god.com",                 "tear.of.sadness@emc.com",  "you.are.my.muse@gmail.com",    "john.travolta@mail.ru",    "snow.is.bastard@gmail.com",    "i.am.the.king@emc.com",            "jordan@yandex.ru",     "billy.talent@mail.ru",                                             "fizruk@emc.com",   "ashton.kutcher@gmail.com",         "your.mother.loves@me.com", "fuck.shit.dick@emc.com",   "admin@emc.com" }
    Passwords := []string   {"test123",             "john.kennedy123",                  "clark.kent123",                "sarah.connor123",          "i.am.the123",                      "tear.of.sadness123",       "you.are.my.muse123",           "john.travolta123",         "snow.is.bastard123",           "i.am.the.king123",                 "jordan123",            "billy.talent123",                                                  "fizruk123",        "ashton.kutcher123",                "your.mother.loves123",     "fuck.shit.dick123",        "admin123" }
    Names := []string       {"Test",                "John",                             "Clark",                        "Sarah",                    "Jesus",                            "Thom",                     "Mattew",                       "John",                     "Kateline",                     "Robert",                           "Michael",              "Benjamin",                                                         "Dmitry",           "Ashton",                           "Stas",                     "Sergey",                   "Admin" }
    SurNames := []string    {"Tested",              "Kennedy",                          "Kent",                         "Connor",                   "Christ",                           "Yorke",                    "Belamy",                       "Travolta",                 "Aaron",                        "Baratheon",                        "Jordan",               "Kowalewicz",                                                       "Nagiev",           "Kutcher",                          "Mihailov",                 "Shnurov",                  "The Handler" }
    Sexes := []string       {"Male",                "Male",                             "Male",                         "Female",                   "Male",                             "Male",                     "Male",                         "Male",                     "Female",                       "Male",                             "Male",                 "Male",                                                             "Male",             "Male",                             "Male",                     "Male",                     "Male" }
    Phones := []string      {"+7 000 000 00 00",    "+7 981 000 00 00",                 "+7 981 000 00 01",             "+7 981 000 00 02",         "+7 981 000 00 03",                 "+7 981 000 00 04",         "+7 981 000 00 05",             "+7 981 000 00 06",         "+7 981 000 00 07",             "+7 981 000 00 08",                 "+7 981 000 00 09",     "+7 981 000 00 10",                                                 "+7 981 000 00 11", "+7 981 000 00 12",                 "+7 981 000 00 13",         "+7 981 000 00 14",         "+7-922-suck-it" }
    BirthDates := []string  {"1990-1-1",            "1917-5-29",                        "1960-1-1",                     "1948-10-12",               "0000-1-1",                         "1968-10-7",                "1978-6-9",                     "1954-2-18",                "1943-12-10",                   "1935-10-3",                        "1963-2-17",            "1975-12-16",                                                       "1967-4-4",         "1978-2-7",                         "1969-4-27",                "1973-4-13",                "1960-1-1" }
    Universities := []string{"Unknown University",  "ITMO University",                  "ITMO University",              "Oxford",                   "MIT",                              "MGU",                      "Polytech",                     "LETI",                     "Oxford",                       "Cambridge",                        "ITMO University",      "MIT",                                                              "URFU",             "Gorny",                            "SurGU",                    "MatMeh",                   "Asgard" }
    Jobs := []string        {"Tester",              "Headshotted president",            "Superman",                     "World saviour",            "God",                              "Singer, Raiohead",         "Singer, Muse",                 "Actor",                    "I'm a mother",                 "Dead King of 7 Kingdoms",          "Basketball player",    "Singer, Billy Talent",                                             "Actor",            "Actor",                            "Singer",                   "Singer, Leningrad",        "The Law"}
    ProfInts := []string    {"Code, C++",           "Death, Cars, Ruling",              "Power, C++",                   "Guns, Death, Java",        "Death, Bible, Fairy",              "Music, Sadness, ASM",      "Music, Love, Golang",          "Movies",                   "Wars",                         "Ruling, C++, C#, Java",            "Sport, C#, C++",       "Music, Golang",                                                    "Movie, Guns",      "Movie, Python, MySQL",             "Music, Love, ASM",         "Music, Truth, Java",       "Banning, Handling" }
    Hobbies := []string     {"Lying on the sofa",   "Guns, Girls",                      "Flying, Girls",                "Terminating, Guns",        "Crosses, Judes",                   "Crying, Singing",          "Singing, Cracking",            "Money, Girls",             "Starks, Death",                "Whores, Wine",                     "Basket, Guitar",       "Canada, El, Girls",                                                "Skinheads, Power", "Girls, Money",                     "Hairs, Chest, Women",      "Insult, Guitar",           "Cruel things" }
    Abouts := []string      {"Nothing to say",      "I am Johnny the Headshotted!",     "I hate fucking cryptonium!",   "Oh my God fuck Skynet",    "Do you know tho is my father?",    "Im a creep",               "SuperMassive Black Hole!",     "So crime, yeah",           "I hate you, John Snow!",       "Wine and whores for the King!",    "Three points babe",    "We'll never drink your medicine and we'll never think your way",   "OKNA is over",     "I dont know who the fuck is that", "VSYO DLYA TEBYA",          "NA LABUTENAH NAH",         "Be handled or die, you shit" }
    Roles := []int          {1,                     5,                                  3,                              2,                          3,                                  1,                          7,                              3,                          3,                              1,                                  3,                      5,                                                                  2,                  3,                                  3,                          5,                          0 }
    Exists := []bool        {false,                 false,                              false,                          false,                      false,                              false,                      false,                          false,                      false,                          false,                              false,                  false,                                                              false,              false,                              false,                      false,                      false }

    PhotoURLs := []string {
        "http://www.hardwareluxx.ru/images/stories/reviews/api/news/2013/p2/emc-1.jpg",
        "https://citelighter-cards.s3.amazonaws.com/p17jf9qlps1dlj1t4r1cspg1dp790_74312.png",
        "https://pbs.twimg.com/media/CMpD5k9UYAApS3h.jpg",
        "http://www.writeups.org/img/fiche/685d.jpg",
        "http://images2.fanpop.com/image/photos/10700000/Jesus-Christ-clarklover-10737807-363-391.jpg",
        "https://a1-images.myspacecdn.com/images03/32/af5788cabd3346e1baeb9e875e460876/300x300.jpg",
        "http://img.wennermedia.com/article-leads-vertical-300/1313152780_matthew-bellamy-402.jpg",
        "https://encrypted-tbn2.gstatic.com/images?q=tbn:ANd9GcTZyqt82DRvaRR693FoPXwmcmviP0N8S-qAtJ3R62ey4MEYAAir",
        "http://fanparty.ru/fanclubs/game-of-thrones/articles/84606/76241_tribune_game_of_thrones.jpg",
        "https://pbs.twimg.com/profile_images/3023568897/d72e28d4596f3623c6c4c3410e6c2e21_400x400.png",
        "http://static.tvgcdn.net/mediabin/galleries/celebrities/m_r/michael-a_michael-n/michael_jordan/1/michael_jordan06.jpg",
        "https://pp.vk.me/c320827/v320827796/985d/ibNvPzrVxfg.jpg",
        "http://img.dni.ru/binaries/v2_articlepic/784889.jpg",
        "http://www.kinofilms.ua/images/actors/big/33_Ashton_Kutcher.jpg",
        "http://www.peopleschoice.ru/storage/c/2011/06/07/1307444946_463221_82.jpg",
        "http://rock-vector.com/wp-content/uploads/2015/07/1429700623_1348285150_sergey-shnurov.jpg",
        "http://esotericnews.ru/newversion/uploads/images/00/00/01/2014/10/18/9c169c.jpg" }

    Salts := make([]string, amount)
    for i:=0; i<amount; i++ {
        Salts[i] = GenerateSalt(10)
    }

    ProgramID := 0

    //connection to DB
    db, err := sql.Open("mysql", "testuser:TestPasswd9@/test")
    if err != nil {
        fmt.Println(err)
        return
    }

    //Checking existing
    for i:=0; i<amount; i++ {
        rows, err := db.Query("SELECT Email FROM User WHERE Email = ?", Emails[i])
        if err != nil {
            fmt.Println(err)
            return
        }
        defer rows.Close()
        if rows.Next() {
            fmt.Println("Email " + Emails[i] + " exists")
            Exists[i] = true;
        }
    }
    
    for i:=0; i<amount; i++ {
        if Exists[i] {
            continue
        }

        //Inserting to DB
        PasswordHash := MD5(Passwords[i], Salts[i])
        //to Profile
        _, err := db.Query("INSERT INTO User (Email, IsConfirmed) VALUES (?, ?)", Emails[i], true)
        if err != nil {
            fmt.Println(err)
            return
        }
        //Get ID
        rows, err := db.Query("SELECT UserID FROM User WHERE Email = ?", Emails[i])
        if err != nil {
            fmt.Println(err)
            return
        }
        defer rows.Close()
        if rows.Next() {
            var id int
            rows.Scan(&id)
            //to PasswordHash
            _, err := db.Query("UPDATE PasswordHash SET Salt=?, Hash=? WHERE UserID=?", Salts[i], PasswordHash, id)
            if err != nil {
                fmt.Println(err)
                return
            }
            //to User
            _, err = db.Query("UPDATE User SET Name=?, SurName=?, Sex=?, PhotoURL=?, BirthDate=?, University=?, Phone=?, Job=? WHERE UserID=?", Names[i], SurNames[i], Sexes[i], PhotoURLs[i], BirthDates[i], Universities[i], Phones[i], Jobs[i], id)
            if err != nil {
                fmt.Println(err)
                return
            }
            //to Application
            _, err = db.Query("UPDATE Application SET ProfessionalInterests=?, Hobbies=?, About=? WHERE UserID=?", ProfInts[i], Hobbies[i], Abouts[i], id)
            if err != nil {
                fmt.Println(err)
                return
            }
            //to UserRoleMap
            _, err = db.Query("INSERT INTO ProgramUserRoleMap (ProgramID, UserID, RoleID) VALUES (?, ?, ?)", ProgramID, id, Roles[i])
            if err != nil {
                fmt.Println(err)
                return
            }
		}
        fmt.Println("Record " + strconv.Itoa(i) + " to Profiles successfully added")
        time.Sleep(100 * time.Millisecond)
	}
}