package main

import (
    "database/sql"                              //sql
    _ "github.com/go-sql-driver/mysql"          //sql driver
    "fmt"
    "crypto/md5"
    "encoding/hex"
    "os"
    "regexp"
    "strconv"
)

const DBIndentify = "testuser:TestPasswd9@/test"

func EmailIsOk(email string) bool {
    if len(email) > 30 {
        fmt.Println("WRONG : ", "Too long email, we are working on it :)")
        return false
    }
    if m, _ := regexp.MatchString(`^([\w\.\_]{2,20})@(\w{1,}).([a-z]{2,4})$`, email); !m {
        return false
    } else {
        return true
    }
}

func EmailIsOkForEngineer(email string) bool {
    if m, _ := regexp.MatchString(`^([\w\.\_]{2,20})@emc.com$`, email); !m {
        return false
    } else {
        return true
    }
}

func GetRandomInt(max *big.Int) int {
    i, _ := rand.Int(rand.Reader, max)
    index := int(i.Int64())
    return index
}

func RandStringBytes(n int) string {
    b := make([]byte, n)
    max := big.NewInt(int64(len(letterBytes) - 1))
    for i := range b {
        b[i] = letterBytes[GetRandomInt(max)]
    }
    return string(b)
}

func GetHashMD5(pass, salt string) string {
    hasher := md5.New()
    hash := pass + salt
    //making 5 times MD5
    for i := 0; i < MD5Iteration; i++ {
        hasher.Write([]byte(hash))
        hash = hex.EncodeToString(hasher.Sum(nil))
    }
    return hash
}

func GenerateSalt(n int) string {
    b := make([]byte, n)
    max := big.NewInt(int64(len(letterBytes) - 1))
    for i := range b {
        b[i] = letterBytes[GetRandomInt(max)]
    }
    return string(b)
}


func main() {
    args := os.Args[1:]
    email := args[0]
    pass := args[1]
    roleID, err := strconv.Atoi(args[2])
    salt := GenerateSalt(10)

    if err != nil {
        fmt.Println("Error in roleID")
        return
    }
    
    if len(args) != 3 {
        fmt.Println("Error, not 4 params")
        return
    }

    if !EmailIsOk(email) {
        fmt.Println("Error in email or login")
        return
    }

    if roleID == 1 || roleID == 2 {
        if !EmailIsOkForEngineer(email) {
            fmt.Println("Cant have such role with such email")
            return
        }
    }

    //connection to DB
    db, err := sql.Open("mysql", "testuser:TestPasswd9@/test")
    if err != nil {
        fmt.Println(err)
        return
    }
    //Checking existing
    rows, err := db.Query("SELECT Email FROM User WHERE Email = ?", email)
    if err != nil {
        fmt.Println(err)
        return
    }
    defer rows.Close()
    if rows.Next() {
        fmt.Println("Email exists")
    } else {
    //Inserting to DB
        passwordHash := MD5(pass)
        _, err = db.Query("INSERT INTO User (Email) VALUES (?)", email)
        if err != nil {
            fmt.Println(err)
            return
        } else {
            fmt.Println("Record to Applications successfully added")
        }

        rows, err = db.Query("SELECT UserID FROM User WHERE Email = ?", email)
        if err != nil {
        	fmt.Println(err)
        	return
    	}
    	
    	if rows.Next() {
        	var id int
        	rows.Scan(&id)
			_, err = db.Query("UPDATE PasswordHash SET Hash = ? WHERE UserID = ?", passwordHash, id)
			if err != nil {
				fmt.Println(err)
			}

            _, err = db.Query("UPDATE PasswordSalt SET Salt = ? WHERE UserID = ?", salt, id)
            if err != nil {
                fmt.Println(err)
            }

            rows, err = db.Query("SELECT MAX(ProgramID) FROM MentoringProgram")
            if err != nil {
                fmt.Println(err)
            }
            _, err = db.Query("INSERT INTO ProgramUserRoleMap (ProgramID, UserID, RoleID) VALUES (?, ?, ?)", programID, id, roleID)
		}
	}
}