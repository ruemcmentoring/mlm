package main

import (
	//our 
	"../../lib/config"
	//customs
	_ "github.com/go-sql-driver/mysql"
	//golang
	"fmt"
	"database/sql"
	"encoding/csv"
	"os"
)

const MD5Iteration = 5

const letterBytes = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"

func GetRandomInt(max *big.Int) int {
	i, _ := rand.Int(rand.Reader, max)
	index := int(i.Int64())
	return index
}

func RandStringBytes(n int) string {
	b := make([]byte, n)
	max := big.NewInt(int64(len(letterBytes) - 1))
	for i := range b {
		b[i] = letterBytes[GetRandomInt(max)]
	}
	return string(b)
}

func GetHashMD5(pass, salt string) string {
	hasher := md5.New()
	hash := pass + salt
	//making 5 times MD5
	for i := 0; i < MD5Iteration; i++ {
		hasher.Write([]byte(hash))
		hash = hex.EncodeToString(hasher.Sum(nil))
	}
	return hash
}

func GenerateSalt(n int) string {
	b := make([]byte, n)
	max := big.NewInt(int64(len(letterBytes) - 1))
	for i := range b {
		b[i] = letterBytes[GetRandomInt(max)]
	}
	return string(b)
}

func main(){

	//We will add candidates
	roleID := 3

	//Definition and opening a CSV file containing names and emails of students
	nameCSVfile := os.Args[1]

	csvfile, err := os.Open(nameCSVfile)
	if err != nil {
		fmt.Println(err)
	}

	defer csvfile.Close()
  	
  	//Parsing CSV file
	reader := csv.NewReader(csvfile)
	reader.Comma = ';'

	csvData,err := reader.ReadAll()
	if err != nil {
		fmt.Println(err)
	}

	//Opening database
	db, err := sql.Open(config.DBdriver, config.GetDBIdentify())
	if err != nil {
		fmt.Println("ERROR : ", err)
		return
	}

	//Inserting new record to Profiles
	for _,each := range csvData {
		_, err := db.Query("INSERT INTO `Profile` (`Email`) VALUES (?)", each[1])
		if err != nil {
			fmt.Println("ERROR : ", err)
			fmt.Println("Not added records")
			return
		}

		rows, err := db.Query("SELECT ID FROM `Profile` WHERE Email = ?", each[1])
		if err != nil {
		
		}

		defer rows.Close()

		//If we added, than ID has to exist
		var id int
		if rows.Next() {
			rows.Scan(&id)
		} else {
			fmt.Println("ID not found")
			return 
		}

		pass := GenerateSalt(6)
		salt := GenerateSalt(10)
		hash := GetHashMD5(pass, salt)

		_,err = db.Query("UPDATE PasswordHash SET Hash=? WHERE ID=?", hash, id) 
		if err != nil {
			fmt.Println("ERROR : ", err)
			return
		}

		_,err := db.Query("UPDATE PasswordSalt SET Salt=? WHERE ID=?", salt, id)
		if err != nil {
			fmt.Println("ERROR : ", err)
			return
		}

		_,err = db.Query("UPDATE User SET Name=? WHERE ID=?", each[0], id)
		if err != nil {
			fmt.Println("ERROR : ", err)
			return
		}

		fmt.Println("Email: ", each[1], "Pass: ", pass)
	}

}