package main

import (
	//our
	"./lib/cmdReader"
	"./lib/config"
	"./lib/handlers"
	"./lib/handlers/databaseHandlers"
	"./lib/handlers/jsonHandlers"
	//customs
	"github.com/antigloss/go/logger"
	"github.com/gorilla/mux"
	//golang
	"fmt"
	"net/http"
)

func main() {
	logger.Init(config.LogDir, config.MaxLogFilesInLogDir, config.NumberOfFilesToDelete, config.MaxSizeLogFile_MB, false)

	logger.Info("Starting server")

	go func() {
		//Creating new router
		router := mux.NewRouter()
		router.KeepContext = true
		router.NotFoundHandler = http.HandlerFunc(Handlers.NotFoundHandler)

		//Errors handler
		router.HandleFunc("/error/{code:[0-9]+}", Handlers.ErrorViewHandler)

		//View handlers
		router.HandleFunc("/"+config.Page404, Handlers.NotFoundHandler)
		router.HandleFunc("/", Handlers.IndexViewHandler)

		router.HandleFunc("/"+config.StartPage, Handlers.IndexViewHandler)
		router.HandleFunc("/"+config.LoginPage, Handlers.LoginViewHandler)
		router.HandleFunc("/"+config.ApplicationPage, Handlers.ApplicationViewHandler)
		router.HandleFunc("/"+config.AfterlogInfoPage, Handlers.AfterlogViewHandler)
		router.HandleFunc("/"+config.AboutPage, Handlers.AboutViewHandler)
		router.HandleFunc("/"+config.ProfilePage, Handlers.MyProfileViewHandler)
		router.HandleFunc("/"+config.Dash, Handlers.DashViewHandler)
		router.HandleFunc("/"+config.Dash+"/userid={userid:[0-9]+}", Handlers.OtherDashViewHandler)
		router.HandleFunc("/application.html/userid={userid:[0-9]*}", Handlers.OtherApplicationViewHandler)
		router.HandleFunc("/search.html", Handlers.SearchViewHandler)

		router.HandleFunc("/test.html", Handlers.ViewHandler)

		//search handlers
		//		router.HandleFunc("/search={search:[a-zA-Z0-9]*}/role={role:[-0-9]*}", Handlers.SearchViewHandler)
		//		router.HandleFunc("/search", Handlers.SearchViewHandler)

		//Database handlers
		router.HandleFunc("/exit", Handlers.ExitHandler)
		router.HandleFunc("/reg", DBHandlers.RegistrationHandler)
		router.HandleFunc("/log", DBHandlers.LoginHandler)

//		router.HandleFunc("/change", DBHandlers.ChangeInfoHandler)
		router.HandleFunc("/sendapp", DBHandlers.SendApplicationHandler)
		router.HandleFunc("/updateapp", DBHandlers.UpdateApplicationHandler)
		router.HandleFunc("/sendevent", DBHandlers.SendEventHandler)
		router.HandleFunc("/newuser", DBHandlers.NewUserHandler)
		router.HandleFunc("/changepassword", DBHandlers.ChangePasswordHandler)
		router.HandleFunc("/deleteevent/eventid={eventid:[0-9]*}", DBHandlers.DeleteEventHandler)
		router.HandleFunc("/pinevent/eventid={eventid:[0-9]*}", DBHandlers.PinEventHandler)
		router.HandleFunc("/unpinevent/eventid={eventid:[0-9]*}", DBHandlers.UnpinEventHandler)
		router.HandleFunc("/editevent", DBHandlers.EditEventHandler)
		router.HandleFunc("/deleteuser", DBHandlers.DeleteUserHandler)
		router.HandleFunc("/{code:[a-zA-Z0-9]{18}}", DBHandlers.ConfirmRegistrationHandler)

		router.HandleFunc("/formcancel", Handlers.FormCancelHandler)

		//JSON GET
		router.HandleFunc("/api/getauth/email={email}", JSONHandlers.GetAuthHandler).Methods("GET")
		router.HandleFunc("/api/getmentorapp/userid={userid:[0-9]*}", JSONHandlers.GetMentorApplicationHandler).Methods("GET")
		router.HandleFunc("/api/getuserinfo/userid={userid:[0-9]*}&programid={programid:[0-9]*}", JSONHandlers.GetUserInfoHandler).Methods("GET")
		router.HandleFunc("/api/getusername/userid={userid:[0-9]*}", JSONHandlers.GetUserNameHandler).Methods("GET")
		router.HandleFunc("/api/getrolepeople/roleid={roleid:[0-9]*}", JSONHandlers.GetRolePeopleHandler).Methods("GET")
		router.HandleFunc("/api/getevents/programid={programid:[0-9]*}", JSONHandlers.GetEventsHandler).Methods("GET")
		router.HandleFunc("/api/getroles", JSONHandlers.GetRolesHandler).Methods("GET")
		router.HandleFunc("/api/getusers/programid={programid:[0-9]*}", JSONHandlers.GetUserInfosHandler).Methods("GET")
		router.HandleFunc("/api/checkapp/email={email}", JSONHandlers.CheckApplicationHandler).Methods("GET")		
		//JSON POST
		router.HandleFunc("/api/postapp", JSONHandlers.PostApplicationHandler).Methods("POST")
		router.HandleFunc("/api/postauth", JSONHandlers.PostAuthHandler).Methods("POST")
		router.HandleFunc("/api/postmentorapp", JSONHandlers.PostMentorApplicationHandler).Methods("POST")
		router.HandleFunc("/api/postnewuser", JSONHandlers.PostNewUserHandler).Methods("POST")
		router.HandleFunc("/api/postuser", JSONHandlers.PostUserHandler).Methods("POST")
		router.HandleFunc("/api/postevent", JSONHandlers.PostEventHandler).Methods("POST")
		router.HandleFunc("/api/updateevent", JSONHandlers.UpdateEventHandler).Methods("POST")
		//JSON DELETE
		router.HandleFunc("/api/deleteevent/eventid={eventid:[0-9]*}", JSONHandlers.DeleteEventHandler).Methods("DELETE")
		router.HandleFunc("/api/deleteuser/userid={userid:[0-9]*}", JSONHandlers.DeleteUserHandler).Methods("DELETE")
		//JSON UPDATE
		router.HandleFunc("/api/pinevent/eventid={eventid:[0-9]*}", JSONHandlers.PinEventHandler)
		router.HandleFunc("/api/unpinevent/eventid={eventid:[0-9]*}", JSONHandlers.UnpinEventHandler)

		//File handlers
		router.HandleFunc("/JS/{file:.*}", Handlers.FileHandler)
		router.HandleFunc("/favicon.ico", Handlers.FaviconHandler)
		router.HandleFunc("/css/{file:.*}", Handlers.FileHandler)
		router.HandleFunc("/fonts/{file:.*}", Handlers.FileHandler)
		router.HandleFunc("/images/{file:.*}", Handlers.FileHandler)

		//Setting handle
		http.Handle("/", router)
		//setting port and context writer (context is cleaned after shutting down)
		http.ListenAndServe(":8080", router)
	}()

	logger.Info("Server started")

	ch := make(chan int)
	go cmdReader.Read(ch)
	select {
	case <-ch:
		fmt.Println("Server is off")
		logger.Info("Server is off")
		return
	}
}
