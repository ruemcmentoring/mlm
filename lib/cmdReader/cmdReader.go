package cmdReader

import (
	"../config"
	"bufio"
	"fmt"
	//"gin"
	"os"
)

func Read(ch chan int) {
	reader := bufio.NewReader(os.Stdin)

	config.DebugInit()
	config.ProgramInit()

	fmt.Println("Console commands are on")

	for {
		cmd, err := reader.ReadString('\n')

		if err != nil {
			fmt.Println(err.Error())
			return
		}
		switch cmd {
		case "quit\n":
			ch <- 1
		case "debug\n":
			config.DebugChange()
			fmt.Println("Debug mode ", config.DEBUG)
		case "rs\n":
			ch <- 2
//		case "test db\n":
//			DBQueries.QueriesTest()
		}
	}
}
