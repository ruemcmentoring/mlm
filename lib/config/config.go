package config

import (
	"fmt"
	"os"
)

//debug mode
var DEBUG bool

func DebugInit() {
	DEBUG = true
}

func DebugChange() {
	DEBUG = !DEBUG
}

const (
	Protocol = "http://"
	Server   = "localhost:8080/"
)

//common server configuration
var HOMEDIR string

func InitHomeDir() {
	HOMEDIR = os.Getenv("HOME") + "mlm"
	fmt.Println("Current project home directory is " + HOMEDIR)
	fmt.Println("If it is not your homedir, please change config.go")
}

//const ProfilePhotoDir = HOMEDIR + "profilePhoto/"
const (
	ProfilePhotoDir = "images/profilePhoto/"
	WebAppex        = "web/"
)

//database configuration
const (
	DBuser     = "testuser"
	DBpassword = "TestPasswd9"
	DBname     = "test"
)

func GetDBIdentify() string {
	return DBuser + ":" + DBpassword + "@/" + DBname
}

const DBdriver = "mysql"

//css visibility styles
const NoneVis = "visible"
const HiddenVis = "invisible"

//logger configuration
const (
	LogDir                = "./log"
	MaxLogFilesInLogDir   = 10
	NumberOfFilesToDelete = 2
	MaxSizeLogFile_MB     = 2
)

//message configuration
const MessageFile = "./commonMessagePattern.msg"
const EmailAddress = "mentoring.lyfecycle.program"
const EmailPassword = "password316"

//size of file configuration
const MaxSizePhoto_Byte = 4096

//session configurations
const (
	StoreSecretKey    = "teststring" //to create if not exist
	UserSessionKey    = "test"       //to get user session
	ErrorSessionKey   = "errorkey"   //to get errors session
	ConfirmSessionKey = "confirmkey" //to get confirmation session
	SearchSessionKey  = "searchkey"  //to get search key
)

//length of unique link
const LenOfCode = 18

//different shit
const NoImage = ProfilePhotoDir + "NoPhoto.jpeg"

//THIS IS NOT FOREVER!!!! BUT NOW PROGRAM ID IS SET TO 0!!!
//const ProgramID = 0

var ProgramID int

func ProgramInit() {
	ProgramID = 0
}
