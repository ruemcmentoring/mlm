package config

import "fmt"

const (
	StartPage             = "index.html"
	Page404               = "404.html"
	ErrorPage             = "error.html"
	LoginPage             = "login.html"
	AfterlogInfoPage      = "afterlog.html"
	AboutPage             = "about.html"
	ApplicationPage 	  = "application.html"
	ProfilePage           = "profile.html"
	ViewBlockPage         = "viewblock.html"
	SearchPage            = "search.html"
	ChangeInfoPage        = "change.html"
)

//dashes
const (
	Dash      	   = "dash.html"
	AdminDash      = "admindash.html"
	MenteeDash     = "menteedash.html"
	LectorDash     = "lectordash.html"
	ConsultantDash = "consultantdash.html"
	MentorDash     = "mentordash.html"
)

//errors
const (
	ErrorRef   = "error"
	ErrorAppex = "error/"
)

/*
func DashByRole(roleid int) string {
	fmt.Println("Dashing")
	switch roleid {
	case Admin:
		return AdminDash
	case Mentee:
		return MenteeDash
	case Lector:
		return LectorDash
	case Consultant:
		return ConsultantDash
	case Mentor:
		return MentorDash
	default:
		if DEBUG {
			fmt.Println("PANIC!!! We do have the dashboard with wrong roleid ", roleid)
		}
		return Page404 //if there are no any errors we will newer get here
	}
}
*/

func DashByRole(roleid int) string {
	fmt.Println("Dashing")
	return Dash
}