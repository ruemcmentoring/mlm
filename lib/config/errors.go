package config

import (
	//customs
	"github.com/antigloss/go/logger"
	//golang
	"fmt"
	"net/http"
	"strconv"
)

const (
	UnknownError = -1

	InvalidSession      = 100
	NotEnoughRights     = 101
	InvalidPassword     = 102
	AlreadyRegistered   = 103
	AlreadyLogged       = 104
	NoUserWithSuchEmail = 105

	DatabaseError = 200
	InsertError   = 201
	UpdateError   = 202
	DeleteError   = 203
	SelectError   = 204

	GetJSONError    = 301
	PostJSONError   = 302
	EncodeJSONError = 303
	DecodeJSONError = 304

	InvalidParameters  = 400
	OpenFileError      = 401
	ParseTemplateError = 402

	InvalidCasting = 500

	SendEmailError = 600

	InvalidRequest = 700
)

func SendError(w http.ResponseWriter, r *http.Request, e string, c int) {
	logger.Error("ERROR : ", e)
	if DEBUG {
		fmt.Println("Redirecting to " + Server + ErrorAppex + strconv.Itoa(c))
	}
	http.Redirect(w, r, Protocol+Server+ErrorAppex+strconv.Itoa(c), 307)
	return
}
