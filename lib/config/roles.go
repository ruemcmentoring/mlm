package config

const (
	Admin      = 0
	Student    = 1
	Candidate  = 2
	Mentee     = 3
	Employee   = 4
	Lector     = 5
	Consultant = 6
	Mentor     = 7
	Inactive   = 10
)

func GetRoleString(roleID int) string {
	switch roleID {
	case Admin:
		return "Admin"
	case Student:
		return "Student"
	case Candidate:
		return "Candidate"
	case Mentee:
		return "Mentee"
	case Employee:
		return "Employee"
	case Lector:
		return "Lector"
	case Consultant:
		return "Consultant"
	case Mentor:
		return "Mentor"
	default:
		return "Inactive"
	}
}

//For what roles can you apply?
func RoleApplication(roleID int) bool {
	//					Admin	Student	Candidate	Mentee	Employee	Lector	Consultant	Mentor 	Inactive
	list := [9]bool{	false,	false,	true,		false,	false,		true,	true,		true,	false}
	return list[roleID];
}

//What roles can edit application?
func RoleEditApplication(roleID int) bool {
	//					Admin	Student	Candidate	Mentee	Employee	Lector	Consultant	Mentor 	Inactive
	list := [9]bool{	true,	false,	true,		true,	false,		true,	true,		true,	false}
	return list[roleID];
}

//What roles can edit other application?
func RoleEditOtherApplication(roleID int) bool {
	//					Admin	Student	Candidate	Mentee	Employee	Lector	Consultant	Mentor 	Inactive
	list := [9]bool{	true,	false,	false,		false,	false,		false,	false,		false,	false}
	return list[roleID];
}