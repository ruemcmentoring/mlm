package Handlers

import (
	//our
	"../config"
	//customs
	"github.com/antigloss/go/logger"
	"github.com/gorilla/mux"
	//golang
	"fmt"
	"html/template"
	"net/http"
	"strconv"
)

func ErrorViewHandler(w http.ResponseWriter, r *http.Request) {
	//Get vars
	vars := mux.Vars(r)
	errorcode, err := strconv.Atoi(vars["code"])
	if err != nil {
		errorcode = -1
	}

	//Get error
	error := errors[errorcode]

	//Get title
	title := config.WebAppex + "error/"
	if config.DEBUG {
		fmt.Println("ErrorView Handler handling " + title + vars["code"])
	}

	title = config.WebAppex + config.ErrorPage

	//Parsing HTML
	t, err := template.ParseFiles(config.HOMEDIR + title)
	if err != nil {
		logger.Error("ERROR : ", err)
		return
	}

	//Creating error variable
	Error := struct {
		Message string
	}{
		error,
	}

	//Execute template
	t.Execute(w, Error)

	return
}

var errors = map[int]string{
	config.UnknownError: "Unknown error",

	config.InvalidSession:      "Invalid session",
	config.NotEnoughRights:     "Not enough rights",
	config.InvalidPassword:     "Invalid password",
	config.AlreadyRegistered:   "Already registered",
	config.AlreadyLogged:       "Already logged",
	config.NoUserWithSuchEmail: "No User With Such Email",

	config.DatabaseError: "Database error",
	config.InsertError:   "Insert error",
	config.UpdateError:   "Update error",
	config.DeleteError:   "Delete error",
	config.SelectError:   "Select error",

	config.GetJSONError:    "Get JSON error",
	config.PostJSONError:   "Post JSON error",
	config.EncodeJSONError: "Encode JSON error",
	config.DecodeJSONError: "Decode JSON error",

	config.InvalidParameters:  "Invalid parameters",
	config.OpenFileError:      "Open File error",
	config.ParseTemplateError: "Parse Template error",

	config.InvalidCasting: "Invalid casting",

	config.SendEmailError: "Send Email error",

	config.InvalidRequest: "Invalid Request error",
}
