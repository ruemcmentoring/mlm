package JSONHandlers

import (
	//our
//	"../../config"
	//customs
	"github.com/gorilla/mux"
	_ "github.com/go-sql-driver/mysql"
	"github.com/antigloss/go/logger"
	//golang
	"net/http"
	"strconv"
	"fmt"
)

func PinEventHandler(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")

	//Get needed vars
	vars := mux.Vars(r)
	eventid := vars["eventid"]

	eventID, err := strconv.Atoi(eventid)
	if err != nil {
		logger.Error("ERROR : ", err)
		return
	}

	err = PinEvent(eventID)
	if err != nil {
		logger.Error("ERROR : ", err)
		return
	}

	fmt.Println("Pinned")

	return
}


//Get (UserID, Hash, Salt, IsConfirmed)
func PinEvent(eventid int) error {
	db, err := OpenDB()
	if err != nil {
		return err
	}
	defer db.Close()

	_, err = db.Query("UPDATE Event SET Pinned=0 WHERE Pinned=1")
	if err != nil {
		return err
	}

	_, err = db.Query("UPDATE Event SET Pinned=1 WHERE EventID = ?", eventid)
	return err
}