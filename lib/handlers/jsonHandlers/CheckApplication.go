//In case user failed application filling
//User doesnt have any fields filled (except password, email and name)
//So lets check just a phone and BirthDate

package JSONHandlers

import (
	//customs
	"github.com/gorilla/mux"
	_ "github.com/go-sql-driver/mysql"
	//golang
	"database/sql"
	"net/http"
	"encoding/json"
)

func CheckApplicationHandler(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")

	//Get needed vars
	vars := mux.Vars(r)
	email := vars["email"]

	badResult := "undefined";

	//Get UserName struct
	check, err := CheckApplication(email)
	if err != nil {
		js, _ := json.Marshal(badResult)
		w.Write(js)
		return
	}
	
	//Encode UserName struct
	js, err := json.Marshal(check)
	if err != nil {
		js, _ := json.Marshal(badResult)
		w.Write(js)
		return
	}

	//Send
	w.Write(js)
	return
}


//Get (Name)
func RowsCheckApplication(email string, db *sql.DB) (*sql.Rows, error) {
	rows, err := db.Query("SELECT Phone, BirthDate FROM User WHERE Email = ?", email)
	return rows, err
}

func CheckApplication(email string) (string, error) {
	db, err := OpenDB()
	if err != nil {
		return "undefined", err
	}
	defer db.Close()

	rows, err := RowsCheckApplication(email, db)

	var phone, bd sql.NullString
	var Phone, BD string

	if err != nil {
		return "undefined", err
	}
	if rows.Next() {
		rows.Scan(&phone, &bd)
		if phone.Valid {
			Phone = phone.String
		} else {
			Phone = ""
		}

		if bd.Valid {
			BD = bd.String
		} else {
			BD = ""
		}

		if Phone == "" || BD == "" {
			return "false", nil
		} else {
			return "true", nil
		}
	}
	return "undefined", err
}