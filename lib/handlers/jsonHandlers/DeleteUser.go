package JSONHandlers

import (
	//our
//	"../../config"
	//customs
	"github.com/gorilla/mux"
	_ "github.com/go-sql-driver/mysql"
	"github.com/antigloss/go/logger"
	//golang
	"net/http"
	"strconv"
)

func DeleteUserHandler(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")

	//Get needed vars
	vars := mux.Vars(r)
	userid := vars["userid"]

	userID, err := strconv.Atoi(userid)
	if err != nil {
		logger.Error("ERROR : ", err)
		return
	}

	err = DeleteUser(userID)
	if err != nil {
		logger.Error("ERROR : ", err)
		return
	}

	return
}

func DeleteUser(userid int) error {
	db, err := OpenDB()
	if err != nil {
		return err
	}
	defer db.Close()

	_, err = db.Query("DELETE FROM User WHERE UserID = ?", userid)

	return err
}