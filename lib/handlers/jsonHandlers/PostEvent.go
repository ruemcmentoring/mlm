package JSONHandlers

import (
	//customs
	_ "github.com/go-sql-driver/mysql"
	//golang
	"net/http"
	"encoding/json"
	"fmt"
)

func PostEventHandler(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")

	//Decode JSON	
	decoder := json.NewDecoder(r.Body)
    var eventInfo EventInfo
    err := decoder.Decode(&eventInfo)
    if err != nil {
    	w.Write(JSONErr(err.Error()))
    	return
    }

    fmt.Println("Decoded event")

    //Post user to DB
    err = InsertEventInfo(eventInfo)
    if err != nil {
    	fmt.Println(err)
    	w.Write(JSONErr(err.Error()))
    	return
    }

    fmt.Println("Inserted")

    //Send OK result
	w.Write(JSONOk())
	return

}

//USE ONLY WITH ALL THE FIELDS FILLED RIGHT!!!
func InsertEventInfo(eventInfo EventInfo) error {
	db, err := OpenDB()
	if err != nil {
		return err
	}
	defer db.Close()
	
	if (eventInfo.LectorID == 0) {
		eventInfo.LectorID = 1
	}

	if (eventInfo.FromID == 0) {
		eventInfo.FromID = 1
	}
	fmt.Println(eventInfo.FromID)

	
	if eventInfo.EventType == 1 {
		_, err = db.Query("INSERT INTO Event (Date, FromID, LectorID, Subject, Body, ImageURL, VoteJSON, ProgramID, EventType) VALUES (?, ?, ?, ?, ?, ? ,? ,?, ?)", eventInfo.Date, eventInfo.FromID, eventInfo.LectorID, eventInfo.Subject, eventInfo.Body, eventInfo.PhotoURL, eventInfo.VoteLink, eventInfo.ProgramID, eventInfo.EventType)
	} else {
		_, err = db.Query("UPDATE Event SET Pinned=0 WHERE Pinned=1")
		if err != nil {
			return err
		}
		_, err = db.Query("INSERT INTO Event (Date, FromID, LectorID, Subject, Body, ImageURL, VoteJSON, ProgramID, EventType, Pinned) VALUES (?, ?, ?, ?, ?, ? ,? ,?, ?, 1)", eventInfo.Date, eventInfo.FromID, eventInfo.LectorID, eventInfo.Subject, eventInfo.Body, eventInfo.PhotoURL, eventInfo.VoteLink, eventInfo.ProgramID, eventInfo.EventType)
	}

	return err
}