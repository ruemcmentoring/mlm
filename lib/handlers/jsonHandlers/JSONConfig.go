package JSONHandlers

/*
ROLES:
    0   Admin
    1   Student
    2   Candidate
    3   Mentee
    4   Employee
    5   Lector
    6   Consultant
    7   Mentor
    10  Inactive
*/

import (
	//our
	"../../config"
	//customs
	_ "github.com/go-sql-driver/mysql"
	//golang
	"database/sql"
	"errors"
	"encoding/json"
	"strings"
)

type RoleInfo struct {
	RoleID 	int
	Name 	string
}

type NewUser struct {
	Email 		string
	Name 		string
	RoleID 		int
}

type UserInfo struct {
	UserID      int
	Email      	string
	Name       	string
	SurName		string
	Sex 		string
	RoleID		int
	RoleStr    	string
	PhotoURL   	string
	Phone      	string
	BD         	string
	University  string
	PI         	string
	Hobbies    	string
	About      	string
	Job 		string
//	Visibility string
}

func EmptyUserInfo() []byte {
	var userInfo UserInfo
	js, _ := json.Marshal(userInfo)
	return js
}

type Auth struct {
	UserID		int
	Email		string
	Hash		string
	Salt		string
	IsConfirmed	bool
}

func EmptyAuth() []byte {
	var auth Auth
	js, _ := json.Marshal(auth)
	return js
}

type RolePeople struct {
	RoleID 		int
	RoleStr 	string
	Users 		[]*User
}

func EmptyRolePeople() []byte {
	var rolePeople RolePeople
	js, _ := json.Marshal(rolePeople)
	return js
}

type EventInfo struct {
	EventID		int
	Date		string
	AddDate		string
	FromID		int
	LectorID	int
	LectorName	string
	Subject		string
	Body		string
	PhotoURL	string
	VoteLink	string
	ProgramID	int
	EventType	int
	Files		[]*File
	Pinned 		bool
}

func (ei EventInfo) GetDate() string {
	if len(ei.Date) != 10 {
		return ""
	}

	date := strings.Split(ei.Date, "-")
	day := date[2]
	mounth := date[1]
	year := date[0]
	var mstr string
	switch mounth {
		case "01": mstr = "Jan"
		case "02": mstr = "Feb"
		case "03": mstr = "Mar"
		case "04": mstr = "Apr"
		case "05": mstr = "May"
		case "06": mstr = "Jun"
		case "07": mstr = "Jul"
		case "08": mstr = "Aug"
		case "09": mstr = "Sep"
		case "10": mstr = "Oct"
		case "11": mstr = "Nov"
		case "12": mstr = "Dec"
	}
	return day + " " + mstr + " " + year
}
func (ei EventInfo) GetAddDate() string {
	if len(ei.AddDate) != 19 {
		return ""
	}

	datetime := strings.Split(ei.AddDate, " ")
	date := strings.Split(datetime[0], "-")
	time := strings.Split(datetime[1], ":")

	day := date[2]
	mounth := date[1]
	year := date[0]

	hour := time[0]
	minute := time[1]

	var mstr string
	switch mounth {
		case "01": mstr = "Jan"
		case "02": mstr = "Feb"
		case "03": mstr = "Mar"
		case "04": mstr = "Apr"
		case "05": mstr = "May"
		case "06": mstr = "Jun"
		case "07": mstr = "Jul"
		case "08": mstr = "Aug"
		case "09": mstr = "Sep"
		case "10": mstr = "Oct"
		case "11": mstr = "Nov"
		case "12": mstr = "Dec"
	}
	return hour + ":" + minute + ", " + day + " " + mstr + " " + year
}


func EmptyEventInfo() []byte {
	var eventInfo EventInfo
	js, _ := json.Marshal(eventInfo)
	return js
}

type User struct {
	UserID 		int
	Name 		string
	SurName 	string
	Sex 		string
	PhotoURL	string
	BirthDate	string
	University	string
	Phone		string
	Email 		string
	Job 		string
}

func EmptyUser() []byte {
	var user User
	js, _ := json.Marshal(user)
	return js
}

type Application struct {
	UserID  	int
	PI 			string
	Hobbies		string
	About		string
}

func EmptyApplication() []byte {
	var application Application
	js, _ := json.Marshal(application)
	return js
}

type MentorApplication struct {
	UserID 				int
	PreferredUniversity	string
	PreferredSex		string
	PreferredInterests	string
	LectureWish			string
	ReadyForTwo			bool
}

func EmptyMentorApplication() []byte {
	var mentorApplication MentorApplication
	js, _ := json.Marshal(mentorApplication)
	return js
}

type File struct {
	FileID int
	Link string
	Name string
}

func EmptyFile() []byte {
	var file File
	js, _ := json.Marshal(file)
	return js
}

type JSONResult struct {
	Result string
}

func JSONOk() []byte {
	res := JSONResult{"OK"}
	js, _ := json.Marshal(res)
	return js
}
func JSONErr(Message string) []byte {
	res := JSONResult{Message}
	js, _ := json.Marshal(res)
	return js
}

type UserName struct {
	Name string
	SurName string
}

func EmptyUserName() []byte {
	var userName UserName
	js, _ := json.Marshal(userName)
	return js
}



func OpenDB() (*sql.DB, error) {
	db, err := sql.Open(config.DBdriver, config.GetDBIdentify())
	return db, err
}

//Get (Email)
func GetRowsEmail(id int, db *sql.DB) (*sql.Rows, error) {
	rows, err := db.Query("SELECT Email FROM User WHERE UserID=?", id)
	return rows, err
}

func GetEmail(id int) (string, error) {
	db, err := OpenDB()
	if err != nil {
		return "", err
	}

	rows, err := GetRowsEmail(id, db)

	var email string
	if rows.Next() {
		rows.Scan(&email)
		return email, nil
	}
	return "", err
}


//Get (UserID)
func GetRowsUserID(email string, db *sql.DB) (*sql.Rows, error) {
	rows, err := db.Query("SELECT UserID FROM User WHERE Email = ?", email)
	return rows, err
}

func GetUserID(email string) (int, error) {
	db, err := OpenDB()
	if err != nil {
		return -1, err
	}

	rows, err := GetRowsUserID(email, db)
	if err!= nil {
		return -1, err
	}

	var id int
	if rows.Next() {
		rows.Scan(&id)
	} else {
		return -1, errors.New("No user")
	}

	return id, nil
}


func UpdateProgramUserRole(programID, userID, roleID int) error {
	db, err := OpenDB()
	if err != nil {
		return err
	}

	_, err = db.Query("UPDATE ProgramUserRoleMap SET ProgramID = ?, RoleID = ? WHERE UserID = ?", programID, roleID, userID)
	return err
}