package JSONHandlers

import (
	//customs
	_ "github.com/go-sql-driver/mysql"
	//golang
	"net/http"
	"encoding/json"
	"fmt"
)

func PostUserHandler(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")

	//Decode JSON	
	decoder := json.NewDecoder(r.Body)
    var user User
    err := decoder.Decode(&user)
    if err != nil {
    	w.Write(JSONErr(err.Error()))
    	return
    }

    //Post user to DB
    err = UpdateUser(user)
    if err != nil {
    	w.Write(JSONErr(err.Error()))
    	return
    }

    //Send OK result
	w.Write(JSONOk())
	return

}

//USE ONLY WITH ALL THE FIELDS FILLED RIGHT!!!
func UpdateUser(user User) error {
	db, err := OpenDB()
	if err != nil {
		return err
	}
	defer db.Close()
	
	if user.PhotoURL == "" {
		fmt.Println("Query with empty photo")
		_, err = db.Query("UPDATE User SET Name=?, SurName=?, Sex=?, BirthDate=?, Phone=?, University=?, Email=?, Job=? WHERE UserID=?", user.Name, user.SurName, user.Sex, user.BirthDate, user.Phone, user.University, user.Email, user.Job, user.UserID)
	} else {
		fmt.Println("Query with photo")
	_, err = db.Query("UPDATE User SET Name=?, SurName=?, Sex=?, PhotoURL=?, BirthDate=?, Phone=?, University=?, Email=?, Job=? WHERE UserID=?", user.Name, user.SurName, user.Sex, user.PhotoURL, user.BirthDate, user.Phone, user.University, user.Email, user.Job, user.UserID)
	}
	
	return err
}