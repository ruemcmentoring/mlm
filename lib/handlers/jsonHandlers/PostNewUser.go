package JSONHandlers

import (
	//our
	"../../config"
	//customs
	_ "github.com/go-sql-driver/mysql"
	//golang
	"database/sql"
	"net/http"
	"encoding/json"
)

func PostNewUserHandler(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")

	//Decode JSON
	decoder := json.NewDecoder(r.Body)
    var newUser NewUser
    err := decoder.Decode(&newUser)
    if err != nil {
   		w.Write(JSONErr(err.Error()))
   		return
    }

    //Post newUser to DB
    err = PutNewUser(newUser)
    if err != nil {
    	w.Write(JSONErr(err.Error()))
    	return
    }

    //Send OK result
	w.Write(JSONOk())
	return

}


func PutEmailName(email, name string, db *sql.DB) error {
	_, err := db.Query("INSERT INTO User (Email, Name) VALUES (?, ?)", email, name)
	return err
}

func PutProgramUserRole(programID, userID, roleID int, db *sql.DB) error {
	_, err := db.Query("INSERT INTO ProgramUserRoleMap (ProgramID, UserID, RoleID) VALUES (?, ?, ?)", programID, userID, roleID)
	return err
}

//GetUserID is implemented in JSONConfig

func PutNewUser(newUser NewUser) error {
	db, err := OpenDB()
	if err != nil {
		return err
	}
	defer db.Close()

	err = PutEmailName(newUser.Email, newUser.Name, db)
	if err != nil {
		return err
	}

	userID, err := GetUserID(newUser.Email)
	if err != nil {
		return err
	}

	err = PutProgramUserRole(config.ProgramID, userID, newUser.RoleID, db)
	if err != nil {
		return err
	}

	return nil
}