package JSONHandlers

import (
	//our
	"../../config"
	//customs
//	"github.com/gorilla/sessions"
	"github.com/gorilla/mux"
	_ "github.com/go-sql-driver/mysql"
	//golang
	"database/sql"
	"net/http"
	"encoding/json"
	"strconv"
)

func GetUserInfoHandler(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")

	//Get needed vars
	vars := mux.Vars(r)
	userID, _ := strconv.Atoi(vars["userid"])
	programID, _ := strconv.Atoi(vars["programid"])

	//Get UserInfo struct
	userInfo, err := GetUserInfo(userID, programID)
	if err != nil {
		w.Write(EmptyUserInfo())
		return
	}

	//Encode UserInfo struct
	js, err := json.Marshal(userInfo)
	if err != nil {
		w.Write(EmptyUserInfo())
		return
	}

	//Send
	w.Write(js)
	return
}


//Use not all the fields if needed
//Get (Email, RoleName, Name, BirthDate, Phone, PhotoURL, University)
func GetRowsUserInfo(userID, programID int, db *sql.DB) (*sql.Rows, error) {
	rows, err := db.Query(`SELECT
			User.Email,
			Role.RoleID, Role.Name,
			User.Name, User.SurName, User.Sex, User.BirthDate, User.Phone, User.PhotoURL, User.University,
			Application.Hobbies, Application.About, Application.ProfessionalInterests,
			User.Job
			FROM User
			INNER JOIN Application ON User.UserID = Application.UserID
			INNER JOIN ProgramUserRoleMap ON User.UserID = ProgramUserRoleMap.UserID
			INNER JOIN Role ON ProgramUserRoleMap.RoleID = Role.RoleID
			WHERE User.UserID = ? AND ProgramUserRoleMap.ProgramID = ?`, userID, programID)
	return rows, err
}

func GetUserInfo(userID, programID int) (*UserInfo, error) {
	db, err := OpenDB()
	if err != nil {
		return nil, err
	}
	defer db.Close()

	rows, err := GetRowsUserInfo(userID, programID, db)

	if err != nil {
		return nil, err
	}
	
	if rows.Next() {
		var email, roleid, rolestr, name, surname, sex, bd, phone, photourl, university, hobbies, about, pi, job sql.NullString
		var Email, RoleStr, Name, SurName, Sex, BD, Phone, PhotoURL, University, Hobbies, About, PI, Job string
		var RoleID int

		rows.Scan(&email, &roleid, &rolestr, &name, &surname, &sex, &bd, &phone, &photourl, &university, &hobbies, &about, &pi, &job)

		if email.Valid {
			Email = email.String
		} else {
			Email = "No email"
		}

		if roleid.Valid {
			RoleID, _ = strconv.Atoi(roleid.String)
		} else {
			RoleID = config.Inactive
		}

		if rolestr.Valid {
			RoleStr = rolestr.String
		} else {
			RoleStr = "No role"
		}

		if name.Valid {
			Name = name.String
		} else {
			Name = "No name"
		}

		if surname.Valid {
			SurName = surname.String
		} else {
			SurName = "No surname"
		}

		if sex.Valid {
			Sex = sex.String
		} else {
			Sex = "Male"
		}
	
		if bd.Valid {
			BD = bd.String
		} else {
			BD = "No birth date"
		}

		if phone.Valid {
			Phone = phone.String
		} else {
			Phone = "No phone"
		}

		if photourl.Valid {
			PhotoURL = photourl.String
		} else {
			PhotoURL = "images/profilePhoto/NoPhoto.jpeg"
		}

		if university.Valid {
			University = university.String
		} else {
			University = "No university"
		}

		if hobbies.Valid {
			Hobbies = hobbies.String
		} else {
			Hobbies = "No hobbies"
		}

		if about.Valid {
			About = about.String
		} else {
			About = "No about info"
		}

		if pi.Valid {
			PI = pi.String
		} else {
			PI = "No interests"
		}

		if job.Valid {
			Job = job.String
		} else {
			Job = "No job"
		}

		return &UserInfo{userID, Email, Name, SurName, Sex, RoleID, RoleStr, PhotoURL, Phone, BD, University, PI, Hobbies, About, Job}, nil
	} else {
		return nil, err
	}
}