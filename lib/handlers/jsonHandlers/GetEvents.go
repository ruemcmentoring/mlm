package JSONHandlers

import (
	//our
//	"../../config"
	//customs
	"github.com/gorilla/mux"
	_ "github.com/go-sql-driver/mysql"
	"github.com/antigloss/go/logger"
	//golang
	"database/sql"
	"net/http"
	"encoding/json"
	"strconv"
)

func GetEventsHandler(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")

	//Get needed vars
	vars := mux.Vars(r)
	id, err := strconv.Atoi(vars["programid"])
	if err != nil {
		logger.Error("ERROR : ", err)
		w.Write(EmptyEventInfo())
		return
	}

	//Get Events struct
	events, err := GetEvents(id)
	if err != nil {
		logger.Error("ERROR : ", err)
		w.Write(nil)
		return
	}

	//Encode Events struct
	js, err := json.Marshal(events)
	if err != nil {
		logger.Error("ERROR : ", err)
		w.Write(nil)
		return
	}

	//Send
	w.Write(js)
	return
}


//Get (UserID, Hash, Salt, IsConfirmed)
func GetRowsEvents(programID int, db *sql.DB) (*sql.Rows, error) {
	rows, err := db.Query(`SELECT User.Name, User.SurName, Event.EventID, Event.Date, Event.AddDate, Event.Subject, Event.FromID, Event.Body, Event.LectorID, Event.ImageURL, Event.EventType, Event.Pinned FROM Event INNER JOIN User ON Event.LectorID = User.UserID WHERE Event.ProgramID = ? ORDER BY Pinned DESC, Event.Date DESC, Event.AddDate DESC`, programID)
	return rows, err
}

func GetEvents(programID int) ([]*EventInfo, error) {
	db, err := OpenDB()
	if err != nil {
		return nil, err
	}
	defer db.Close()

	rows, err := GetRowsEvents(programID, db)

	if err != nil {
		return nil, err
	}

	defer rows.Close()

	Events := make([]*EventInfo, 50)
	counter := 0

	for rows.Next() {
		var date, adddate, name, surname, subject, body, photourl sql.NullString

		var EventID	int
		var Date, AddDate string
		var FromID int
		var LectorID int
		var Name, SurName string
		var Subject string
		var Body string
		var PhotoURL string
		var VoteLink string
//		var ProgramID int
		var EventType int
		var Files []*File
		var Pinned bool

		rows.Scan(&name, &surname, &EventID, &date, &adddate, &subject, &FromID, &body, &LectorID, &photourl, &EventType, &Pinned)

		if date.Valid {
			Date = date.String
		} else {
			Date = "Unknown date"
		}

		if adddate.Valid {
			AddDate = adddate.String
		} else {
			AddDate = "Unknown date"
		}

		if name.Valid {
			Name = name.String
		} else {
			Name = "Unknown"
		}

		if surname.Valid {
			SurName = surname.String
		} else {
			SurName = "Unknown"
		}

		if subject.Valid {
			Subject = subject.String
		} else {
			Subject = "No information about event"
		}

		if body.Valid {
			Body = body.String
		} else {
			Body = "No information about event"
		}

		if photourl.Valid {
			PhotoURL = photourl.String
		} else {
			PhotoURL = "images/profilePhoto/NoPhoto.jpeg"
		}

		if (Name != "Unknown" && SurName != "Unknown") {
			Name = Name + " " + SurName
		} else if (Name == "Unknown" && SurName != "Unknown") {
			Name = SurName
		}

		if counter > 49 {
			Events = append(Events, &EventInfo{EventID, Date, AddDate, FromID, LectorID, Name, Subject, Body, PhotoURL, VoteLink, programID, EventType, Files, Pinned})
			counter++
		} else {
			Events[counter] = &EventInfo{EventID, Date, AddDate, FromID, LectorID, Name, Subject, Body, PhotoURL, VoteLink, programID, EventType, Files, Pinned}
			counter++
		}
	}

	if counter < 50 {
		Events = Events[:counter]
	}

	if counter == 0 {
		return nil, nil
	} else {
/*
		for i := 0; i < len(Events)-1; i++ {
			for j:= 0; j<len(Events)-i-1; j++ {
				if Events[j].EventType == 1 && Events[j+1].EventType == 1 && Events[j].Date<Events[j+1].Date {
					tmp := Events[j];
					Events[j] = Events[j+1];
					Events[j+1] = tmp;
				}
			}
		}
*/
		return Events, nil
	}
}