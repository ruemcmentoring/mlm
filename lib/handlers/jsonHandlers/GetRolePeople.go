package JSONHandlers

import (
	//our
	"../../config"
	//customs
	"github.com/gorilla/mux"
	_ "github.com/go-sql-driver/mysql"
	"github.com/antigloss/go/logger"
	//golang
	"strconv"
	"database/sql"
	"net/http"
	"encoding/json"
)

func GetRolePeopleHandler(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")

	//Get needed vars
	vars := mux.Vars(r)
	role := vars["roleid"]

	roleID, err := strconv.Atoi(role)
	if err != nil {
		logger.Error("ERROR : ", err)
		w.Write(EmptyRolePeople())
		return
	}

	//Get Auth struct
	rolePeople, err := GetRolePeople(roleID)
	if err != nil {
		logger.Error("ERROR : ", err)
		w.Write(EmptyRolePeople())
		return
	}

	//Encode Auth struct
	js, err := json.Marshal(rolePeople)
	if err != nil {
		logger.Error("ERROR : ", err)
		w.Write(EmptyRolePeople())
		return
	}

	//Send
	w.Write(js)
	return
}


//Get (UserID, Hash, Salt, IsConfirmed)
func GetRowsRolePeople(roleID int, db *sql.DB) (*sql.Rows, error) {
	rows, err := db.Query("SELECT User.UserID, User.Name, User.SurName, User.Sex, User.PhotoURL, User.BirthDate, User.University, User.Phone, User.Email, User.Job FROM User INNER JOIN ProgramUserRoleMap ON ProgramUserRoleMap.UserID = User.UserID WHERE ProgramUserRoleMap.RoleID = ?", roleID)
	return rows, err
}

func GetRolePeople(roleID int) (*RolePeople, error) {
	db, err := OpenDB()
	if err != nil {
		return nil, err
	}
	defer db.Close()

	rows, err := GetRowsRolePeople(roleID, db)

	if err != nil {
		return nil, err
	}

	defer rows.Close()

	Users := make([]*User, 10)
	counter := 0

	for rows.Next() { //means this user is already registered
		var email, name, surname, sex, bd, phone, photourl, university, job sql.NullString
		var Email, Name, SurName, Sex, BD, Phone, PhotoURL, University, Job string
		var UserID int

		rows.Scan(&UserID, &name, &sex, &surname, &photourl, &bd, &university, &phone, &email, &job)

		if email.Valid {
			Email = email.String
		} else {
			Email = "No email"
		}

		if name.Valid {
			Name = name.String
		} else {
			Name = "No name"
		}

		if surname.Valid {
			SurName = surname.String
		} else {
			SurName = "No surname"
		}

		if sex.Valid {
			Sex = sex.String
		} else {
			Sex = "Shemale"
		}

		if bd.Valid {
			BD = bd.String
		} else {
			BD = "No birth date"
		}

		if phone.Valid {
			Phone = phone.String
		} else {
			Phone = "No phone"
		}

		if photourl.Valid {
			PhotoURL = photourl.String
		} else {
			PhotoURL = "images/profilePhoto/NoPhoto.jpeg"
		}

		if university.Valid {
			University = university.String
		} else {
			University = "No university"
		}

		if job.Valid {
			Job = job.String
		} else {
			Job = "No job"
		}


		if counter > 9 {
			Users = append(Users, &User{UserID, Name, SurName, Sex, PhotoURL, BD, University, Phone, Email, Job})
			counter++
		} else {
			Users[counter] = &User{UserID, Name, SurName, Sex, PhotoURL, BD, University, Phone, Email, Job}
			counter++
		}
	}

	if counter < 10 {
		Users = Users[:counter]
	}

	var RoleStr string

	switch roleID {
		case config.Admin:
			RoleStr = "Admin"
		case config.Student:
			RoleStr = "Student"
		case config.Candidate:
			RoleStr = "Candidate"
		case config.Mentee:
			RoleStr = "Mentee"
		case config.Employee:
			RoleStr = "Employee"
		case config.Lector:
			RoleStr = "Lector"
		case config.Consultant:
			RoleStr = "Consultant"
		case config.Mentor:
			RoleStr = "Mentor"
		case config.Inactive:
			RoleStr = "Inactive"
		default:
			RoleStr = "Unknown"
	}

	if counter == 0 {
		return nil, nil
	} else {
		return &RolePeople{roleID, RoleStr, Users}, nil
	}
}