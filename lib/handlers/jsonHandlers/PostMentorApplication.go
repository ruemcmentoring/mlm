package JSONHandlers

import (
	//customs
	_ "github.com/go-sql-driver/mysql"
	//golang
	"net/http"
	"encoding/json"
)

func PostMentorApplicationHandler(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")

	//Decode JSON	
	decoder := json.NewDecoder(r.Body)
    var mentorApplication MentorApplication
    err := decoder.Decode(&mentorApplication)
    if err != nil {
    	w.Write(JSONErr(err.Error()))
    	return
    }

    //Post mentorApplication to DB
    err = UpdateMentorApplication(mentorApplication)
    if err != nil {
    	w.Write(JSONErr(err.Error()))
		return
    }

    //Send OK result
	w.Write(JSONOk())
	return

}

func UpdateMentorApplication(mentorApplication MentorApplication) error {
	db, err := OpenDB()
	if err != nil {
		return err
	}
	defer db.Close()
	
	_, err = db.Query("UPDATE MentorApplication SET PreferredUniversity=?, PreferredSex=?, PreferredInterests=?, LectureWish=?, ReadyForTwo=? WHERE UserID=?", mentorApplication.PreferredUniversity, mentorApplication.PreferredSex, mentorApplication.PreferredInterests, mentorApplication.LectureWish, mentorApplication.ReadyForTwo, mentorApplication.UserID)
	return err
}