package JSONHandlers

import (
	//customs
	_ "github.com/go-sql-driver/mysql"
	//golang
	"net/http"
	"encoding/json"
)

func PostApplicationHandler(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")

	//Decode JSON
	decoder := json.NewDecoder(r.Body)
    var application Application
    err := decoder.Decode(&application)
    if err != nil {
    	w.Write(JSONErr(err.Error()))
		return
    }

    //Post application to DB
    err = UpdateApplication(application)
    if err != nil {
    	w.Write(JSONErr(err.Error()))
		return
    }

    //Send OK result
	w.Write(JSONOk())
	return

}

func UpdateApplication(application Application) error {
	db, err := OpenDB()
	if err != nil {
		return err
	}
	defer db.Close()
	
	_, err = db.Query("UPDATE Application SET ProfessionalInterests=?, Hobbies=?, About=? WHERE UserID=?", application.PI, application.Hobbies, application.About, application.UserID)
	return err
}