package JSONHandlers

import (
	//our
//	"../../config"
	//customs
	"github.com/gorilla/mux"
	_ "github.com/go-sql-driver/mysql"
	"github.com/antigloss/go/logger"
	//golang
	"database/sql"
	"net/http"
	"encoding/json"
)

func GetAuthHandler(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")

	//Get needed vars
	vars := mux.Vars(r)
	email := vars["email"]

	//Get Auth struct
	auth, err := GetAuth(email)
	if err != nil {
		logger.Error("ERROR : ", err)
		w.Write(EmptyAuth())
		return
	}

	//Encode Auth struct
	js, err := json.Marshal(auth)
	if err != nil {
		logger.Error("ERROR : ", err)
		w.Write(EmptyAuth())
		return
	}

	//Send
	w.Write(js)
	return
}


//Get (UserID, Hash, Salt, IsConfirmed)
func GetRowsAuth(email string, db *sql.DB) (*sql.Rows, error) {
	rows, err := db.Query("SELECT User.UserID, Hash, Salt, IsConfirmed FROM User INNER JOIN PasswordHash ON User.UserID = PasswordHash.UserID WHERE Email = ?", email)
	return rows, err
}

func GetAuth(email string) (*Auth, error) {
	db, err := OpenDB()
	if err != nil {
		return nil, err
	}
	defer db.Close()

	rows, err := GetRowsAuth(email, db)

	if err != nil {
		return nil, err
	}

	defer rows.Close()

	if rows.Next() { //means this user is already registered
		var hash string
		var salt string
		var userID int
		var isConfirmed bool

		rows.Scan(&userID, &hash, &salt, &isConfirmed)
	
		return &Auth{userID, email, hash, salt, isConfirmed}, nil
	} else {
		return nil, nil
	}
}