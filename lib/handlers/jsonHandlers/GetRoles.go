package JSONHandlers

import (
	//our
//	"../../config"
	//customs
	_ "github.com/go-sql-driver/mysql"
	//golang
	"database/sql"
	"net/http"
	"encoding/json"
	"strconv"
)

func GetRolesHandler(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")

	//Get Roles struct
	roles, err := GetRoles()
	if err != nil {
		w.Write(EmptyUserInfo())
		return
	}

	//Encode UserInfo struct
	js, err := json.Marshal(roles)
	if err != nil {
		w.Write(EmptyUserInfo())
		return
	}

	//Send
	w.Write(js)
	return
}


//Use not all the fields if needed
//Get (Email, RoleName, Name, BirthDate, Phone, PhotoURL, University)
func GetRowsRoles(db *sql.DB) (*sql.Rows, error) {
	rows, err := db.Query(`SELECT
			Role.RoleID,
			Role.Name
			FROM Role`)
	return rows, err
}

func GetRoles() ([]*RoleInfo, error) {
	db, err := OpenDB()
	if err != nil {
		return nil, err
	}
	defer db.Close()

	rows, err := GetRowsRoles(db)

	if err != nil {
		return nil, err
	}

	defer rows.Close()

	Roles := make([]*RoleInfo, 10)
	counter := 0
	
	for rows.Next() {
		var name, roleid sql.NullString
		var Name string
		var RoleID int

		rows.Scan(&roleid, &name)

		if name.Valid {
			Name = name.String
		} else {
			Name = "Unknown"
		}

		if roleid.Valid {
			RoleID, _ = strconv.Atoi(roleid.String)
		} else {
			RoleID = -1
		}

		if counter > 9 {
			Roles = append(Roles, &RoleInfo{RoleID, Name})
			counter++
		} else {
			Roles[counter] = &RoleInfo{RoleID, Name}
			counter++
		}
	}

	if counter < 10 {
		Roles = Roles[:counter]
	}

	if counter == 0 {
		return nil, nil
	} else {
		return Roles, nil
	}
}