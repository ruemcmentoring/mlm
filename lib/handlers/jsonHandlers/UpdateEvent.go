package JSONHandlers

import (
	//customs
	_ "github.com/go-sql-driver/mysql"
	//golang
	"net/http"
	"encoding/json"
	"fmt"
)

func UpdateEventHandler(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")

	//Decode JSON	
	decoder := json.NewDecoder(r.Body)
    var eventInfo EventInfo
    err := decoder.Decode(&eventInfo)
    if err != nil {
    	w.Write(JSONErr(err.Error()))
    	return
    }

    fmt.Println("Decoded event")

    //Post user to DB
    err = UpdateEventInfo(eventInfo)
    if err != nil {
    	fmt.Println(err)
    	w.Write(JSONErr(err.Error()))
    	return
    }

    fmt.Println("Updated")

    //Send OK result
	w.Write(JSONOk())
	return

}

//USE ONLY WITH ALL THE FIELDS FILLED RIGHT!!!
func UpdateEventInfo(eventInfo EventInfo) error {
	fmt.Println(eventInfo)
	db, err := OpenDB()
	if err != nil {
		return err
	}
	defer db.Close()
	
	if (eventInfo.LectorID == 0) {
		eventInfo.LectorID = 1
	}

	if (eventInfo.FromID == 0) {
		eventInfo.FromID = 1
	}

	if eventInfo.PhotoURL == "" {
		fmt.Println("Query with empty photo")
		db.Query("UPDATE Event SET Date=?, LectorID=?, Subject=?, Body=?, VoteJSON=? WHERE EventID=?", eventInfo.Date, eventInfo.LectorID, eventInfo.Subject, eventInfo.Body, eventInfo.VoteLink, eventInfo.EventID)
	} else {
		fmt.Println("Query with photo")
		db.Query("UPDATE Event SET Date=?, LectorID=?, Subject=?, Body=?, ImageURL=?, VoteJSON=? WHERE EventID=?", eventInfo.Date, eventInfo.LectorID, eventInfo.Subject, eventInfo.Body, eventInfo.PhotoURL, eventInfo.VoteLink, eventInfo.EventID)
	}


	return err
}