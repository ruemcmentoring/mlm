package JSONHandlers

import (
	//our
	"../../config"
	//customs
//	"github.com/gorilla/sessions"
	"github.com/gorilla/mux"
	_ "github.com/go-sql-driver/mysql"
	//golang
	"database/sql"
	"net/http"
	"encoding/json"
	"strconv"
)

func GetUserInfosHandler(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")

	//Get needed vars
	vars := mux.Vars(r)
	programID, _ := strconv.Atoi(vars["programid"])

	//Get UserInfo struct
	userInfos, err := GetUserInfos(programID)
	if err != nil {
		w.Write(EmptyUserInfo())
		return
	}

	//Encode UserInfo struct
	js, err := json.Marshal(userInfos)
	if err != nil {
		w.Write(EmptyUserInfo())
		return
	}

	//Send
	w.Write(js)
	return
}


//Use not all the fields if needed
//Get (Email, RoleName, Name, BirthDate, Phone, PhotoURL, University)
func GetRowsUserInfos(programID int, db *sql.DB) (*sql.Rows, error) {
	rows, err := db.Query(`SELECT
			User.UserID, User.Email,
			Role.RoleID, Role.Name,
			User.Name, User.SurName, User.Sex, User.BirthDate, User.Phone, User.PhotoURL, User.University,
			Application.Hobbies, Application.About, Application.ProfessionalInterests,
			User.Job
			FROM User
			INNER JOIN Application ON User.UserID = Application.UserID
			INNER JOIN ProgramUserRoleMap ON User.UserID = ProgramUserRoleMap.UserID
			INNER JOIN Role ON ProgramUserRoleMap.RoleID = Role.RoleID
			WHERE ProgramUserRoleMap.ProgramID = ?
			ORDER BY User.SurName, User.Name DESC`, programID)
	return rows, err
}

func GetUserInfos(programID int) ([]*UserInfo, error) {
	db, err := OpenDB()
	if err != nil {
		return nil, err
	}
	defer db.Close()

	rows, err := GetRowsUserInfos(programID, db)

	if err != nil {
		return nil, err
	}

	defer rows.Close()

	Users := make([]*UserInfo, 100)
	counter := 0
	
	for rows.Next() {
		var userid, email, roleid, rolestr, name, surname, sex, bd, phone, photourl, university, hobbies, about, pi, job sql.NullString
		var Email, RoleStr, Name, SurName, Sex, BD, Phone, PhotoURL, University, Hobbies, About, PI, Job string
		var UserID, RoleID int

		rows.Scan(&userid, &email, &roleid, &rolestr, &name, &surname, &sex, &bd, &phone, &photourl, &university, &hobbies, &about, &pi, &job)

		if userid.Valid {
			UserID, _ = strconv.Atoi(userid.String)
		} else {
			UserID = 0
		}

		if email.Valid {
			Email = email.String
		} else {
			Email = "No email"
		}

		if roleid.Valid {
			RoleID, _ = strconv.Atoi(roleid.String)
		} else {
			RoleID = config.Inactive
		}

		if rolestr.Valid {
			RoleStr = rolestr.String
		} else {
			RoleStr = "No role"
		}

		if name.Valid {
			Name = name.String
		} else {
			Name = "No name"
		}

		if surname.Valid {
			SurName = surname.String
		} else {
			SurName = "No surname"
		}

		if sex.Valid {
			Sex = sex.String
		} else {
			Sex = "Male"
		}
	
		if bd.Valid {
			BD = bd.String
		} else {
			BD = "No birth date"
		}

		if phone.Valid {
			Phone = phone.String
		} else {
			Phone = "No phone"
		}

		if photourl.Valid {
			PhotoURL = photourl.String
		} else {
			PhotoURL = "images/profilePhoto/NoPhoto.jpeg"
		}

		if university.Valid {
			University = university.String
		} else {
			University = "No university"
		}

		if hobbies.Valid {
			Hobbies = hobbies.String
		} else {
			Hobbies = "No hobbies"
		}

		if about.Valid {
			About = about.String
		} else {
			About = "No about info"
		}

		if pi.Valid {
			PI = pi.String
		} else {
			PI = "No interests"
		}

		if job.Valid {
			Job = job.String
		} else {
			Job = "No job"
		}

		if counter > 99 {
			Users = append(Users, &UserInfo{UserID, Email, Name, SurName, Sex, RoleID, RoleStr, PhotoURL, Phone, BD, University, PI, Hobbies, About, Job})
			counter++
		} else {
			Users[counter] = &UserInfo{UserID, Email, Name, SurName, Sex, RoleID, RoleStr, PhotoURL, Phone, BD, University, PI, Hobbies, About, Job}
			counter++
		}

	}
	
	if counter < 100 {
		Users = Users[:counter]
	}

	if counter == 0 {
		return nil, nil
	} else {
		return Users, nil
	}
}