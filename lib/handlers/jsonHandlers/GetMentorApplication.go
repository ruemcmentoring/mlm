package JSONHandlers

import (
	//customs
//	"github.com/gorilla/sessions"
	"github.com/gorilla/mux"
	_ "github.com/go-sql-driver/mysql"
	//golang
	"database/sql"
	"net/http"
	"encoding/json"
	"strconv"
)

func GetMentorApplicationHandler(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")

	//Get needed vars
	vars := mux.Vars(r)
	userID, _ := strconv.Atoi(vars["userid"])

	//Get UserInfo struct
	mentorApplication, err := GetMentorApplication(userID)
	if err != nil {
		w.Write(EmptyMentorApplication())
		return
	}

	//Encode UserInfo struct
	js, err := json.Marshal(mentorApplication)
	if err != nil {
		w.Write(EmptyMentorApplication())
		return
	}

	//Send
	w.Write(js)
	return
}


//Use not all the fields if needed
//Get (Email, RoleName, Name, BirthDate, Phone, PhotoURL, University)
func GetRowsMentorApplication(userID int, db *sql.DB) (*sql.Rows, error) {
	rows, err := db.Query(`SELECT
			PreferredUniversity,
			PreferredSex,
			PreferredInterests,
			LectureWish,
			ReadyForTwo
		FROM MentorApplication
		WHERE UserID = ?`, userID)			
	return rows, err
}

func GetMentorApplication(userID int) (*MentorApplication, error) {
	db, err := OpenDB()
	if err != nil {
		return nil, err
	}
	defer db.Close()

	rows, err := GetRowsMentorApplication(userID, db)

	if err != nil {
		return nil, err
	}
	
	if rows.Next() {
		var prefUniversity, prefSex, prefInterests, lectureWish, readyForTwo sql.NullString
		var PrefUniversity, PrefSex, PrefInterests, LectureWish string
		var ReadyForTwo bool

		rows.Scan(&prefUniversity, &prefSex, &prefInterests, &lectureWish, &readyForTwo)

		if prefUniversity.Valid {
			PrefUniversity = prefUniversity.String
		} else {
			PrefUniversity = "No preferred university"
		}

		if prefSex.Valid {
			PrefSex = prefSex.String
		} else {
			PrefSex = "Male"
		}

		if prefInterests.Valid {
			PrefInterests = prefInterests.String
		} else {
			PrefInterests = "No preferred interests"
		}

		if lectureWish.Valid {
			LectureWish = lectureWish.String
		} else {
			LectureWish = "No lecture wish"
		}

		if readyForTwo.Valid {
			temp, _ := strconv.Atoi(readyForTwo.String)
			ReadyForTwo = (temp != 0)
		} else {
			ReadyForTwo = false
		}

		return &MentorApplication{userID, PrefUniversity, PrefSex, PrefInterests, LectureWish, ReadyForTwo}, nil
	} else {
		return nil, err
	}
}