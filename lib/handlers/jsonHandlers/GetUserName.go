package JSONHandlers

import (
	//customs
	"github.com/gorilla/mux"
	_ "github.com/go-sql-driver/mysql"
	//golang
	"database/sql"
	"net/http"
	"encoding/json"

	"fmt"
)

func GetUserNameHandler(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")

	//Get needed vars
	vars := mux.Vars(r)
	userid := vars["userid"]

	//Get UserName struct
	name, err := GetUserName(userid)
	if err != nil {
		w.Write(EmptyUserName())
		return
	}
	
	//Encode UserName struct
	js, err := json.Marshal(name)
	if err != nil {
		fmt.Println("writing empty")
		w.Write(EmptyUserName())
		return
	}

	//Send
	w.Write(js)
	return
}


//Get (Name)
func GetRowsNameSurName(userid string, db *sql.DB) (*sql.Rows, error) {
	rows, err := db.Query("SELECT Name, SurName FROM User WHERE UserID = ?", userid)
	return rows, err
}

func GetUserName(userid string) (*UserName, error) {
	db, err := OpenDB()
	if err != nil {
		return nil, err
	}
	defer db.Close()

	rows, err := GetRowsNameSurName(userid, db)

	var name, surname sql.NullString
	var Name, SurName string

	if err != nil {
		return nil, err
	}
	if rows.Next() {
		rows.Scan(&name, &surname)
		if name.Valid {
			Name = name.String
		} else {
			Name = "No Name"
		}

		if surname.Valid {
			SurName = surname.String
		} else {
			SurName = "No SurName"
		}

		return &UserName{Name, SurName}, err
	}
	return nil, err
}