package JSONHandlers

import (
	//customs
	_ "github.com/go-sql-driver/mysql"
	//golang
	"net/http"
	"encoding/json"
)

func PostAuthHandler(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")

	//Decode JSON
	decoder := json.NewDecoder(r.Body)
    var auth Auth  
    err := decoder.Decode(&auth)
    if err != nil {
	    w.Write(JSONErr(err.Error()))
	    return
    }

    //Get UserID for next actions
    userID, err := GetUserID(auth.Email)
    if err != nil {
    	w.Write(JSONErr(err.Error()))
    	return
    }

    //Post Hash to DB
    err = UpdateHash(userID, auth.Salt, auth.Hash)
    if err != nil {
    	w.Write(JSONErr(err.Error()))
    	return
    }

    //Post IsConfirmed to DB
    err = UpdateIsConfirmed(userID, auth.IsConfirmed)
    if err != nil {
    	w.Write(JSONErr(err.Error()))
    	return
    }

    //Send OK result
	w.Write(JSONOk())
	return

}

//Updates Hash (also from null)
func UpdateHash(id int, salt string, hash string) error {
	db, err := OpenDB()
	if err != nil {
		return err
	}
	defer db.Close()

	_, err = db.Query("UPDATE PasswordHash SET Salt=?, Hash=? WHERE UserID=?", salt, hash, id)
	return err
}

//Updates IsConfirmed
func UpdateIsConfirmed(id int, confirmed bool) error {
	db, err := OpenDB()
	if err != nil {
		return err
	}
	defer db.Close()
	
	_, err = db.Query("UPDATE User SET IsConfirmed=? WHERE UserID=?", confirmed, id)
	return err
}