package JSONHandlers

import (
	//our
//	"../../config"
	//customs
	"github.com/gorilla/mux"
	_ "github.com/go-sql-driver/mysql"
	"github.com/antigloss/go/logger"
	//golang
	"database/sql"
	"net/http"
	"encoding/json"
	"strconv"
)

func GetEventHandler(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")

	//Get needed vars
	vars := mux.Vars(r)
	id, err := strconv.Atoi(vars["id"])
	if err != nil {
		logger.Error("ERROR : ", err)
		w.Write(EmptyEventInfo())
		return
	}

	//Get EventInfo struct
	event, err := GetEvent(id)
	if err != nil {
		logger.Error("ERROR : ", err)
		w.Write(EmptyEventInfo())
		return
	}

	//Encode EventInfo struct
	js, err := json.Marshal(event)
	if err != nil {
		logger.Error("ERROR : ", err)
		w.Write(EmptyEventInfo())
		return
	}

	//Send
	w.Write(js)
	return
}


//Get (UserID, Hash, Salt, IsConfirmed)
func GetRowsEvent(id int, db *sql.DB) (*sql.Rows, error) {
	rows, err := db.Query(`SELECT User.Name, User.SurName, 
		Event.EventID, Event.Date, Event.AddDate, Event.Subject, Event.FromID, Event.Body, Event.LectorID, Event.ImageURL, Event.EventType, Event.Pinned
		FROM Event INNER JOIN User ON Event.LectorID = User.UserID WHERE EventID = ?`, id)
	return rows, err
}

func GetEvent(id int) (*EventInfo, error) {
	db, err := OpenDB()
	if err != nil {
		return nil, err
	}
	defer db.Close()

	rows, err := GetRowsEvent(id, db)

	if err != nil {
		return nil, err
	}

	defer rows.Close()

	if rows.Next() { //means this user is already registered
		var date, adddate, name, surname, subject, body, photourl sql.NullString

		var EventID	int
		var Date, AddDate string
		var FromID int
		var LectorID int
		var Name, SurName string
		var Subject string
		var Body string
		var PhotoURL string
		var VoteLink string
//		var ProgramID int
		var EventType int
		var Files []*File
		var Pinned bool

		rows.Scan(&name, &surname, &EventID, &date, &adddate, &subject, &FromID, &body, &LectorID, &photourl, &EventType, &photourl, &Pinned)

		if date.Valid {
			Date = date.String
		} else {
			Date = "Unknown date"
		}

		if adddate.Valid {
			AddDate = adddate.String
		} else {
			AddDate = "Unknown date"
		}

		if name.Valid {
			Name = name.String
		} else {
			Name = "Unknown"
		}

		if surname.Valid {
			SurName = surname.String
		} else {
			SurName = "Unknown"
		}

		if subject.Valid {
			Subject = subject.String
		} else {
			Subject = "No information about event"
		}

		if body.Valid {
			Body = body.String
		} else {
			Body = "No information about event"
		}

		if photourl.Valid {
			PhotoURL = photourl.String
		} else {
			PhotoURL = "images/profilePhoto/NoPhoto.jpeg"
		}
	
		return &EventInfo{EventID, Date, AddDate, FromID, LectorID, Name+SurName, Subject, Body, PhotoURL, VoteLink, 0, EventType, Files, Pinned}, nil
	} else {
		return nil, nil
	}
}