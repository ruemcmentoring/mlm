package Handlers

import (
	//our
	"../config"
	//customs
	"github.com/antigloss/go/logger"
	//golang
	"fmt"
	"net/http"
)

func AboutViewHandler(w http.ResponseWriter, r *http.Request) {
	//Get title of page
	title := r.URL.Path[1:] //[1:] - is to delete "/"
	if title == "" {
		title = config.StartPage
	}
	title = config.WebAppex + title

	if config.DEBUG {
		fmt.Println("AboutView Handler handling " + title)
	}

	logger.Info("Sending... ", title)

	//Serving
	p, err := loadPage(title)
	if err != nil {
		logger.Error("ERROR : ", err.Error())
		p, _ := loadPage(config.Page404)
		fmt.Fprintf(w, "%s", p.body) //wired way to send HTML page
		return                       //just return because of printing dry p.body
	}
	http.ServeFile(w, r, p.dir)
	logger.Info(p.dir, " has been sent")
}
