package Handlers

import (
	//our
	"../config"
	"../sessfunc"
	"./jsonHandlers"
	//customs
	"github.com/gorilla/sessions"
	//golang
	"fmt"
	"html/template"
	"net/http"
	"strconv"
	"encoding/json"
)

func ApplicationViewHandler(w http.ResponseWriter, r *http.Request) {
	//Get title of page
	title := r.URL.Path[1:] //[1:] - is to delete "/"
	title = config.WebAppex + title

	if config.DEBUG {
		fmt.Println("Application Handler handling " + title)
	}

	//Get session
	store := sessions.NewCookieStore([]byte(config.StoreSecretKey))
	userSession, err := store.Get(r, config.UserSessionKey)
	if err != nil {
		config.SendError(w, r, err.Error(), config.InvalidSession)
		return
	}


	isConfirmed := false

	if !userSession.IsNew {
		//Get Auth struct via JSON
		jsonResp, err := http.Get(config.Api + "getauth/email=" + userSession.Values["email"].(string))
		if err != nil {
			config.SendError(w, r, err.Error(), config.GetJSONError)
			return
		}

		//Decode JSON
		decoder := json.NewDecoder(jsonResp.Body)
		
		var auth JSONHandlers.Auth
		err = decoder.Decode(&auth)
		if err != nil {
			config.SendError(w, r, err.Error(), config.DecodeJSONError)
			return
		}

		isConfirmed = auth.IsConfirmed
	} else {
		//Deleting (actually, marking)
		sessfunc.KillSession(userSession)

		sessions.Save(r, w)

		fmt.Println("Error, can't apply")

		http.Redirect(w, r, config.StartPage, 307)

		return
	}


	//Get Roles struct via JSON
	jsonResp, err := http.Get(config.Api + "getroles");
	if err != nil {
		config.SendError(w, r, err.Error(), config.GetJSONError)
		return
	}

	//Decode JSON
	decoder := json.NewDecoder(jsonResp.Body)
	var roles []*JSONHandlers.RoleInfo
	err = decoder.Decode(&roles)
	if err != nil {
		config.SendError(w, r, err.Error(), config.DecodeJSONError)
		return
	}


	//You cant get there if you are not logged in
	//if you can apply but still not confirmed (after registration or failure)
	if !isConfirmed && config.RoleApplication(userSession.Values["roleid"].(int)) {

		//Parsing HTML
		t, err := template.ParseFiles(config.HOMEDIR + title)
		if err != nil { //here we have to send 404 error
			config.SendError(w, r, err.Error(), config.ParseTemplateError)
			return
		}

		//Creating userVariable
		user := &User{
			RoleID:				userSession.Values["roleid"].(int),
			RoleStr:			userSession.Values["role"].(string),
			ID:					userSession.Values["id"].(int),
			Email:				userSession.Values["email"].(string),
			Name:				userSession.Values["name"].(string),
			PhotoURL:			"https://upload.wikimedia.org/wikipedia/commons/3/37/No_person.jpg",
			IsConfirmed:		false,
			Roles:				roles}

		//Killing session
		sessfunc.KillSession(userSession)
		sessions.Save(r, w)

		//Take template t, insert it to w, using user as list of variables there
		t.Execute(w, user)

		return
	//else you are confirmed (want to edit application)
	} else if isConfirmed && config.RoleEditApplication(userSession.Values["roleid"].(int)) {

			//Parsing HTML
			t, err := template.ParseFiles(config.HOMEDIR + title)
			if err != nil { //here we have to send 404 error
				config.SendError(w, r, err.Error(), config.ParseTemplateError)
				return
			}

			//Get UserInfo struct via JSON
			jsonResp, err := http.Get(config.Api + "getuserinfo/userid=" + strconv.Itoa(userSession.Values["id"].(int)) + "&programid=" + strconv.Itoa(config.ProgramID))
			if err != nil {
				config.SendError(w, r, err.Error(), config.GetJSONError)
				return
			}

			//Decode JSON
			decoder := json.NewDecoder(jsonResp.Body)
			var userInfo JSONHandlers.UserInfo
			err = decoder.Decode(&userInfo)
			if err != nil {
				config.SendError(w, r, err.Error(), config.DecodeJSONError)
				return
			}

			//Get MentorApplication struct via JSON
			jsonResp, err = http.Get(config.Api + "getmentorapp/userid=" + strconv.Itoa(userSession.Values["id"].(int)))
			if err != nil {
				config.SendError(w, r, err.Error(), config.GetJSONError)
				return
			}

			//Decode JSON
			decoder = json.NewDecoder(jsonResp.Body)
			var mentorApplication JSONHandlers.MentorApplication
			err = decoder.Decode(&mentorApplication)
			if err != nil {
				config.SendError(w, r, err.Error(), config.DecodeJSONError)
				return
			}

			//Creating userVariable
				user := &User{
				RoleID:				userSession.Values["roleid"].(int),
				RoleStr:			userSession.Values["role"].(string),
				ID:					userSession.Values["id"].(int),
				Email:				userSession.Values["email"].(string),
				Name:				userSession.Values["name"].(string),
				SurName:			userInfo.SurName,
				Sex:				userInfo.Sex,
				PhotoURL:			userInfo.PhotoURL,
				Phone:				userInfo.Phone,
				BirthDate:			userInfo.BD,
				Job:				userInfo.Job,
				University:			userInfo.University,
				PI:					userInfo.PI,
				Hobbies:			userInfo.Hobbies,
				About:				userInfo.About,
				PrefUniversity:		mentorApplication.PreferredUniversity,
				PrefGender:			mentorApplication.PreferredSex,
				PrefPI:				mentorApplication.PreferredInterests,
				LectureWish:		mentorApplication.LectureWish,
				WishForTwo:			mentorApplication.ReadyForTwo,
				IsConfirmed:		true,
				Roles:				roles}
fmt.Println(user)
			//Take template t, insert it to w, using user as list of variables there
			t.Execute(w, user)

			return

	//else delete session and redirect to start page
	} else {
		//Deleting (actually, marking)
		sessfunc.KillSession(userSession)

		sessions.Save(r, w)

		fmt.Println("Error, can't apply")

		http.Redirect(w, r, config.StartPage, 307)
	}

	return
}


type User struct {
	RoleID int
	RoleStr string
	ID int
	Email string
	Name string
	SurName string
	Sex string
	PhotoURL string
	Phone string
	BirthDate string
	Job string
	University string
	PI string
	Hobbies string
	About string
	PrefUniversity string
	PrefGender string
	PrefPI string
	LectureWish string
	WishForTwo bool
	IsConfirmed bool

	EditorID int
	Roles []*JSONHandlers.RoleInfo
}



/*
	register -> application (not confirmed) -> afterlog (all filled)
	register -> application (not confirmed) -> crash (not all filled) ....
		login -> application (not confirmed) -> afterlog (all filled)
	something -> application (confirmed) -> index (=dash)
*/