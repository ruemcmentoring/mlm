package Handlers

import (
	//our
	"../config"
	"./jsonHandlers"
	//customs
	"github.com/antigloss/go/logger"
	"github.com/gorilla/sessions"
	//golang
	"encoding/json"
	"fmt"
	"net/http"
)

func IndexViewHandler(w http.ResponseWriter, r *http.Request) {
	//Get title of page
	title := r.URL.Path[1:] //[1:] - is to delete "/"
	if title == "" {
		title = config.StartPage
	}
	title = config.WebAppex + title

	if config.DEBUG {
		fmt.Println("IndexView Handler handling " + title)
	}

	//Get session
	store := sessions.NewCookieStore([]byte(config.StoreSecretKey))
	userSession, err := store.Get(r, config.UserSessionKey)
	if err != nil {
		config.SendError(w, r, err.Error(), config.InvalidSession)
		return
	}

	//If logged...
	if !userSession.IsNew {
		if config.DEBUG {
			fmt.Println("user session is not new")
		}

		//Get Auth struct via JSON
		jsonResp, err := http.Get(config.Api + "getauth/email=" + userSession.Values["email"].(string))
		if err != nil {
			config.SendError(w, r, err.Error(), config.GetJSONError)
			return
		}

		//Decode JSON
		decoder := json.NewDecoder(jsonResp.Body)
		var auth JSONHandlers.Auth
		err = decoder.Decode(&auth)
		if err != nil {
			config.SendError(w, r, err.Error(), config.DecodeJSONError)
			return
		}

		//If confirmed, then redirect to Profile
		if auth.IsConfirmed {
			if config.DEBUG {
				fmt.Println("To dashboard...")
			}
			http.Redirect(w, r, config.DashByRole(userSession.Values["roleid"].(int)), 307)
			return
		}
		//Else redirect to afterlog
		if config.DEBUG {
			fmt.Println("To afterlog...")
		}
		http.Redirect(w, r, config.AfterlogInfoPage, 307)
		return
		//If not logged then serve index.html
	} else {
		logger.Info("Sending... ", title)

		p, err := loadPage(title)
		if err != nil {
			logger.Error("ERROR : ", err.Error())
			p, _ := loadPage(config.Page404)
			fmt.Fprintf(w, "%s", p.body) //wired way to send HTML page
			return                       //just return because of printing dry p.body
		}

		http.ServeFile(w, r, p.dir)
		logger.Info(p.dir, " has been sent")

		return
	}
}
