package Handlers

import (
	//our
	"../config"
	"../sessfunc"
	"./jsonHandlers"
	//customs
	"github.com/antigloss/go/logger"
	"github.com/gorilla/sessions"
	//golang
	"fmt"
	"net/http"
	"html/template"
	"encoding/json"
)

func LoginViewHandler(w http.ResponseWriter, r *http.Request) {
	//Get title of page
	title := r.URL.Path[1:] //[1:] - is to delete "/"
	title = config.WebAppex + title

	if config.DEBUG {
		fmt.Println("LoginView Handler handling " + title)
	}

	//Getting session
	store := sessions.NewCookieStore([]byte(config.StoreSecretKey))
	userSession, err := store.Get(r, config.UserSessionKey)
	if err != nil {
		logger.Error("ERROR : ", err)
	}

	confirmSession, err := store.Get(r, config.ConfirmSessionKey)
	if err != nil {
		logger.Error("ERROR : ", err)
	}

	//redirecting if logged in
	if !userSession.IsNew {
		//Cant register or login while having session
		jsonResp, err := http.Get(config.Api + "getauth/email=" + userSession.Values["email"].(string))
		if err != nil {
			fmt.Println(err)
			return
		}

		decoder := json.NewDecoder(jsonResp.Body)
		var auth JSONHandlers.Auth
		err = decoder.Decode(&auth)
		if err != nil {
			fmt.Println(err)
			return
		}

		if auth.IsConfirmed {
			http.Redirect(w, r, config.ProfilePage, 307)
			return
		}
		http.Redirect(w, r, config.AfterlogInfoPage, 307)
		return
	} else {
/*		//If there is no session, make it just through load page (actually all the HTML can be shown by template)
		logger.Info("Sending... ", title)

		p, err := loadPage(title)
		if err != nil {
			logger.Error("ERROR : ", err.Error())
			p, _ := loadPage(config.Page404)
			fmt.Fprintf(w, "%s", p.body) //wired way to send HTML page
			return                       //just return because of printing dry p.body
		}

		http.ServeFile(w, r, p.dir)
		logger.Info(p.dir, " has been sent")*/
		//Parsing HTML
		t, err := template.ParseFiles(config.HOMEDIR + title)
		if err != nil { //here we have to send 404 error
			http.Redirect(w, r, config.Page404, 307)
			return
		}

		if config.DEBUG {
			fmt.Println("Parsed")
		}

		content := struct {
			Content string
		}{
			"",
		}

		if !confirmSession.IsNew {

			if confirmSession.Values["confirmed"] != nil {

				if config.DEBUG {
					fmt.Println("confirmed is not nil")
				}

				if confirmSession.Values["confirmed"].(bool) == true {
					content.Content = "Email was successfully approved, log in!"
				} else {
					content.Content = ""
				}
			}
		}

		//Deleting (actually, marking)
		sessfunc.KillSession(confirmSession)

		sessfunc.KillSession(userSession)

		sessions.Save(r, w)

		if config.DEBUG {
			fmt.Println("executing")
		}

		//Execution
		t.Execute(w, content)

		return
	}
}
