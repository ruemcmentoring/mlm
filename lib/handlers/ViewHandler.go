package Handlers

import (
	//our
	"../config"
	//customs
	"github.com/gorilla/sessions"
	//golang
	"fmt"
	"html/template"
	"net/http"
)

func ViewHandler(w http.ResponseWriter, r *http.Request) {
	//Get title of page
	title := r.URL.Path[1:] //[1:] - is to delete "/"
	if title == "" {
		title = config.StartPage
	}
	title = config.WebAppex + title

	if config.DEBUG {
		fmt.Println("View Handler handling " + title)
	}

	//Get session
	store := sessions.NewCookieStore([]byte(config.StoreSecretKey))
	userSession, err := store.Get(r, config.UserSessionKey)
	if err != nil {
		config.SendError(w, r, err.Error(), config.InvalidSession)
		return
	}

	//If logged...
	if !userSession.IsNew {
		//Parsing HTML
		t, err := template.ParseFiles(config.HOMEDIR + title)
		if err != nil { //here we have to send 404 error
			config.SendError(w, r, err.Error(), config.ParseTemplateError)
			return
		}

		//Creating user variable
		//Just two parameters, cause for nobody can get info from here
		user := struct {
			Email string
			Name  string
		}{
			userSession.Values["email"].(string),
			userSession.Values["name"].(string),
		}

		//Take template t, insert it to w, using user as list of variables there
		t.Execute(w, user)
		return

	//else redirect to start page
	} else {
		http.Redirect(w, r, config.StartPage, 307)
		return
	}
}
