package Handlers

import (
	//our
	"../config"
	"./jsonHandlers"
	//customs
	"github.com/gorilla/sessions"
	//golang
	"fmt"
	"net/http"
	"html/template"
	"encoding/json"
	"strconv"
)

func SearchViewHandler(w http.ResponseWriter, r *http.Request) {
	//Get title of page
	title := r.URL.Path[1:] //[1:] - is to delete "/"
	title = config.WebAppex + title

	if config.DEBUG {
		fmt.Println("SearchView Handler handling " + title)
	}

	//Get session
	store := sessions.NewCookieStore([]byte(config.StoreSecretKey))
	userSession, err := store.Get(r, config.UserSessionKey)
	if err != nil {
		config.SendError(w, r, err.Error(), config.InvalidSession)
		return
	}

	//if logged in...
	if !userSession.IsNew {
		//Get Auth struct via JSON
		jsonResp, err := http.Get(config.Api + "getauth/email=" + userSession.Values["email"].(string))
		if err != nil {
			config.SendError(w, r, err.Error(), config.GetJSONError)
			return
		}

		//Decode JSON
		decoder := json.NewDecoder(jsonResp.Body)
		var auth JSONHandlers.Auth
		err = decoder.Decode(&auth)
		if err != nil {
			config.SendError(w, r, err.Error(), config.DecodeJSONError)
			return
		}

		if !auth.IsConfirmed {
			http.Redirect(w, r, config.AfterlogInfoPage, 307)
			return
		}

	//Else if not logged in...
	} else {
		http.Redirect(w, r, config.StartPage, 307)
		return
	}
		
	//Parsing HTML
	paths := []string {
		config.HOMEDIR + "web/search.html",
		config.HOMEDIR + "web/search_editmodal.html",
		config.HOMEDIR + "web/search_controlmodal.html",
		config.HOMEDIR + "web/search_foot.html",
	}
	fmt.Println(paths);
	t, err := template.ParseFiles(paths...)
	if err != nil { //here we have to send 404 error
		config.SendError(w, r, err.Error(), config.ParseTemplateError)
		return
	}

	//Get UserInfo struct via JSON
	jsonResp, err := http.Get(config.Api + "getuserinfo/userid=" + strconv.Itoa(userSession.Values["id"].(int)) + "&programid=" + strconv.Itoa(config.ProgramID))
	if err != nil {
		config.SendError(w, r, err.Error(), config.GetJSONError)
		return
	}

	//Decode JSON
	decoder := json.NewDecoder(jsonResp.Body)
	var userInfo JSONHandlers.UserInfo
	err = decoder.Decode(&userInfo)
	if err != nil {
		config.SendError(w, r, err.Error(), config.DecodeJSONError)
		return
	}

	//Get Roles struct via JSON
	jsonResp, err = http.Get(config.Api + "getroles");
	if err != nil {
		config.SendError(w, r, err.Error(), config.GetJSONError)
		return
	}

	//Decode JSON
	decoder = json.NewDecoder(jsonResp.Body)
	var roles []*JSONHandlers.RoleInfo
	err = decoder.Decode(&roles)
	if err != nil {
		config.SendError(w, r, err.Error(), config.DecodeJSONError)
		return
	}

	//Get all people from this program
	jsonResp, err = http.Get(config.Api + "getusers/programid=" + strconv.Itoa(config.ProgramID));
	if err != nil {
		config.SendError(w, r, err.Error(), config.GetJSONError)
		return
	}

	//Decode JSON
	decoder = json.NewDecoder(jsonResp.Body)
	var users []*JSONHandlers.UserInfo
	err = decoder.Decode(&users)
	if err != nil {
		config.SendError(w, r, err.Error(), config.DecodeJSONError)
		return
	}

	content := struct {
		User JSONHandlers.UserInfo
		Users []*JSONHandlers.UserInfo
		Roles []*JSONHandlers.RoleInfo
		ProgramStarted bool
	}{
		userInfo,
		users,
		roles,
		true,
	}


	//Deleting (actually, marking)
	sessions.Save(r, w)

	//Execution
	t.Execute(w, content)

	return
}
