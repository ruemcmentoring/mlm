package Handlers

import (
	//our
	"../config"
	"../sessfunc"
	"./jsonHandlers"
	//customs
	"github.com/gorilla/mux"
	"github.com/gorilla/sessions"
	//golang
	"fmt"
	"html/template"
	"net/http"
	"strconv"
	"encoding/json"
)

func OtherApplicationViewHandler(w http.ResponseWriter, r *http.Request) {
	//Get title of page
	title := config.ApplicationPage
	title = config.WebAppex + title

	//Get needed vars
	vars := mux.Vars(r)
	sacrificeid := vars["userid"]

	if config.DEBUG {
		fmt.Println("OtherApplication Handler handling " + title)
	}

	//Get session
	store := sessions.NewCookieStore([]byte(config.StoreSecretKey))
	userSession, err := store.Get(r, config.UserSessionKey)
	if err != nil {
		config.SendError(w, r, err.Error(), config.InvalidSession)
		return
	}


	isConfirmed := false

	if !userSession.IsNew {
		//Get Auth struct via JSON
		jsonResp, err := http.Get(config.Api + "getauth/email=" + userSession.Values["email"].(string))
		if err != nil {
			config.SendError(w, r, err.Error(), config.GetJSONError)
			return
		}

		//Decode JSON
		decoder := json.NewDecoder(jsonResp.Body)
		
		var auth JSONHandlers.Auth
		err = decoder.Decode(&auth)
		if err != nil {
			config.SendError(w, r, err.Error(), config.DecodeJSONError)
			return
		}

		isConfirmed = auth.IsConfirmed
	} else {
		//Deleting (actually, marking)
		sessfunc.KillSession(userSession)

		sessions.Save(r, w)

		fmt.Println("Error, can't apply")

		http.Redirect(w, r, config.Protocol+config.Server+config.StartPage, 307)

		return
	}

	//You cant get there if you are not logged in
	//if you can edit other applications
	if isConfirmed && config.RoleEditOtherApplication(userSession.Values["roleid"].(int)) {

			//Parsing HTML
			t, err := template.ParseFiles(config.HOMEDIR + title)
			if err != nil { //here we have to send 404 error
				config.SendError(w, r, err.Error(), config.ParseTemplateError)
				return
			}

			//Get UserInfo struct via JSON
			jsonResp, err := http.Get(config.Api + "getuserinfo/userid=" + sacrificeid + "&programid=" + strconv.Itoa(config.ProgramID))
			if err != nil {
				config.SendError(w, r, err.Error(), config.GetJSONError)
				return
			}

			//Decode JSON
			decoder := json.NewDecoder(jsonResp.Body)
			var userInfo JSONHandlers.UserInfo
			err = decoder.Decode(&userInfo)
			if err != nil {
				config.SendError(w, r, err.Error(), config.DecodeJSONError)
				return
			}

			//Get MentorApplication struct via JSON
			jsonResp, err = http.Get(config.Api + "getmentorapp/userid=" + sacrificeid)
			if err != nil {
				config.SendError(w, r, err.Error(), config.GetJSONError)
				return
			}

			//Decode JSON
			decoder = json.NewDecoder(jsonResp.Body)
			var mentorApplication JSONHandlers.MentorApplication
			err = decoder.Decode(&mentorApplication)
			if err != nil {
				config.SendError(w, r, err.Error(), config.DecodeJSONError)
				return
			}

			//Get Roles struct via JSON
			jsonResp, err = http.Get(config.Api + "getroles");
			if err != nil {
				config.SendError(w, r, err.Error(), config.GetJSONError)
				return
			}

			//Decode JSON
			decoder = json.NewDecoder(jsonResp.Body)
			var roles []*JSONHandlers.RoleInfo
			err = decoder.Decode(&roles)
			if err != nil {
				config.SendError(w, r, err.Error(), config.DecodeJSONError)
				return
			}

			//Creating userVariable (from ApplicationView)
				user := &User{
				RoleID:				userInfo.RoleID,
				RoleStr:			userInfo.RoleStr,
				ID:					userInfo.UserID,
				Email:				userInfo.Email,
				Name:				userInfo.Name,
				SurName:			userInfo.SurName,
				Sex:				userInfo.Sex,
				PhotoURL:			userInfo.PhotoURL,
				Phone:				userInfo.Phone,
				BirthDate:			userInfo.BD,
				Job:				userInfo.Job,
				University:			userInfo.University,
				PI:					userInfo.PI,
				Hobbies:			userInfo.Hobbies,
				About:				userInfo.About,
				PrefUniversity:		mentorApplication.PreferredUniversity,
				PrefGender:			mentorApplication.PreferredSex,
				PrefPI:				mentorApplication.PreferredInterests,
				LectureWish:		mentorApplication.LectureWish,
				WishForTwo:			mentorApplication.ReadyForTwo,
				IsConfirmed:		true,

				EditorID:			userSession.Values["id"].(int),
				Roles:				roles}
fmt.Println(user)
			//Take template t, insert it to w, using user as list of variables there
			t.Execute(w, user)

			return

	//else delete session and redirect to start page
	} else {
		//Deleting (actually, marking)
		sessfunc.KillSession(userSession)

		sessions.Save(r, w)

		fmt.Println("Error, can't apply")

		http.Redirect(w, r, config.StartPage, 307)
	}

	return
}


/*
	register -> application (not confirmed) -> afterlog (all filled)
	register -> application (not confirmed) -> crash (not all filled) ....
		login -> application (not confirmed) -> afterlog (all filled)
	something -> application (confirmed) -> index (=dash)
*/