package DBHandlers
/*
import (
	//our
	"../../config"
	"../../sessfunc"
	//customs
	"github.com/antigloss/go/logger"
	_ "github.com/go-sql-driver/mysql"
	"github.com/gorilla/sessions"
	//golang
	"database/sql"
	"fmt"
	"html/template"
	"net/http"
)

func ChangeInfoHandler(w http.ResponseWriter, r *http.Request) {
	if config.DEBUG {
		fmt.Println("ChangeInfo Handler handling")
	}

	//Getting session
	store := sessions.NewCookieStore([]byte(config.StoreSecretKey))
	userSession, err := store.Get(r, config.UserSessionKey)
	if err != nil {
		logger.Error("ERROR : ", err)
	}

	//deleting session after filling if logged in
	if !userSession.IsNew {
		//checking confirmation
		if _, err := IsConfirmed(userSession.Values["email"].(string)); err != nil {
			logger.Error("ERROR : ", err)
			http.Redirect(w, r, config.ErrorPage, 307)
			return
		}

		id := userSession.Values["id"].(int)

		//Parsing HTML
		t, err := template.ParseFiles(config.HOMEDIR + config.WebAppex + config.ChangeInfoPage)
		if err != nil {
			logger.Error("ERROR : ", err)
		}

		//getting profile image url
		db, err := sql.Open(config.DBdriver, config.GetDBIdentify())
		if err != nil {
			fmt.Println(err)
			return
		}

		rows, err := db.Query(`SELECT
			Profile.Email,
			Role.Name,
			User.Name, User.BirthDate, User.Phone, User.PhotoURL, User.University,
			Application.Hobbies, Application.About, Application.ProfInterests
			FROM Profile
			INNER JOIN User ON Profile.ID = User.ID
			INNER JOIN Application ON Profile.ID = Application.ID
			INNER JOIN UserRoleMap ON Profile.ID = UserRoleMap.UserID
			INNER JOIN Role ON UserRoleMap.RoleID = Role.ID
			WHERE Profile.ID = ? AND UserRoleMap.ProgramID = ?`, id, config.ProgramID)

		if err != nil {
			logger.Error("ERROR : ", err)
			fmt.Println("ERROR : ", err)
		}

		if rows.Next() {
			var email, rolestr, name, bd, phone, imglink, university, hobbies, about, pi sql.NullString
			var Email, RoleStr, Name, BD, Phone, ImgLink, University, Hobbies, About, PI string

			rows.Scan(&email, &rolestr, &name, &bd, &phone, &imglink, &university, &hobbies, &about, &pi)

			if config.DEBUG {
				fmt.Println(email, rolestr, name, bd)
			}

			if email.Valid {
				Email = email.String
			} else {
				Email = "No email"
			}

			if rolestr.Valid {
				RoleStr = rolestr.String
			} else {
				RoleStr = "No role"
			}

			if name.Valid {
				Name = name.String
			} else {
				Name = "No name"
			}

			if bd.Valid {
				BD = bd.String
			} else {
				BD = "No birth date"
			}

			if phone.Valid {
				Phone = phone.String
			} else {
				Phone = "No phone"
			}

			if imglink.Valid {
				ImgLink = imglink.String
			} else {
				ImgLink = "images/profilePhoto/NoPhoto.jpeg"
			}

			if university.Valid {
				University = university.String
			} else {
				University = "No university"
			}

			if hobbies.Valid {
				Hobbies = hobbies.String
			} else {
				Hobbies = "No hobbies"
			}

			if about.Valid {
				About = about.String
			} else {
				About = "No about info"
			}

			if pi.Valid {
				PI = pi.String
			} else {
				PI = "No interests"
			}

			//Creating user variable
			if id == userSession.Values["id"].(int) { //user wants his profile page
				user := struct {
					Email      string
					Name       string
					RoleStr    string
					ImgLink    string
					Phone      string
					BD         string
					Univer     string
					PI         string
					Hobbies    string
					About      string
					Visibility string
					ID         int
				}{
					Email,
					Name,
					RoleStr,
					ImgLink,
					Phone,
					BD,
					University,
					PI,
					Hobbies,
					About,
					config.NoneVis,
					id,
				}
				//Take template t, insert it to w, using user as list of variables there
				t.Execute(w, user)
			} else { //else delete session and redirect to start page
				//Deleting (actually, marking)
				sessfunc.KillSession(userSession)
				sessions.Save(r, w)
				http.Redirect(w, r, config.StartPage, 307)
			}
		}
	}
}
*/