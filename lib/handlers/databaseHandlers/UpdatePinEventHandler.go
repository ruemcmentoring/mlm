package DBHandlers

import (
	//our
	"../../config"
	//customs
	"github.com/gorilla/mux"
	"github.com/gorilla/sessions"
	//golang
	"fmt"
	"net/http"
)

func PinEventHandler(w http.ResponseWriter, r *http.Request) {
	if config.DEBUG {
		fmt.Println("Updatepin event Handler")
	}

	//Creating error session in case of error occuring. If there is no error, session wil  be killed in the end
	store := sessions.NewCookieStore([]byte(config.StoreSecretKey))
	userSession, err := store.Get(r, config.UserSessionKey)
	if err != nil {
		config.SendError(w, r, err.Error(), config.InvalidSession)
		return
	}

	vars := mux.Vars(r)
	eventid := vars["eventid"]

	req, err := http.NewRequest("UPDATE", config.Api+"pinevent/eventid="+eventid, nil)
	if err != nil {
		config.SendError(w, r, err.Error(), config.InvalidRequest)
		return
	}
	_, err = http.DefaultClient.Do(req)
	if err != nil {
		config.SendError(w, r, err.Error(), config.GetJSONError)
		return
	}	

	http.Redirect(w, r, "/"+config.DashByRole(userSession.Values["roleid"].(int)), 302)
}

func UnpinEventHandler(w http.ResponseWriter, r *http.Request) {
	if config.DEBUG {
		fmt.Println("Unpin handler")
	}

	//Creating error session in case of error occuring. If there is no error, session wil  be killed in the end
	store := sessions.NewCookieStore([]byte(config.StoreSecretKey))
	userSession, err := store.Get(r, config.UserSessionKey)
	if err != nil {
		config.SendError(w, r, err.Error(), config.InvalidSession)
		return
	}

	vars := mux.Vars(r)
	eventid := vars["eventid"]

	req, err := http.NewRequest("UPDATE", config.Api+"unpinevent/eventid="+eventid, nil)
	if err != nil {
		config.SendError(w, r, err.Error(), config.InvalidRequest)
		return
	}
	_, err = http.DefaultClient.Do(req)
	if err != nil {
		config.SendError(w, r, err.Error(), config.GetJSONError)
		return
	}

	http.Redirect(w, r, "/"+config.DashByRole(userSession.Values["roleid"].(int)), 302)
}
