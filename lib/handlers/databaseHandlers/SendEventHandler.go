package DBHandlers

import (
	//our
	"../../config"
//	"../../sessfunc"
	"../jsonHandlers"
	//customs
	"github.com/antigloss/go/logger"
	_ "github.com/go-sql-driver/mysql"
	"github.com/gorilla/sessions"
	//golang
	"io"
	"net/http"
	"os"
	"strconv"
	"time"
	"encoding/json"
	"bytes"
	"fmt"
)

//type 1 for event and 1 for broadcast

func SendEventHandler(w http.ResponseWriter, r *http.Request) {
	//Parse Form
	r.ParseMultipartForm(config.MaxSizePhoto_Byte)

	//Creating error session in case of error occuring. If there is no error, session wil  be killed in the end
	store := sessions.NewCookieStore([]byte(config.StoreSecretKey))
	userSession, err := store.Get(r, config.UserSessionKey)
	if err != nil {
		config.SendError(w, r, err.Error(), config.InvalidSession)
		return
	}

	if _, err := r.Form["eventtype"]; !err {
		config.SendError(w, r, "Empty eventtype field", config.InvalidParameters)
		return
	} else if _, err := r.Form["subject"]; !err {
		config.SendError(w, r, "Empty subject field", config.InvalidParameters)
		return
	} else if _, err := r.Form["body"]; !err {
		config.SendError(w, r, "Empty body field", config.InvalidParameters)
		return
	}

	eventType, _ := strconv.Atoi(r.Form["eventtype"][0])

	var eventInfo JSONHandlers.EventInfo
	eventInfo.FromID = userSession.Values["id"].(int)
	eventInfo.Body = r.Form["body"][0]
	eventInfo.Subject = r.Form["subject"][0]
	eventInfo.EventType = eventType
	eventInfo.Date = strconv.Itoa(time.Now().Year()) + "-" + strconv.Itoa(int(time.Now().Month())) + "-" + strconv.Itoa(time.Now().Day())

	if eventType == 1 {
		if _, err := r.Form["lector"]; !err {
			config.SendError(w, r, "Empty lector field", config.InvalidParameters)
			return
		}
		if _, err := r.Form["date"]; !err {
			config.SendError(w, r, "Empty date field", config.InvalidParameters)
			return
		}

		eventInfo.LectorID, _ = strconv.Atoi(r.Form["lector"][0])
		eventInfo.Date = r.Form["date"][0]
		
		photoURL := ""

		file, header, err := r.FormFile("photo")
		if err != nil {
			logger.Error("ERROR : ", err)
			photoURL = config.ProfilePhotoDir + "NoPhoto.jpeg"
			fmt.Println(photoURL)
		} else {
			defer file.Close()
			fmt.Println("Filename = ", header.Filename)
		}


		
		if photoURL == "" {
			photoURL = "images/" + time.Now().Format(time.RFC3339) + header.Filename

			//Create photo file
			f, err := os.OpenFile(config.HOMEDIR + "web/" + photoURL, os.O_WRONLY|os.O_CREATE, 0666)
			if err != nil {
				config.SendError(w, r, err.Error(), config.OpenFileError)
				return
			}

			defer f.Close()
			//Copy photo to file on server
			io.Copy(f, file)
		}

		eventInfo.PhotoURL = photoURL

	}

	//Encode eventInfo
	js, err := json.Marshal(eventInfo)
	if err != nil {
		config.SendError(w, r, err.Error(), config.EncodeJSONError)
		return
	}

	//Post JSON
	fmt.Println("Posting json...")
	reader := bytes.NewReader(js)
	_, err = http.Post(config.Api + "postevent", "application/json", reader)
	if err != nil {
		config.SendError(w, r, err.Error(), config.PostJSONError)
		return
	}

	fmt.Println("Redirecting to " + config.DashByRole(userSession.Values["roleid"].(int)))

	http.Redirect(w, r, "/"+config.DashByRole(userSession.Values["roleid"].(int)), 301)

	return
}