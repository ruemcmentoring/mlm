package DBHandlers

import (
	//golang
	"fmt"
	"io/ioutil"
	"net/smtp"
	//customs
	"github.com/antigloss/go/logger"
	//our
	"../../config"
)

func PersonalizeMessage(content string, RecieverName string, RoleID int, UniqueURL string) string {
	message := "Hello " + RecieverName + "\n"
	message += "Your role is " + GetRole(RoleID) + "\n"
	message += content + "\n"
	message += UniqueURL
	return message
}

func SendInviteEmail(RecieverAddress string, RecieverName string, RoleID int, UniqueURL string) error {
	logger.Init(config.LogDir, config.MaxLogFilesInLogDir, config.NumberOfFilesToDelete, config.MaxSizeLogFile_MB, false)

	fileContent, err := ioutil.ReadFile(config.MessageFile)
	if err != nil {
		if config.DEBUG {
			fmt.Println("ERROR: Cant read message template file!!!")
		}
		logger.Error("Cant read message template file: ", err)
	}

	from := config.EmailAddress
	pass := config.EmailPassword
	to := RecieverAddress

	message := PersonalizeMessage(string(fileContent), RecieverName, RoleID, UniqueURL)

	msg := "From: " + from + "\n" +
		"To: " + to + "\n" +
		"Subject: MLM registration link\n\n" +
		message

	fmt.Println(msg)
	err = smtp.SendMail("smtp.gmail.com:587",
		smtp.PlainAuth("", from, pass, "smtp.gmail.com"),
		from, []string{to}, []byte(msg))

	if err != nil {
		if config.DEBUG {
			fmt.Println("ERROR: smtp error: %s", err)
		}
		logger.Error("smtp error: %s", err)
	}

	return nil
}

func GetRole(RoleID int) string {
	var role string

	switch RoleID {
	case 0:
		role = "Admin"
	case 1:
		role = "Student"
	case 2:
		role = "Candidate"
	case 3:
		role = "Mentee"
	case 4:
		role = "Employee"
	case 5:
		role = "Lector"
	case 6:
		role = "Consultant"
	case 7:
		role = "Mentor"
	case 10:
		role = "inactive"
	default:
		role = "Unknown"
	}

	return role
}
