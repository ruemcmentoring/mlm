package DBHandlers

import (
	//our
	"../../config"
	"../jsonHandlers"
	//customs
	"github.com/antigloss/go/logger"
	_ "github.com/go-sql-driver/mysql"
	"github.com/gorilla/sessions"
	//golang
	"io"
	"net/http"
	"os"
	"strconv"
	"time"
	"encoding/json"
	"bytes"

	"fmt"
)

func UpdateApplicationHandler(w http.ResponseWriter, r *http.Request) {
	//Parse Form

	r.ParseMultipartForm(config.MaxSizePhoto_Byte)

	photoURL := ""

	if config.DEBUG {
		fmt.Println("Update Application Handler handling")
	}

	//Creating error session in case of error occuring. If there is no error, session wil  be killed in the end
	store := sessions.NewCookieStore([]byte(config.StoreSecretKey))
	userSession, err := store.Get(r, config.UserSessionKey)
	if err != nil {
		config.SendError(w, r, err.Error(), config.InvalidSession)
		return
	}

	//checking for empty fields
	if _, err := r.Form["id"]; !err {
		config.SendError(w, r, "Empty id field", config.InvalidParameters)
		return
	} else if _, err := r.Form["roleid"]; !err {
		config.SendError(w, r, "Empty roleid field", config.InvalidParameters)
		return
	} else if _, err := r.Form["name"]; !err {
		config.SendError(w, r, "Empty name field", config.InvalidParameters)
		return
	} else if _, err := r.Form["surname"]; !err {
		config.SendError(w, r, "Empty surname field", config.InvalidParameters)
		return
	} else if _, err := r.Form["sex"]; !err {
		config.SendError(w, r, "Empty sex field", config.InvalidParameters)
		return
	} else if _, err := r.Form["birthdate"]; !err {
		config.SendError(w, r, "Empty birthdate field", config.InvalidParameters)
		return
	} else if _, err := r.Form["phone"]; !err {
		config.SendError(w, r, "Empty phone field", config.InvalidParameters)
		return
	} else if _, err := r.Form["university"]; !err {
		config.SendError(w, r, "Empty university field", config.InvalidParameters)
		return
	} else if _, err := r.Form["job"]; !err {
		config.SendError(w, r, "Empty job field", config.InvalidParameters)
		return
	} else if _, err := r.Form["interests"]; !err {
		config.SendError(w, r, "Empty interests field", config.InvalidParameters)
		return
	} else if _, err := r.Form["hobbies"]; !err {
		config.SendError(w, r, "Empty hobbies field", config.InvalidParameters)
		return
	} else if _, err := r.Form["about"]; !err {
		config.SendError(w, r, "Empty about field", config.InvalidParameters)
		return

	} else if _, err := r.Form["newimage"]; !err {
		config.SendError(w, r, "Empty newimage field", config.InvalidParameters)
	} else if _, err := r.Form["isconfirmed"]; !err {
		config.SendError(w, r, "Empty isconfirmed field", config.InvalidParameters)


	} else if _, err := r.Form["editorid"]; !err {
		config.SendError(w, r, "Empty editorid field", config.InvalidParameters)
		return
	} else if _, err := r.Form["changerole"]; !err {
		config.SendError(w, r, "Empty changerole field", config.InvalidParameters)
		return
	}

	//Read from fields
	idstr := r.Form["id"][0]
	id, _ := strconv.Atoi(idstr)
	roleidstr := r.Form["roleid"][0]
	roleid, _ := strconv.Atoi(roleidstr)
	name := r.Form["name"][0]
	surname := r.Form["surname"][0]
	sex := r.Form["sex"][0]
	birthdate := r.Form["birthdate"][0]
	phone := r.Form["phone"][0]
	job := r.Form["job"][0]
	university := r.Form["university"][0]
	interests := r.Form["interests"][0]
	hobbies := r.Form["hobbies"][0]
	about := r.Form["about"][0]

	editoridstr := r.Form["editorid"][0]
	editorid, _ := strconv.Atoi(editoridstr)
	changerolestr := r.Form["changerole"][0]
	changerole, _ := strconv.Atoi(changerolestr)

	newimage, _ := strconv.Atoi(r.Form["newimage"][0])

	if newimage == 1 {
		//Get photo (or make it NoPhoto)
		file, _, err := r.FormFile("photo")
		if err != nil {
			logger.Error("ERROR : ", err)
			photoURL = config.HOMEDIR + "web/" + config.ProfilePhotoDir + "NoPhoto.jpeg"
		} else {
			defer file.Close()
		}

		//Get photo path
		year := time.Now().Year()
		pathPhoto := config.HOMEDIR + "web/" + config.ProfilePhotoDir + strconv.Itoa(year) + "-" + strconv.Itoa(year+1) + "/"

		//Create or choose directory
		dir := os.MkdirAll(pathPhoto, os.ModePerm)
		if dir != nil {
			logger.Info("Create new photo directory")
		}

		//It means photo was uploaded
		if photoURL == "" {
			fmt.Println("Photo uploading: ", pathPhoto + "ID" + idstr + ".jpeg")

			photoURL = pathPhoto + "ID" + idstr + ".jpeg"

			//Create photo file
			f, err := os.OpenFile(photoURL, os.O_WRONLY|os.O_CREATE, 0666)
			if err != nil {
				config.SendError(w, r, err.Error(), config.OpenFileError)
				return
			}

			defer f.Close()
			//Copy photo to file on server
			io.Copy(f, file)
		}

		photoURL = photoURL[len(config.HOMEDIR+"web/"):]
	} else {
		photoURL = ""
	}

	//Get Email
	email, err := JSONHandlers.GetEmail(id)
	if err != nil {
		config.SendError(w, r, err.Error(), config.SelectError)
		return
	}

	//create Application and User structs
	application := JSONHandlers.Application{id, interests, hobbies, about}
	user := JSONHandlers.User{id, name, surname, sex, photoURL, birthdate, university, phone, email, job}

	//Encode application
	js, err := json.Marshal(application)
	if err != nil {
		config.SendError(w, r, err.Error(), config.EncodeJSONError)
		return
	}

	//Post JSON
	reader := bytes.NewReader(js)
	_, err = http.Post(config.Api + "postapp", "application/json", reader)
	if err != nil {
		config.SendError(w, r, err.Error(), config.PostJSONError)
		return
	}

	//Encode user
	js, err = json.Marshal(user)
	if err != nil {
		config.SendError(w, r, err.Error(), config.EncodeJSONError)
		return
	}

	//Post JSON
	reader = bytes.NewReader(js)
	_, err = http.Post(config.Api + "postuser", "application/json", reader)
	if err != nil {
		config.SendError(w, r, err.Error(), config.PostJSONError)
		return
	}

	//if admin is editing
	if (editorid != id) {
		if (roleid != changerole) {
			roleid = changerole
			//change role
			JSONHandlers.UpdateProgramUserRole(config.ProgramID, id, roleid)
		}
	}

	if roleid == config.Mentor {
		if _, err := r.Form["prefuniversity"]; !err {
			config.SendError(w, r, "Empty prefuniversity field", config.InvalidParameters)
			return
		} else if _, err := r.Form["prefsex"]; !err {
			config.SendError(w, r, "Empty prefsex field", config.InvalidParameters)
			return
		} else if _, err := r.Form["prefinterests"]; !err {
			config.SendError(w, r, "Empty prefinterests field", config.InvalidParameters)
			return
		} else if _, err := r.Form["wish"]; !err {
			config.SendError(w, r, "Empty wish field", config.InvalidParameters)
			return
		} else if _, err := r.Form["twomentee"]; !err {
			config.SendError(w, r, "Empty twomentee field", config.InvalidParameters)
			return
		}

		prefuniversity := r.Form["prefuniversity"][0]
		prefsex := r.Form["prefsex"][0]
		prefinterests := r.Form["prefinterests"][0]
		wish := r.Form["wish"][0]
		twomentees := r.Form["twomentee"][0]
		twoMentees, _ := strconv.ParseBool(twomentees)

		//Create MentorApplication struct
		mentorApplication := JSONHandlers.MentorApplication{id, prefuniversity, prefsex, prefinterests, wish, twoMentees}

		//Encode JSON
		js, err = json.Marshal(mentorApplication)
		if err != nil {
			config.SendError(w, r, err.Error(), config.EncodeJSONError)
			return
		}

		//Post JSON
		reader = bytes.NewReader(js)
		_, err = http.Post(config.Api + "postmentorapp", "application/json", reader)
		if err != nil {
			config.SendError(w, r, err.Error(), config.PostJSONError)
			return
		}

	}

	fmt.Println(editorid, id)

	if (editorid != id) {
		fmt.Println("edited")
		http.Redirect(w, r, config.SearchPage, 301)
	} else {
		//Ok, now go to page with confirmEmail message
		userSession.Values["name"] = name
		userSession.Values["roleid"] = roleid
		userSession.Values["role"] = config.GetRoleString(roleid)
		sessions.Save(r, w)
		http.Redirect(w, r, config.DashByRole(userSession.Values["id"].(int)), 301)
	}

	return
}