package DBHandlers

import (
	//our
	"../../config"
	"../../sessfunc"
	"../jsonHandlers"
	//customs
	_ "github.com/go-sql-driver/mysql"
	"github.com/gorilla/sessions"
	//golang
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"
	"strconv"
)

func RegistrationHandler(w http.ResponseWriter, r *http.Request) {
	//Parse form
	r.ParseForm()

	if config.DEBUG {
		fmt.Println("Registration Handler")
	}

	//checking for empty fields
	if _, err := r.Form["pass"]; !err {
		config.SendError(w, r, "Empty password field", config.InvalidParameters)
		return
	} else if _, err = r.Form["name"]; !err {
		config.SendError(w, r, "Empty name field", config.InvalidParameters)
		return
	} else if _, err = r.Form["email"]; !err {
		config.SendError(w, r, "Empty email field", config.InvalidParameters)
		return
	} else if _, err = r.Form["role"]; !err {
		config.SendError(w, r, "Empty role field", config.InvalidParameters)
		return
	}

	//Reading from fields
	pass := r.Form["pass"][0]
	email := r.Form["email"][0]
	name := r.Form["name"][0]
	roleID, _ := strconv.Atoi(r.Form["role"][0])

	//Get UserID to know if already exists
	userID, err := JSONHandlers.GetUserID(email)
	if err != nil && err.Error() != "No user" {
		config.SendError(w, r, err.Error(), config.SelectError)
		return
	}
	if userID != -1 {
		config.SendError(w, r, "Email already registered. Check you email: "+email, config.AlreadyRegistered)
		return
	}

	//Create NewUser struct
	newUser := JSONHandlers.NewUser{email, name, roleID}

	//Encode JSON
	js, err := json.Marshal(newUser)
	if err != nil {
		config.SendError(w, r, err.Error(), config.EncodeJSONError)
		return
	}

	//Post JSON
	reader := bytes.NewReader(js)
	_, err = http.Post(config.Api+"postnewuser", "application/json", reader)
	if err != nil {
		config.SendError(w, r, err.Error(), config.PostJSONError)
		return
	}

	//Generate salt
	salt := GenerateSalt(10)
	hash := GetHashMD5(pass, salt)

	//Create Auth struct
	auth := JSONHandlers.Auth{userID, email, hash, salt, false}

	//Encode JSON
	js, err = json.Marshal(auth)
	if err != nil {
		config.SendError(w, r, err.Error(), config.EncodeJSONError)
		return
	}

	//Post JSON
	reader = bytes.NewReader(js)
	_, err = http.Post(config.Api+"postauth", "application/json", reader)
	if err != nil {
		config.SendError(w, r, err.Error(), config.PostJSONError)
		return
	}

	//Get userID (it exists cuz we created user a few moments ago)
	userID, err = JSONHandlers.GetUserID(email)
	if err != nil {
		config.SendError(w, r, err.Error(), config.InsertError)
		return
	}

	//Get user session
	store := sessions.NewCookieStore([]byte(config.StoreSecretKey))
	userSession, err := store.Get(r, config.UserSessionKey)
	if err != nil {
		config.SendError(w, r, err.Error(), config.InvalidSession)
		return
	}
	if !userSession.IsNew {
		config.SendError(w, r, "Already logged", config.AlreadyLogged)
		return
	}

	//Create new session by id, name, email and roleID. False stands for IsConfirmd
	sessfunc.CreateUserSession(userID, name, roleID, email, userSession)

	sessions.Save(r, w)

	//Well, now redirect to profile page
	/*	if roleID == config.Mentor {
			http.Redirect(w, r, config.MentorApplicationPage, 301)
			return
		} else {
			http.Redirect(w, r, config.CommonApplicationPage, 301)
			return
		}
	*/
	http.Redirect(w, r, config.ApplicationPage, 301)
	return
}
