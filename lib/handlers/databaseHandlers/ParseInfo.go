package DBHandlers

import (
	//golang
	"strings"
)

func Parse(info string) []string {
	info = strings.Replace(strings.ToLower(info)," ","",-1)
	info = strings.Replace(strings.ToLower(info),"\t","",-1)
	return strings.Split(info,",")
}