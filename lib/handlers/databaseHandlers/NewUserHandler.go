package DBHandlers

import (
	//our
	"../../config"
	"../jsonHandlers"
	//customs
	"github.com/antigloss/go/logger"
	_ "github.com/go-sql-driver/mysql"
//	"github.com/gorilla/sessions"
	//golang
	"io"
	"net/http"
	"os"
	"strconv"
	"time"
	"encoding/json"
	"bytes"
)

func NewUserHandler(w http.ResponseWriter, r *http.Request) {
	//Parse Form
	r.ParseMultipartForm(config.MaxSizePhoto_Byte)

	photoURL := ""
/*
	//Creating error session in case of error occuring. If there is no error, session wil  be killed in the end
	store := sessions.NewCookieStore([]byte(config.StoreSecretKey))
	userSession, err := store.Get(r, config.UserSessionKey)
	if err != nil {
		config.SendError(w, r, err.Error(), config.InvalidSession)
		return
	}
*/
	if _, err := r.Form["email"]; !err {
		config.SendError(w, r, "Empty email field", config.InvalidParameters)
		return
	} else if _, err := r.Form["password"]; !err {
		config.SendError(w, r, "Empty password field", config.InvalidParameters)
		return
	} else if _, err := r.Form["name"]; !err {
		config.SendError(w, r, "Empty name field", config.InvalidParameters)
		return
	} else if _, err := r.Form["surname"]; !err {
		config.SendError(w, r, "Empty surname field", config.InvalidParameters)
		return
	} else if _, err := r.Form["sex"]; !err {
		config.SendError(w, r, "Empty sex field", config.InvalidParameters)
		return
	} else if _, err := r.Form["birthdate"]; !err {
		config.SendError(w, r, "Empty birthdate field", config.InvalidParameters)
		return
	} else if _, err := r.Form["phone"]; !err {
		config.SendError(w, r, "Empty phone field", config.InvalidParameters)
		return
	} else if _, err := r.Form["job"]; !err {
		config.SendError(w, r, "Empty job field", config.InvalidParameters)
		return
	} else if _, err := r.Form["university"]; !err {
		config.SendError(w, r, "Empty university field", config.InvalidParameters)
		return
	} else if _, err := r.Form["roleselect"]; !err {
		config.SendError(w, r, "Empty role field", config.InvalidParameters)
		return
	}

	//Get photo (or make it NoPhoto)
	file, _, err := r.FormFile("photo")
	if err != nil {
		logger.Error("ERROR : ", err)
		photoURL = config.HOMEDIR + "web/" + config.ProfilePhotoDir + "NoPhoto.jpeg"
	} else {
		defer file.Close()
	}

	//Read from fields
	rolestr := r.Form["roleselect"][0]
	role, _ := strconv.Atoi(rolestr)
	email := r.Form["email"][0]
	pass := r.Form["password"][0]
	name := r.Form["name"][0]
	surname := r.Form["surname"][0]
	sex := r.Form["sex"][0]
	birthdate := r.Form["birthdate"][0]
	university := r.Form["university"][0]
	phone := r.Form["phone"][0]
	job := r.Form["job"][0]

	//REGISTRATION PART
	//Get UserID to know if already exists
	userID, err := JSONHandlers.GetUserID(email)
	if err != nil && err.Error() != "No user"  {
		config.SendError(w, r, err.Error(), config.SelectError)
		return
	}
	if userID != -1 {
		config.SendError(w, r, "Email already registered. Check your email: " + email, config.AlreadyRegistered)
		return
	}

	//Create NewUser struct
	newUser := JSONHandlers.NewUser{email, name, role}

	//Encode JSON
	js, err := json.Marshal(newUser)
	if err != nil {
		config.SendError(w, r, err.Error(), config.EncodeJSONError)
		return
	}

	//Post JSON
	reader := bytes.NewReader(js)
	_, err = http.Post(config.Api + "postnewuser", "application/json", reader)
	if err != nil {
		config.SendError(w, r, err.Error(), config.PostJSONError)
		return
	}

	//Generate salt
	salt := GenerateSalt(10)
	hash := GetHashMD5(pass, salt)

	//Create Auth struct
	auth := JSONHandlers.Auth{userID, email, hash, salt, false}

	//Encode JSON
	js, err = json.Marshal(auth)
	if err != nil {
		config.SendError(w, r, err.Error(), config.EncodeJSONError)
		return
	}

	//Post JSON
	reader = bytes.NewReader(js)
	_, err = http.Post(config.Api + "postauth", "application/json", reader)
	if err != nil {
		config.SendError(w, r, err.Error(), config.PostJSONError)
		return
	}

	//Get userID (it exists cuz we created user a few moments ago)
	userID, _ = JSONHandlers.GetUserID(email)


	//Get photo path
	year := time.Now().Year()
	pathPhoto := config.HOMEDIR + "web/" + config.ProfilePhotoDir + strconv.Itoa(year) + "-" + strconv.Itoa(year+1) + "/"

	//Create or choose directory
	dir := os.MkdirAll(pathPhoto, os.ModePerm)
	if dir != nil {
		logger.Info("Create new photo directory")
	}

	//It means photo was uploaded
	if photoURL == "" {
		photoURL = pathPhoto + "ID" + strconv.Itoa(userID) + ".jpeg"

		//Create photo file
		f, err := os.OpenFile(photoURL, os.O_WRONLY|os.O_CREATE, 0666)
		if err != nil {
			config.SendError(w, r, err.Error(), config.OpenFileError)
			return
		}

		defer f.Close()
		//Copy photo to file on server
		io.Copy(f, file)
	}

	photoURL = photoURL[len(config.HOMEDIR+"web/"):]

	user := JSONHandlers.User{userID, name, surname, sex, photoURL, birthdate, university, phone, email, job}

	//Encode user
	js, err = json.Marshal(user)
	if err != nil {
		config.SendError(w, r, err.Error(), config.EncodeJSONError)
		return
	}

	//Post JSON
	reader = bytes.NewReader(js)
	_, err = http.Post(config.Api + "postuser", "application/json", reader)
	if err != nil {
		config.SendError(w, r, err.Error(), config.PostJSONError)
		return
	}

	http.Redirect(w, r, config.DashByRole(config.Admin), 301)

	return
}