package DBHandlers

import (
	//our
	"../../config"
	"../jsonHandlers"
	//customs
	"github.com/antigloss/go/logger"
	_ "github.com/go-sql-driver/mysql"
	"github.com/gorilla/sessions"
//	"github.com/gorilla/sessions"
	//golang
	"net/http"
	"encoding/json"
	"bytes"
	"fmt"
)

func ChangePasswordHandler(w http.ResponseWriter, r *http.Request) {
	//Parse Form
	r.ParseForm()

	if _, err := r.Form["email"]; !err {
		config.SendError(w, r, "Empty email field", config.InvalidParameters)
		return
	} else if _, err := r.Form["oldpassword"]; !err {
		config.SendError(w, r, "Empty old password field", config.InvalidParameters)
		return
	} else if _, err := r.Form["newpassword"]; !err {
		config.SendError(w, r, "Empty new password field", config.InvalidParameters)
		return
	} else if _, err := r.Form["repeatpassword"]; !err {
		config.SendError(w, r, "Empty repeat password field", config.InvalidParameters)
		return
	}

	//Get session
	store := sessions.NewCookieStore([]byte(config.StoreSecretKey))
	userSession, err := store.Get(r, config.UserSessionKey)
	if err != nil {
		config.SendError(w, r, err.Error(), config.InvalidSession)
		return
	}

	//if logged in...
	if userSession.IsNew {
		http.Redirect(w, r, config.StartPage, 307)
		return
	}

	email := r.Form["email"][0]
	oldpass := r.Form["oldpassword"][0]
	newpass := r.Form["newpassword"][0]
	reppass := r.Form["repeatpassword"][0]

	if (newpass != reppass) {
		config.SendError(w, r, "New password and repeated password are different", config.InvalidPassword)
		return
	}

	//Take old auth

	//Get Auth struct via JSON
	jsonResp, err := http.Get(config.Api + "getauth/email=" + email)
	if err != nil {
		config.SendError(w, r, err.Error(), config.GetJSONError)
		return
	}

	//Decode JSON
	decoder := json.NewDecoder(jsonResp.Body)
	var auth JSONHandlers.Auth
	err = decoder.Decode(&auth)
	if err != nil {
		config.SendError(w, r, err.Error(), config.DecodeJSONError)
		return
	}

	if !auth.IsConfirmed {
		http.Redirect(w, r, config.AfterlogInfoPage, 307)
		return
	}

	//Generate hash
	oldhash := GetHashMD5(oldpass, auth.Salt)
	newhash := GetHashMD5(newpass, auth.Salt)

	fmt.Println("Got hash: " + auth.Hash + ", old: " + oldhash + ", new: ", newhash)

	if (oldhash != auth.Hash) {
		fmt.Println("password wrong")
		config.SendError(w, r, "Wrong password", config.InvalidPassword)
		return
	}

	fmt.Println("Ive got there!")

	auth.Hash = newhash

	//Encode JSON
	js, err := json.Marshal(auth)
	if err != nil {
		config.SendError(w, r, err.Error(), config.EncodeJSONError)
		return
	}

	//Post JSON
	reader := bytes.NewReader(js)
	_, err = http.Post(config.Api + "postauth", "application/json", reader)
	if err != nil {
		config.SendError(w, r, err.Error(), config.PostJSONError)
		return
	}

	logger.Info("Password of user with email = " + email + " was changed")

	http.Redirect(w, r, config.DashByRole(userSession.Values["roleid"].(int)), 301)

	return
}