package DBHandlers

import (
	//our
	"../../config"
	//customs
	"github.com/gorilla/sessions"
	//golang
	"fmt"
	"net/http"
)

func DeleteUserHandler(w http.ResponseWriter, r *http.Request) {
	r.ParseForm()

	if config.DEBUG {
		fmt.Println("Delete user Handler")
	}

	//Creating error session in case of error occuring. If there is no error, session wil  be killed in the end
	store := sessions.NewCookieStore([]byte(config.StoreSecretKey))
	userSession, err := store.Get(r, config.UserSessionKey)
	if err != nil {
		config.SendError(w, r, err.Error(), config.InvalidSession)
		return
	}

	if _, err := r.Form["userid"]; !err {
		config.SendError(w, r, "Empty eventid field", config.InvalidParameters)
		return
	}

	userID := r.Form["userid"][0]

	if userSession.Values["roleid"] == config.Admin {

		fmt.Println("Deleting user with ID = ", userID)

		req, err := http.NewRequest("DELETE", config.Api+"deleteuser/userid="+userID, nil)
		if err != nil {
			config.SendError(w, r, err.Error(), config.InvalidRequest)
			return
		}
		_, err = http.DefaultClient.Do(req)
		if err != nil {
			config.SendError(w, r, err.Error(), config.GetJSONError)
			return
		}
	}

	http.Redirect(w, r, "/"+config.SearchPage, 301)
}
