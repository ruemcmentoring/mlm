package DBHandlers

import (
	//our
	"../../config"
	//customs
	"github.com/antigloss/go/logger"
	_ "github.com/go-sql-driver/mysql"
	//golang
	"regexp"
	"database/sql"
)

//Checking email: length and pattern
func EmailIsOk(email string) bool {
	if len(email) > 30 {
		logger.Warn("WRONG : ", email, "Too long email, we are working on it :)")
		return false
	}

	if m, _ := regexp.MatchString(`^([\w\.\_]{2,20})@(\w{1,}).([a-z]{2,4})$`, email); !m {
		return false
	} else {
		return true
	}
}

//Checking engineer email - should be EmailIsOk and also @emc.com
func EmailIsOkForEngineer(email string) bool {
	if m, _ := regexp.MatchString(`^([\w\.\_]{2,20})@emc.com$`, email); !m {
		return false
	} else {
		return true
	}
}

func IsConfirmed(email string) (bool, error) {
	db, err := sql.Open(config.DBdriver, config.GetDBIdentify())
	if err != nil {
		return false, err
	}

	var confirmed bool

	rows, err := db.Query("SELECT IsConfirmed FROM Profile WHERE Email=?", email)
	if err != nil {
		return false, err
	}
	if rows.Next() {
		rows.Scan(&confirmed)
	}
	return confirmed, nil
}