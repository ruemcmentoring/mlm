package DBHandlers

import (
	//our
	"../../config"
	//golang
	"fmt"
)

const OurSecretRandomNumber = 92233720368547758

//200 symbols
const SecretAlphabet = "qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM1234567890QqWwEeRrTtYyUuIiOoPpAaSsDdFfGgHhJjKkLlZzXxCcVvBbNnMm11223344556677889900213243546576879800mnbvcxzlkjhgfdsapoiuytrewqMnBvCxZlKjHgFdSaPoIuYt"

func find(s string, c string) int {
	for i := 0; i < len(s); i += 2 {
		if c[0] == s[i] && c[1] == s[i+1] {
			return i
		}
	}
	return -2
}

func getPair(index int) (s string) {
	s = ""
	s += string(SecretAlphabet[2*index+1]) + string(SecretAlphabet[2*index])
	return
}

func reverse(s string) (ret string) {
	for i := len(s) - 1; i >= 0; i-- {
		ret += string(s[i])
	}
	return
}

func CreateUniqueLink(id int) (string, error) {
	var usefulID uint64 = uint64(id) * 21234

	if config.DEBUG {
		fmt.Println("clear id ", id, " useful ID ", usefulID)
	}

	usefulID ^= OurSecretRandomNumber

	if config.DEBUG {
		fmt.Println("XOR id ", usefulID)
	}

	URL := ""
	for usefulID != 0 {
		URL += getPair(int(usefulID % 100))
		usefulID /= 100
	}

	if config.DEBUG {
		fmt.Println(URL)
		fmt.Println("Decoded: ", DecodeUniqueLink(URL))
	}

	URL = config.Server + URL
	return URL, nil
}

func DecodeUniqueLink(link string) int {
	//link2 := link[len(link)-2*LenOfCode+2 : len(link)-LenOfCode+1]
	link = link[len(link)-config.LenOfCode:]
	link = reverse(link)

	if config.DEBUG {
		fmt.Println("To decode: ", link)
	}

	id := uint64(0)
	for i := 0; i < len(link); i += 2 {
		pair := string(link[i]) + string(link[i+1])
		id += uint64(find(SecretAlphabet, pair) / 2)
		id *= 100
	}
	id /= 100

	if config.DEBUG {
		fmt.Println("Decoded with xor: ", id)
		fmt.Println("Decoded without xor: ", id^OurSecretRandomNumber)
	}

	return int((id ^ OurSecretRandomNumber) / 21234)
}
