package DBHandlers
/*
import (
	//our
	"../../config"
	"../../DBQueries"
	//customs
	"github.com/antigloss/go/logger"
	_ "github.com/go-sql-driver/mysql"
	//golang
	"database/sql"
	"strings"
)

func FindUsersByFilter(search string, role int) ([]int, error) {

	//Searching
	//Opening database
	db, err := DBQueries.OpenDB()

	if err != nil {
		logger.Error("ERROR : ", err)
		return nil, err
	}

	var rows *sql.Rows

	if role != -1 {
		rows, err = DBQueries.GetRowsUsersByRole(role, config.ProgramID, db)
		if err != nil {
			return nil, err
		}
	} else {
		rows, err = DBQueries.GetRowsUsers(config.ProgramID, db)
		if err != nil {
			return nil, err
		}
	}

	//Map ID - Name. Filling in
	UserMap := make(map[int]string)

	for rows.Next() {
		var id int
		var name string
		rows.Scan(&id, &name)

		UserMap[id] = name
	}

	if search != "" {
		//Search for instances
		for id, name := range UserMap {
			if !strings.Contains(strings.ToLower(name), strings.ToLower(search)) {
				delete(UserMap, id)
			}
		}
	}

	IDs := make([]int, len(UserMap))
	iter := 0
	for key, _ := range UserMap {
		IDs[iter] = key
		iter++
	}

	return IDs, nil
}
*/