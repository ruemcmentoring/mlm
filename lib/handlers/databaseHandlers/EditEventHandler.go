package DBHandlers

import (
	//our
	"../../config"
//	"../../sessfunc"
	"../jsonHandlers"
	//customs
	"github.com/antigloss/go/logger"
	_ "github.com/go-sql-driver/mysql"
	"github.com/gorilla/sessions"
	//golang"
	"net/http"
	"strconv"
	"encoding/json"
	"bytes"
	"fmt"
	"time"
	"os"
	"io"
)

//type 1 for event and 1 for broadcast

func EditEventHandler(w http.ResponseWriter, r *http.Request) {
	//Parse Form

	r.ParseMultipartForm(config.MaxSizePhoto_Byte)

	//Creating error session in case of error occuring. If there is no error, session wil  be killed in the end
	store := sessions.NewCookieStore([]byte(config.StoreSecretKey))
	userSession, err := store.Get(r, config.UserSessionKey)
	if err != nil {
		config.SendError(w, r, err.Error(), config.InvalidSession)
		return
	}

	if _, err := r.Form["editeventid"]; !err {
		config.SendError(w, r, "Empty eventid field", config.InvalidParameters)
		return
	} else if _, err := r.Form["editeventtype"]; !err {
		config.SendError(w, r, "Empty eventtype field", config.InvalidParameters)
		return
	} else if _, err := r.Form["editsubject"]; !err {
		config.SendError(w, r, "Empty subject field", config.InvalidParameters)
		return
	} else if _, err := r.Form["editbody"]; !err {
		config.SendError(w, r, "Empty body field", config.InvalidParameters)
		return
	}

	eventType, _ := strconv.Atoi(r.Form["editeventtype"][0])
	eventID, _ := strconv.Atoi(r.Form["editeventid"][0])

	var eventInfo JSONHandlers.EventInfo
	eventInfo.EventID = eventID;
	eventInfo.Body = r.Form["editbody"][0]
	eventInfo.Subject = r.Form["editsubject"][0]
	eventInfo.EventType = eventType
	eventInfo.EventID = eventID;

	if eventType == 1 {
		if _, err := r.Form["editlector"]; !err {
			config.SendError(w, r, "Empty lector field", config.InvalidParameters)
			return
		}
		if _, err := r.Form["editdate"]; !err {
			config.SendError(w, r, "Empty date field", config.InvalidParameters)
			return
		}
		if _, err := r.Form["newimage"]; !err {
			config.SendError(w, r, "Empty newimage field", config.InvalidParameters)
			return
		}

		eventInfo.LectorID, _ = strconv.Atoi(r.Form["editlector"][0])
		eventInfo.Date = r.Form["editdate"][0]

		newimage, _ := strconv.Atoi(r.Form["newimage"][0])

		fmt.Println("New image? ", newimage)
		
		photoURL := ""

		if newimage == 1 {
			file, header, err := r.FormFile("editphoto")
			if err != nil {
				logger.Error("ERROR : ", err)
				photoURL = config.ProfilePhotoDir + "NoPhoto.jpeg"
				fmt.Println(photoURL)
			} else {
				defer file.Close()
				fmt.Println("Filename = ", header.Filename)
				photoURL = "images/" + time.Now().Format(time.RFC3339) + header.Filename

				//Create photo file
				f, err := os.OpenFile(config.HOMEDIR + "web/" + photoURL, os.O_WRONLY|os.O_CREATE, 0666)
				if err != nil {
					config.SendError(w, r, err.Error(), config.OpenFileError)
					return
				}

				defer f.Close()
				//Copy photo to file on server
				io.Copy(f, file)
			}

			eventInfo.PhotoURL = photoURL
		}

	}

	//Encode eventInfo
	js, err := json.Marshal(eventInfo)
	if err != nil {
		config.SendError(w, r, err.Error(), config.EncodeJSONError)
		return
	}

	//Post JSON
	fmt.Println("Posting json...")
	reader := bytes.NewReader(js)
	_, err = http.Post(config.Api + "updateevent", "application/json", reader)
	if err != nil {
		config.SendError(w, r, err.Error(), config.PostJSONError)
		return
	}

	fmt.Println("Redirecting to " + config.DashByRole(userSession.Values["roleid"].(int)))

	http.Redirect(w, r, "/"+config.DashByRole(userSession.Values["roleid"].(int)), 301)

	return
}