package DBHandlers

import (
	//our
	"../../config"
	//customs
	"github.com/gorilla/mux"
	//golang
	"fmt"
	"net/http"
)

func DeleteEventHandler(w http.ResponseWriter, r *http.Request) {
	if config.DEBUG {
		fmt.Println("Delete event Handler")
	}

	vars := mux.Vars(r)
	eventid := vars["eventid"]

	req, err := http.NewRequest("DELETE", config.Api+"deleteevent/eventid="+eventid, nil)
	if err != nil {
		config.SendError(w, r, err.Error(), config.InvalidRequest)
		return
	}
	_, err = http.DefaultClient.Do(req)
	if err != nil {
		config.SendError(w, r, err.Error(), config.GetJSONError)
		return
	}

	http.Redirect(w, r, "/"+config.DashByRole(0), 301)
}
