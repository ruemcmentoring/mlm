package DBHandlers

import (
	//our
	"../../config"
	"../../sessfunc"
	"../jsonHandlers"
	//customs
	_ "github.com/go-sql-driver/mysql"
	"github.com/gorilla/sessions"
	//golang
	"encoding/json"
	"fmt"
	"net/http"
	"strconv"
)

func LoginHandler(w http.ResponseWriter, r *http.Request) {
	//Parse form
	r.ParseForm()

	if config.DEBUG {
		fmt.Println("Login Handler")
	}

	//checking for empty fields
	ok := true
	if _, ok = r.Form["pass"]; !ok {
		config.SendError(w, r, "Empty password field", config.InvalidParameters)
		return
	} else if _, ok = r.Form["email"]; !ok {
		config.SendError(w, r, "Empty email field", config.InvalidParameters)
		return
	}

	//Reading from fields
	email := r.Form["email"][0]
	pass := r.Form["pass"][0]

	var isConfirmed bool

	//Get Auth struct via JSON
	jsonResp, err := http.Get(config.Api + "getauth/email=" + email)
	if err != nil {
		config.SendError(w, r, err.Error(), config.GetJSONError)
		return
	}

	//Decode JSON
	decoder := json.NewDecoder(jsonResp.Body)
	var auth JSONHandlers.Auth
	err = decoder.Decode(&auth)
	if err != nil {
		config.SendError(w, r, err.Error(), config.DecodeJSONError)
		return
	}


	var check string

	/*check fields*/
	//If user updated or closed application page then he has no conformation letter.
	//Lets check his application
	//Get Auth struct via JSON
	jsonResp, err = http.Get(config.Api + "checkapp/email=" + email)
	if err != nil {
		config.SendError(w, r, err.Error(), config.GetJSONError)
		return
	}

	//Decode JSON
	decoder = json.NewDecoder(jsonResp.Body)

	err = decoder.Decode(&check)
	if err != nil {
		config.SendError(w, r, err.Error(), config.DecodeJSONError)
		return
	}

	if &auth != nil {
		//check password hash
		if auth.Hash == GetHashMD5(pass, auth.Salt) {
			//Getting session
			store := sessions.NewCookieStore([]byte(config.StoreSecretKey))
			userSession, err := store.Get(r, config.UserSessionKey)
			if err != nil {
				config.SendError(w, r, err.Error(), config.InvalidSession)
				return
			}
			if !userSession.IsNew {
				config.SendError(w, r, "Already logged", config.AlreadyLogged)
				return
			}

			//Get UserInfo struct via JSON
			jsonResp, err = http.Get(config.Api + "getuserinfo/userid=" + strconv.Itoa(auth.UserID) + "&programid=" + strconv.Itoa(config.ProgramID))
			if err != nil {
				config.SendError(w, r, err.Error(), config.GetJSONError)
				return
			}

			//Decode JSON
			decoder = json.NewDecoder(jsonResp.Body)
			var userInfo JSONHandlers.UserInfo
			err = decoder.Decode(&userInfo)
			if err != nil {
				config.SendError(w, r, err.Error(), config.GetJSONError)
				return
			}

			sessfunc.CreateUserSession(auth.UserID, userInfo.Name, userInfo.RoleID, email, userSession)
			userSession.IsNew = false
			sessions.Save(r, w)

			//Soon we will need this var
			isConfirmed = auth.IsConfirmed

			sessions.Save(r, w)

		} else {
			config.SendError(w, r, "ERROR : "+"ID = "+strconv.Itoa(auth.UserID)+" Wrong password", config.InvalidPassword)
			return
		}
	} else {
		config.SendError(w, r, "ERROR : "+email+" - no such user registered", config.InvalidPassword)
		return
	}

	//Well, now redirecting
	if isConfirmed {
		if check == "true" {
			http.Redirect(w, r, "/", 301)
		} else {
			http.Redirect(w, r, config.ApplicationPage, 301)
		}
	} else {
		if check == "true" {
			http.Redirect(w, r, "/afterlog.html", 301)
		} else {
			http.Redirect(w, r, config.ApplicationPage, 301)
		}
	}
	//redirects are commented, because it is better to resend to login again, but need to redo view page handler and login.html - but too lazy now :)
}