package DBHandlers

import (
	//our
	"../../config"
	//customs
	"github.com/antigloss/go/logger"
	_ "github.com/go-sql-driver/mysql"
	"github.com/gorilla/mux"
	"github.com/gorilla/sessions"
	//golang
	"database/sql"
	"net/http"
	"strconv"
)

func ConfirmRegistrationHandler(w http.ResponseWriter, r *http.Request) {
	//Get vars
	vars := mux.Vars(r)

	//Decode id
	id := DecodeUniqueLink(vars["code"])

	//Confirm user
	err := ConfirmUser(id)
	if err != nil {
		config.SendError(w, r, err.Error(), config.UpdateError)
		return
	}

	store := sessions.NewCookieStore([]byte(config.StoreSecretKey))
	confirmSession, err := store.Get(r, config.ConfirmSessionKey)
	if err != nil {
		config.SendError(w, r, err.Error(), config.InvalidSession)
		return
	}
	confirmSession.Values["confirmed"] = true

	sessions.Save(r, w)

	http.Redirect(w, r, config.LoginPage, 307)
}

func ConfirmUser(id int) error {
	//Opening database
	db, err := sql.Open(config.DBdriver, config.GetDBIdentify())

	if err != nil {
		return err
	}

	logger.Info("Trying to update record for user " + strconv.Itoa(id))
	//haven't debugged yet...
	_, err = db.Query("UPDATE User SET IsConfirmed=? WHERE UserID=?", true, id)
	if err != nil {
		return err
	} else {
		logger.Info("Record successfully changed")
	}

	return nil
}
