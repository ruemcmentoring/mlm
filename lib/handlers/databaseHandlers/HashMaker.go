package DBHandlers

import (
	//golang
	"crypto/md5"
	"crypto/rand"
	"encoding/hex"
	"math/big"
)

const MD5Iteration = 5

const letterBytes = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"

func GetRandomInt(max *big.Int) int {
	i, _ := rand.Int(rand.Reader, max)
	index := int(i.Int64())
	return index
}

func RandStringBytes(n int) string {
	b := make([]byte, n)
	max := big.NewInt(int64(len(letterBytes) - 1))
	for i := range b {
		b[i] = letterBytes[GetRandomInt(max)]
	}
	return string(b)
}

func GetHashMD5(pass, salt string) string {
	hasher := md5.New()
	hash := pass + salt
	//making 5 times MD5
	for i := 0; i < MD5Iteration; i++ {
		hasher.Write([]byte(hash))
		hash = hex.EncodeToString(hasher.Sum(nil))
	}
	return hash
}

func GenerateSalt(n int) string {
	b := make([]byte, n)
	max := big.NewInt(int64(len(letterBytes) - 1))
	for i := range b {
		b[i] = letterBytes[GetRandomInt(max)]
	}
	return string(b)
}
