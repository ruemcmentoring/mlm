package Handlers

import (
	//our
	"../config"
	//customs
	//golang
	"net/http"
	"fmt"
)

func FormCancelHandler(w http.ResponseWriter, r *http.Request) {
	//Parse Form
	r.ParseMultipartForm(config.MaxSizePhoto_Byte)

	fmt.Println("Cancelling form from " + r.Referer())

	http.Redirect(w, r, r.Referer(), 301)

	return
}
