package Handlers

import (
	//our
	"../config"
	"../sessfunc"
	"./jsonHandlers"
	//customs
	"github.com/gorilla/sessions"
	//golang
	"fmt"
	"html/template"
	"net/http"
	"encoding/json"
)

func AfterlogViewHandler(w http.ResponseWriter, r *http.Request) {
	//Get title of page
	title := config.AfterlogInfoPage
	title = config.WebAppex + title

	if config.DEBUG {
		fmt.Println("AfterlogView Handler handling " + title)
	}

	//Get session
	store := sessions.NewCookieStore([]byte(config.StoreSecretKey))
	userSession, err := store.Get(r, config.UserSessionKey)
	if err != nil {
		config.SendError(w, r, err.Error(), config.InvalidSession)
		return
	}

	//if logged in...
	if !userSession.IsNew {
		//Get Auth struct via JSON
		jsonResp, err := http.Get(config.Api + "getauth/email=" + userSession.Values["email"].(string))
		if err != nil {
			config.SendError(w, r, err.Error(), config.GetJSONError)
			return
		}

		//Decode JSON
		decoder := json.NewDecoder(jsonResp.Body)
		var auth JSONHandlers.Auth
		err = decoder.Decode(&auth)
		if err != nil {
			config.SendError(w, r, err.Error(), config.DecodeJSONError)
			return
		}

		//If confirmed then afterlog is not accessable
		if auth.IsConfirmed {
			http.Redirect(w, r, config.ProfilePage, 307)
			return
		}

		//Parsing HTML
		t, err := template.ParseFiles(config.HOMEDIR + title)
		if err != nil { //here we have to send 404 error
			config.SendError(w, r, err.Error(), config.ParseTemplateError)
			return
		}

		//If Email is known, then name and id are known also
		if userSession.Values["email"] != nil {
			//Creating user variable
			user := struct {
				Email string
				Name  string
				ID    int
			}{
				userSession.Values["email"].(string),
				userSession.Values["name"].(string),
				userSession.Values["id"].(int),
			}
			//Deleting (actually, marking) - unlogging
			sessfunc.KillSession(userSession)

			sessions.Save(r, w)

			//Execution
			t.Execute(w, user)

			return
		}
	}

	//else delete session and redirect to start page

	//Deleting (actually, marking)
	sessfunc.KillSession(userSession)

	sessions.Save(r, w)

	http.Redirect(w, r, config.StartPage, 307)

	return
}
