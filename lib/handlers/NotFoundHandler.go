package Handlers

import (
	//our
	"../config"
	//customs
	"github.com/antigloss/go/logger"
	//golang
	"fmt"
	"net/http"
)

func NotFoundHandler(w http.ResponseWriter, r *http.Request) {
	//title has to be 404.html
	title := config.Page404
	title = config.WebAppex + title

	if config.DEBUG {
		fmt.Println("Comes from " + r.Referer() + " " + r.RequestURI)
		fmt.Println("NotFound Handler handling " + title)
	}

	logger.Info("Sending... ", title)

	r, err := http.NewRequest("GET", config.Page404, nil)
	if err != nil {
		fmt.Println(err)
	}

	//load page
	p, err := loadPage(title)
	if err != nil {
		logger.Error("ERROR : ", err.Error())
		p, _ := loadPage(config.Page404)
		fmt.Fprintf(w, "%s", p.body) //wired way to send HTML page
		return                       //just return because of printing dry p.body
	}

	//serve it
	http.ServeFile(w, r, p.dir)
	logger.Info(p.dir, " has been sent")
}
