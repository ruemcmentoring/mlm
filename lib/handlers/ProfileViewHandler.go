package Handlers

import (
	//our
	"../config"
	"./jsonHandlers"
	//customs
//	"./databaseHandlers"
	"github.com/antigloss/go/logger"
	"github.com/gorilla/sessions"
	//golang
	"fmt"
	"html/template"
	"net/http"
	"strconv"
	"encoding/json"
)

func ProfileViewHandler(w http.ResponseWriter, r *http.Request) {
	//Get title of page
	title := r.URL.Path[1:] //[1:] - is to delete "/"
	if title == "" {
		title = config.StartPage
	}
	//id := DBHandlers.DecodeUniqueLink(title)
	id, err := strconv.Atoi(title[3:])
	title = config.WebAppex + title



	if config.DEBUG {
		fmt.Println("ProfileView Handler handling " + title + " id is")
		fmt.Println(id)
	}

	//Getting session
	store := sessions.NewCookieStore([]byte(config.StoreSecretKey))
	userSession, err := store.Get(r, config.UserSessionKey)
	if err != nil {
		config.SendError(w, r, err.Error(), config.InvalidSession)
		return
	}

	//If logged...
	if !userSession.IsNew {
		if config.DEBUG {
			fmt.Println("ProfileView Handler Old session " + title)
		}

		//Get UserInfo struct via JSON
		jsonResp, err := http.Get(config.Api + "getuserinfo/userid=" + strconv.Itoa(userSession.Values["id"].(int)) + "&programid=" + strconv.Itoa(config.ProgramID))
		if err != nil {
			config.SendError(w, r, err.Error(), config.GetJSONError)
			return
		}

		//Decode JSON
		decoder := json.NewDecoder(jsonResp.Body)
		var userInfo JSONHandlers.UserInfo
		err = decoder.Decode(&userInfo)
		if err != nil {
			config.SendError(w, r, err.Error(), config.DecodeJSONError)
			return
		}

		if &userInfo == nil {
			config.SendError(w, r, "No User Info found", config.SelectError)
			return
		}

		//Parsing HTML (do it earlier - there are 2 variants of executing)
		t, err := template.ParseFiles(config.HOMEDIR + config.WebAppex + config.ProfilePage)
		if err != nil { //here we have to send 404 error
			config.SendError(w, r, err.Error(), config.ParseTemplateError)
			return
		}

		//Creating user variable
		if id == userSession.Values["id"].(int) { //user wants his profile page
			user := struct {	//user wants his profile
				Email      string
				Name       string
				RoleStr    string
				PgLink     string
				ImgLink    string
				Phone      string
				BD         string
				Univer     string
				PI         string
				Hobbies    string
				About      string
				Visibility string
			}{
				userInfo.Email,
				userInfo.Name,
				userInfo.RoleStr,
				title,
				userInfo.PhotoURL,
				userInfo.Phone,
				userInfo.BD,
				userInfo.University,
				userInfo.PI,
				userInfo.Hobbies,
				userInfo.About,
				config.NoneVis,
			}
			//Take template t, insert it to w, using user as list of variables there
			t.Execute(w, user)
		} else { //user wants other profile page
			user := struct {
				Email      string
				Name       string
				RoleStr    string
				PgLink     string
				ImgLink    string
				Visibility string
			}{
				userInfo.Email,
				userInfo.Name,
				userInfo.RoleStr,
				title,
				userInfo.PhotoURL,
				config.HiddenVis,
			}
			//Take template t, insert it to w, using user as list of variables there
			t.Execute(w, user)
		}

	//Else redirect to blocker
	} else {
		if config.DEBUG {
			fmt.Println("ProfileView Handler New session " + title)
		}
		//Redirecting to page with login request
		http.Redirect(w, r, config.ViewBlockPage, 307)
		logger.Info("Trying to view profile unlogged. Redirecting... ")
		return
	}
}
