package Handlers

import (
	//our
	"../config"
	//customs
	"github.com/antigloss/go/logger"
	//golang
	"fmt"
	"net/http"
)

func FileHandler(w http.ResponseWriter, r *http.Request) {
	//get page title
	title := r.URL.Path[1:] //[1:] - is to delete "/"
	title = config.WebAppex + title
	logger.Info("Sending... ", title)

	if config.DEBUG {
		fmt.Println("File Handler handling " + title)
	}

	//load it
	p, err := loadPage(title)

	if err != nil {
		config.SendError(w, r, err.Error(), config.OpenFileError)
	}
	//Serve
	http.ServeFile(w, r, p.dir)
	logger.Info(p.dir, " has been sent")

	return
}
