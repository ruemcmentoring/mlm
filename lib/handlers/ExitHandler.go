package Handlers

import (
	//our
	"../config"
	"../sessfunc"
	//customs
	"github.com/antigloss/go/logger"
	"github.com/gorilla/sessions"
	//golang
	"fmt"
	"net/http"
)

func ExitHandler(w http.ResponseWriter, r *http.Request) {
	if config.DEBUG {
		fmt.Println("Exit Handler")
	}

	//Get session
	store := sessions.NewCookieStore([]byte(config.StoreSecretKey))
	userSession, err := store.Get(r, config.UserSessionKey)
	if err != nil {
		logger.Error("ERROR : ", err)
	}

	//Deleting (actually, marking)
	sessfunc.KillSession(userSession)

	sessions.Save(r, w)

	//redirect to start page
	http.Redirect(w, r, config.StartPage, 307)
}
