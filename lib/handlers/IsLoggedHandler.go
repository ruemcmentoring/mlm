package Handlers

import (
	//our
	"../config"
	//customs
	"github.com/gorilla/sessions"
	//golang
	"net/http"
)

func IsLoggedHandler(w http.ResponseWriter, r *http.Request) {
	//Get session
	store := sessions.NewCookieStore([]byte(config.StoreSecretKey))
	userSession, err := store.Get(r, config.UserSessionKey)
	if err != nil {
		config.SendError(w, r, err.Error(), config.InvalidSession)
		return
	}

	//If logged...
	if !userSession.IsNew {
		w.Write([]byte("Logged. Session is old"))
	} else {
		w.Write([]byte("Not logged. Session is new"))
	}
}
