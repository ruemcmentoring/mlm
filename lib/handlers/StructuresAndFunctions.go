package Handlers

import (
	//our
	"../config"
	//customs
	"github.com/antigloss/go/logger"
	//golang
	"io/ioutil"
)

//page structure
type page struct {
	dir  string
	body []byte
}

//error structure (has to be normally rewritten)
type Err struct {
	Message string
}

//Function loading page by title to *page
func loadPage(title string) (*page, error) {
	logger.Info("Trying to load", title)
	//writing from title to body
	body, err := ioutil.ReadFile(title)
	if err != nil {
		return nil, err
	}

	return &page{dir: config.HOMEDIR + title, body: body}, nil
}