package Handlers

import (
	//our
	"../config"
//	"./databaseHandlers"
	//customs
	"github.com/gorilla/sessions"
	//golang
	"fmt"
	"net/http"
)

func MyProfileViewHandler(w http.ResponseWriter, r *http.Request) {
	if config.DEBUG {
		fmt.Println("MyProfileView Handler")
	}

	//Get session
	store := sessions.NewCookieStore([]byte(config.StoreSecretKey))
	userSession, err := store.Get(r, config.UserSessionKey)
	if err != nil {
		config.SendError(w, r, err.Error(), config.InvalidSession)
		return
	}

	//If logged...
	if !userSession.IsNew {

		http.Redirect(w, r, "/admindash.html", 307)
		return
		/*
		//Get ID
		id := userSession.Values["id"].(int)

		//Get title
		title, _ := DBHandlers.CreateUniqueLink(id)
		title = title[len(title)-config.LenOfCode:]

		if config.DEBUG {
			fmt.Println("ProfileView Handler Old session")
		}

		//Redirectto profile page
		http.Redirect(w, r, "id/"+title, 307)
		*/

	//if not logged then redirect to blocker
	} else {
		if config.DEBUG {
			fmt.Println("ProfileView Handler no account")
		}
		//Redirecting to page with login request
		http.Redirect(w, r, config.ViewBlockPage, 307)
		return
	}
}
