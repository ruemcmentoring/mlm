package Handlers

import (
	//our
	"../config"
	"./jsonHandlers"
	//customs
	"github.com/gorilla/sessions"
	//golang
	"encoding/json"
	"fmt"
	"html/template"
	"net/http"
	"strconv"
)

func DashViewHandler(w http.ResponseWriter, r *http.Request) {
	//Get title of page
	title := r.URL.Path[1:] //[1:] - is to delete "/"
	title = config.WebAppex + title

	if config.DEBUG {
		fmt.Println("DashView Handler handling " + title)
	}

	//Get session
	store := sessions.NewCookieStore([]byte(config.StoreSecretKey))
	userSession, err := store.Get(r, config.UserSessionKey)
	if err != nil {
		config.SendError(w, r, err.Error(), config.InvalidSession)
		return
	}

	//if logged in...
	if !userSession.IsNew {
		//Get Auth struct via JSON
		jsonResp, err := http.Get(config.Api + "getauth/email=" + userSession.Values["email"].(string))
		if err != nil {
			config.SendError(w, r, err.Error(), config.GetJSONError)
			return
		}

		//Decode JSON
		decoder := json.NewDecoder(jsonResp.Body)
		var auth JSONHandlers.Auth
		err = decoder.Decode(&auth)
		if err != nil {
			config.SendError(w, r, err.Error(), config.DecodeJSONError)
			return
		}

		auth.IsConfirmed = true
		if !auth.IsConfirmed {
			http.Redirect(w, r, config.AfterlogInfoPage, 307)
			return
		}

		//Else if not logged in...
	} else {
		http.Redirect(w, r, config.StartPage, 307)
		return
	}

	//Parsing HTML
	var paths []string
	/**
	TODO: this looks pretty sheety, so it seems, we need to take this hardcode from config...
		  but may be it gonna be better to calm down
	*/
	paths = []string{
			config.HOMEDIR + "web/dash.html",
			config.HOMEDIR + "web/dash_profile.html",
			config.HOMEDIR + "web/dash_timeline.html",
			config.HOMEDIR + "web/dash_addmodal.html",
			config.HOMEDIR + "web/dash_editmodal.html",
			config.HOMEDIR + "web/dash_controlmodal.html",
			config.HOMEDIR + "web/dash_admincontrolmodal.html",
			config.HOMEDIR + "web/dash_foot.html",
		}

	if config.DEBUG {
		fmt.Println(paths)
	}
	t, err := template.ParseFiles(paths...)
	if err != nil { //here we have to send 404 error
		config.SendError(w, r, err.Error(), config.ParseTemplateError)
		return
	}

	//Get UserInfo struct via JSON
	jsonResp, err := http.Get(config.Api + "getuserinfo/userid=" + strconv.Itoa(userSession.Values["id"].(int)) + "&programid=" + strconv.Itoa(config.ProgramID))
	if err != nil {
		config.SendError(w, r, err.Error(), config.GetJSONError)
		return
	}

	//Decode JSON
	decoder := json.NewDecoder(jsonResp.Body)
	var userInfo JSONHandlers.UserInfo
	err = decoder.Decode(&userInfo)
	if err != nil {
		config.SendError(w, r, err.Error(), config.DecodeJSONError)
		return
	}

	//Get RolePeople struct via JSON
	jsonResp, err = http.Get(config.Api + "getrolepeople/roleid=" + strconv.Itoa(config.Lector))
	if err != nil {
		config.SendError(w, r, err.Error(), config.GetJSONError)
		return
	}

	//Decode JSON
	decoder = json.NewDecoder(jsonResp.Body)
	var rolePeople JSONHandlers.RolePeople
	err = decoder.Decode(&rolePeople)
	if err != nil {
		config.SendError(w, r, err.Error(), config.DecodeJSONError)
		return
	}

	//Get Events struct via JSON
	jsonResp, err = http.Get(config.Api + "getevents/programid=" + strconv.Itoa(config.ProgramID))
	if err != nil {
		config.SendError(w, r, err.Error(), config.GetJSONError)
		return
	}

	//Decode JSON
	decoder = json.NewDecoder(jsonResp.Body)
	var events []*JSONHandlers.EventInfo
	err = decoder.Decode(&events)
	if err != nil {
		config.SendError(w, r, err.Error(), config.DecodeJSONError)
		return
	}

	//Get Roles struct via JSON
	jsonResp, err = http.Get(config.Api + "getroles")
	if err != nil {
		config.SendError(w, r, err.Error(), config.GetJSONError)
		return
	}

	//Decode JSON
	decoder = json.NewDecoder(jsonResp.Body)
	var roles []*JSONHandlers.RoleInfo
	err = decoder.Decode(&roles)
	if err != nil {
		config.SendError(w, r, err.Error(), config.DecodeJSONError)
		return
	}

	content := struct {
		User    JSONHandlers.UserInfo
		Events  []*JSONHandlers.EventInfo
		Lectors JSONHandlers.RolePeople
		Roles   []*JSONHandlers.RoleInfo
		ProgramStarted bool
	}{
		userInfo,
		events,
		rolePeople,
		roles,
		true,
	}

	//Deleting (actually, marking)
	sessions.Save(r, w)

	//Execution
	t.Execute(w, content)

	return
}
