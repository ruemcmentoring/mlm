package sessfunc

import (
	//customs
	"github.com/gorilla/sessions"
	//our
	"../config"
)

//Help function to create user session by id, name, role and session (actually, its Options.IsNew should be true)
//name and roleID for unknown are "" and -1
func CreateUserSession(id int, name string, roleID int, email string, Session *sessions.Session) {
	if Session == nil {
		return
	}

	if name == "" {
		Session.Values["name"] = "Unknown"
	} else {
		Session.Values["name"] = name
	}

	Session.Values["roleid"] = roleID

	Session.Values["role"] = config.GetRoleString(roleID)

	Session.Values["id"] = id

	Session.Values["email"] = email
}

func KillSession(Session *sessions.Session) {
	if Session != nil {
		Session.Options.MaxAge = -1
	}
}
