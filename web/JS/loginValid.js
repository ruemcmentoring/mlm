$(document).ready(function() {
	document.getElementById('button').disabled = true;
	var validPass = false
	var validEmail = false

    $('input[name=pass]').keyup(function() {
		var pswd = $(this).val();

		//validate the length
		if (pswd.length < 6) {
   	 		$('#length').removeClass('valid').addClass('invalid');
   	 		validPass = false
		} else {
    		$('#length').removeClass('invalid').addClass('valid');
    		validPass = true
		}

		$('input[name=email]').keyup();

		if (validEmail && validPass) {
			document.getElementById('button').disabled = false;
		} else {
			document.getElementById('button').disabled = true;
		}

	}).focus(function() {
		$(this).keyup();
	}).blur(function() {
		$(this).keyup();
	});

	$('input[name=email]').keyup(function() {
		var email = $(this).val();

		if (email.match(/^[a-zA-Z0-9._%+-]+@/)) {
    		$('#emailWarn').removeClass('invalid').addClass('valid');
    		validEmail = true
		} else {
    		$('#emailWarn').removeClass('valid').addClass('invalid');
    		validEmail = false
 		}

 		if (validEmail && validPass) {
			document.getElementById('button').disabled = false;
		} else {
			document.getElementById('button').disabled = true;
		}

	}).focus(function() {
		$(this).keyup();
	}).blur(function() {
		$(this).keyup();
	});
});