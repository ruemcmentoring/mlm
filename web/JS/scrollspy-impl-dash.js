$(document).ready(function() {
	$('.timeline-inverted').on('scrollSpy:enter', function() {
		if ( !($(this).css('display') == "none") ) {
			console.log('enter:', $(this).attr('id'));
			var nextid = $(this).attr('id');
			nextid = parseInt(nextid.substring(18))+1;
			$('#timeline-inverted-'+nextid).show(1000);
			$('#timeline-inverted-'+(nextid+1)).show(1000);
		}
	});
/*
	$('.timeline-inverted').on('scrollSpy:exit', function() {
		console.log('enter:', $(this).attr('id'));
	});
*/

	$('.timeline-inverted').scrollSpy();
});
