function readURL(input, imgid) {

	if (input.files && input.files[0]) {
		var reader = new FileReader();

		reader.onload = function(e)	{
			$(imgid).attr('src',e.target.result);
		}

		reader.readAsDataURL(input.files[0]);
	}
};

function readURLedited(input, imgid, newimgid) {

	if (input.files && input.files[0]) {
		var reader = new FileReader();

		reader.onload = function(e)	{
			$(imgid).attr('src',e.target.result);
		}

		reader.readAsDataURL(input.files[0]);

		$("#newimageid").val(1);
	} else {
		$("#newigameid").val(0);
	}
}
