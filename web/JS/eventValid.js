$(document).ready(function() {

	document.getElementById('sendbroadcast').disabled = true;
	document.getElementById('sendevent').disabled = true;

	$('#eventform .necessaryfield').keyup(function() {
        var eventempty = false;
        $('#eventform .necessaryfield').each(function() {
           	if ($(this).val() == '') {
               	eventempty = true;
           	}
        });

        if (eventempty) {
            $('#sendevent').attr('disabled', 'disabled'); // updated according to http://stackoverflow.com/questions/7637790/how-to-remove-disabled-attribute-with-jquery-ie
        } else {
            $('#sendevent').removeAttr('disabled'); // updated according to http://stackoverflow.com/questions/7637790/how-to-remove-disabled-attribute-with-jquery-ie
        }
    });
    $('#eventform .necessaryfield').change(function() {
        var eventempty = false;
        $('#eventform .necessaryfield').each(function() {
            if ($(this).val() == '') {
                eventempty = true;
            }
        });

        if (eventempty) {
            $('#sendevent').attr('disabled', 'disabled'); // updated according to http://stackoverflow.com/questions/7637790/how-to-remove-disabled-attribute-with-jquery-ie
        } else {
            $('#sendevent').removeAttr('disabled'); // updated according to http://stackoverflow.com/questions/7637790/how-to-remove-disabled-attribute-with-jquery-ie
        }
    });





	$('#broadcastform .necessaryfield').keyup(function() {
       	var broadcastempty = false;
       	$('#broadcastform .necessaryfield').each(function() {
           	if ($(this).val() == '') {
              	broadcastempty = true;
         	}
        });

        if (broadcastempty) {
            $('#sendbroadcast').attr('disabled', 'disabled'); // updated according to http://stackoverflow.com/questions/7637790/how-to-remove-disabled-attribute-with-jquery-ie
        } else {
            $('#sendbroadcast').removeAttr('disabled'); // updated according to http://stackoverflow.com/questions/7637790/how-to-remove-disabled-attribute-with-jquery-ie
        }
    });
    $('#broadcastform .necessaryfield').change(function() {
        var broadcastempty = false;
        $('#broadcastform .necessaryfield').each(function() {
            if ($(this).val() == '') {
                broadcastempty = true;
            }
        });

        if (broadcastempty) {
            $('#sendbroadcast').attr('disabled', 'disabled'); // updated according to http://stackoverflow.com/questions/7637790/how-to-remove-disabled-attribute-with-jquery-ie
        } else {
            $('#sendbroadcast').removeAttr('disabled'); // updated according to http://stackoverflow.com/questions/7637790/how-to-remove-disabled-attribute-with-jquery-ie
        }
    });
});
