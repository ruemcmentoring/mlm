$(document).ready(function() {

	if ( $("#isconfirmed").val() == "true" )
		document.getElementById('button').disabled = false;
	else
		document.getElementById('button').disabled = true;


	$("select[name='changerole']").change(function() {
		if ($(this).val() == 7) {
			$(".mentor_application_form").show();
		} else {
			$(".mentor_application_form").hide();
		}
	});



	$('#application .necessaryfield').keyup(function() {
        	var empty = false;

		var field_selector;
		if ($("select[name='changerole']").val() == 7) {
			field_selector = "#application .necessaryfield";
		} else {
			field_selector = ".common_application_form .necessaryfield";
		}

        	$(field_selector).each(function() {
           		if ($(this).val() == '') {
               			empty = true;
           		}
        	});

       		if (empty) {
            		$('#button').attr('disabled', 'disabled'); // updated according to http://stackoverflow.com/questions/7637790/how-to-remove-disabled-attribute-with-jquery-ie
        	} else {
            		$('#button').removeAttr('disabled'); // updated according to http://stackoverflow.com/questions/7637790/how-to-remove-disabled-attribute-with-jquery-ie
        	}
    	});

    	$('#application .necessaryfield').change(function() {
        	var empty = false;

        	var field_selector;
		if ($("select[name='changerole']").val() == 7) {
			field_selector = "#application .necessaryfield";
		} else {
			field_selector = ".common_application_form .necessaryfield";
		}

        	$(field_selector).each(function() {
           		if ($(this).val() == '') {
               			empty = true;
           		}
        	});

        	if (empty) {
            		$('#button').attr('disabled', 'disabled'); // updated according to http://stackoverflow.com/questions/7637790/how-to-remove-disabled-attribute-with-jquery-ie
        	} else {
            		$('#button').removeAttr('disabled'); // updated according to http://stackoverflow.com/questions/7637790/how-to-remove-disabled-attribute-with-jquery-ie
        	}
    	});
});
