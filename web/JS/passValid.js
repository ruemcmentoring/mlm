$(document).ready(function() {
	document.getElementById('button').disabled = true;
	var validPass = false
	var validEmail = false
	var validName = false

	//password
    $('input[name=pass]').keyup(function() {
		var pswd = $(this).val();
		validPass = true

		//validate the length
		if (pswd.length < 6) {
   	 		$('#length').removeClass('valid').addClass('invalid');
   	 		validPass = false
		} else {
    		$('#length').removeClass('invalid').addClass('valid');
		}

		//validate letter
		if (pswd.match(/[a-z]/) || pswd.match(/[A-Z]/)) {
    		$('#letter').removeClass('invalid').addClass('valid');
		} else {
    		$('#letter').removeClass('valid').addClass('invalid');
    		validPass = false
		}

		//validate number
		if ( pswd.match(/\d/) ) {
		    $('#number').removeClass('invalid').addClass('valid');
		} else {
 		   $('#number').removeClass('valid').addClass('invalid');
 		   validPass = false
		}

		//email
		var email = $('input[type=text]').val();

		if (email.match(/^[a-zA-Z0-9._%+-]+@emc.com$/)) {
    		$('#emailWarn').removeClass('invalid').addClass('valid');
    		validEmail = true
		} else {
    		$('#emailWarn').removeClass('valid').addClass('invalid');
    		validEmail = false
 		}

 		if (validEmail && validPass && $('input[name=name]').val()) {
			document.getElementById('button').disabled = false;
		} else {
			document.getElementById('button').disabled = true;
		}

	}).focus(function() {
    	$('#pswd_info').fadeIn(700);
    	$(this).keyup();
	}).blur(function() {
    	$('#pswd_info').fadeOut(700);
    	$(this).keyup();
	});

	//email
	$('input[name=email]').keyup(function() {
		var email = $(this).val();

		if (email.match(/^[a-zA-Z0-9._%+-]+@emc.com$/)) {
    		$('#emailWarn').removeClass('invalid').addClass('valid');
    		validEmail = true
		} else {
    		$('#emailWarn').removeClass('valid').addClass('invalid');
    		validEmail = false
 		}

 		if (validEmail && validPass && $('input[name=name]').val()) {
			document.getElementById('button').disabled = false;
		} else {
			document.getElementById('button').disabled = true;
		}
	}).focus(function() {
    	$('#email_info').fadeIn(700);
    	$(this).keyup();
	}).blur(function() {
    	$('#email_info').fadeOut(700);
    	$(this).keyup();
	});

	//name
	$('input[name=name]').keyup(function() {
		if ($(this).val()) {
			validName = true
			$('#nameWarn').removeClass('invalid').addClass('valid');
		} else {
			validName = false
			$('#nameWarn').removeClass('valid').addClass('invalid');
		}

		if (validEmail && validPass && $('input[name=name]').val()) {
			document.getElementById('button').disabled = false;
		} else {
			document.getElementById('button').disabled = true;
		}

	}).focus(function() {
    	$('#name_info').fadeIn(700);
    	$(this).keyup();
	}).blur(function() {
    	$('#name_info').fadeOut(700);
    	$(this).keyup();
	});

});