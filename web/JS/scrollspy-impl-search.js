$(document).ready(function() {
	$('.search-result').on('scrollSpy:enter', function() {
		if ($("#have_searched").val() == "0") {
			if ( !($(this).css('display') == "none") ) {
				console.log('enter:', $(this).attr('id'));
				var nextid = $(this).attr('id');
				nextid = parseInt(nextid.substring(14))+1;
				$('#search-result-'+nextid).show(800);
				$('#search-result-'+(nextid+1)).show(800);
				$('#search-result-'+(nextid+2)).show(800);
			}
		}
	});

	$('.search-result').scrollSpy();
});
