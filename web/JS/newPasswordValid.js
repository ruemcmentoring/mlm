$(document).ready(function() {
	document.getElementById('sendchangepassword').disabled = true;
	var validPassOld = false
	var validPassNew = false
	var validPassRep = false
	var samePass = false

	$('input[name=oldpassword]').keyup(function() {
		var pswd = $(this).val();

		validPassOld = true;

		//validate the length
		if ( pswd.length < 6 ) {
   	 		validPassOld = false
		}

		//validate letter
		if ( !(pswd.match(/[a-z]/) || pswd.match(/[A-Z]/)) ) {
			validPassOld = false
		}

		//validate number
		if ( !(pswd.match(/\d/)) ) {
 			validPassOld = false
		}

		console.log(validPassOld, validPassNew, validPassRep, samePass)

		if (validPassOld && validPassNew && validPassRep && samePass) {
			document.getElementById('sendchangepassword').disabled = false;
		} else {
			document.getElementById('sendchangepassword').disabled = true;
		}

	}).focus(function() {
		$(this).keyup();
	}).blur(function() {
		$(this).keyup();
	});

	$('input[name=newpassword]').keyup(function() {
		var pswd = $(this).val();

		validPassNew = true;
		samePass = true;

		//validate the length
		if ( pswd.length < 6 ) {
   	 		validPassNew = false
		}

		//validate letter
		if ( !(pswd.match(/[a-z]/) || pswd.match(/[A-Z]/)) ) {
			validPassNew = false
		}

		//validate number
		if ( !(pswd.match(/\d/)) ) {
 			validPassNew = false
		}

		if ( !($('input[name=repeatpassword]').val() == pswd) ) {
			samePass = false
		}

		console.log(validPassOld, validPassNew, validPassRep, samePass)

		if (validPassOld && validPassNew && validPassRep && samePass) {
			document.getElementById('sendchangepassword').disabled = false;
		} else {
			document.getElementById('sendchangepassword').disabled = true;
		}

	}).focus(function() {
		$(this).keyup();
	}).blur(function() {
		$(this).keyup();
	});

	$('input[name=repeatpassword]').keyup(function() {
		var pswd = $(this).val();

		validPassRep = true;
		samePass = true;

		//validate the length
		if ( pswd.length < 6 ) {
   	 		validPassRep = false
		}

		//validate letter
		if ( !(pswd.match(/[a-z]/) || pswd.match(/[A-Z]/)) ) {
			validPassRep = false;
		}

		//validate number
		if ( !(pswd.match(/\d/)) ) {
 			validPassRep = false
		}

		if ( !($('input[name=newpassword]').val() == pswd) ) {
			samePass = false
		}

		console.log(validPassOld, validPassNew, validPassRep, samePass)

		if (validPassOld && validPassNew && validPassRep && samePass) {
			document.getElementById('sendchangepassword').disabled = false;
		} else {
			document.getElementById('sendchangepassword').disabled = true;
		}

	}).focus(function() {
		$(this).keyup();
	}).blur(function() {
		$(this).keyup();
	});


});
