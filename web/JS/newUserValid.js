$(document).ready(function() {
	document.getElementById('newuserbutton').disabled = true;
	var validPass = false
	var validEmail = false
	var empty = true;

    $('input[name=password]').keyup(function() {
		var pswd = $(this).val();

		validPass = true;

		//validate the length
		if (pswd.length < 6) {
   	 		$('#length').removeClass('valid').addClass('invalid');
   	 		validPass = false
		} else {
    		$('#length').removeClass('invalid').addClass('valid');
		}

		//validate letter
		if (pswd.match(/[a-z]/) || pswd.match(/[A-Z]/)) {
    		$('#letter').removeClass('invalid').addClass('valid');
		} else {
    		$('#letter').removeClass('valid').addClass('invalid');
    		validPass = false
		}

		//validate number
		if ( pswd.match(/\d/) ) {
		    $('#number').removeClass('invalid').addClass('valid');
		} else {
 		   $('#number').removeClass('valid').addClass('invalid');
 		   validPass = false
		}

		$('input[name=email]').keyup();

		if (validEmail && validPass && !empty) {
			document.getElementById('newuserbutton').disabled = false;
		} else {
			document.getElementById('newuserbutton').disabled = true;
		}

	}).focus(function() {
		$(this).keyup();
	}).blur(function() {
		$(this).keyup();
	});

	$('input[name=email]').keyup(function() {
		var email = $(this).val();

		if (email.match(/^[a-zA-Z0-9._%+-]+@/)) {
    		$('#emailWarn').removeClass('invalid').addClass('valid');
    		validEmail = true
		} else {
    		$('#emailWarn').removeClass('valid').addClass('invalid');
    		validEmail = false
 		}

 		if (validEmail && validPass && !empty) {
			document.getElementById('newuserbutton').disabled = false;
		} else {
			document.getElementById('newuserbutton').disabled = true;
		}

	}).focus(function() {
		$(this).keyup();
	}).blur(function() {
		$(this).keyup();
	});

	$('#newuserform .necessaryfield').keyup(function() {
        $('#application .necessaryfield').each(function() {
           	if ($(this).val() == '') {
               	empty = true;
           	}
        });

        if (validEmail && validPass && !empty) {
			document.getElementById('newuserbutton').disabled = false;
		} else {
			document.getElementById('newuserbutton').disabled = true;
		}

        empty = false;
    });
    $('#newuserform .necessaryfield').change(function() {
        $('#application .necessaryfield').each(function() {
           	if ($(this).val() == '') {
               	empty = true;
           	}
        });

        if (validEmail && validPass && !empty) {
			document.getElementById('newuserbutton').disabled = false;
		} else {
			document.getElementById('newuserbutton').disabled = true;
		}

        empty = false;
    });
});