$(document).ready(function() {

	$(".editbroadcastforJS").click(function() {
        var timeline_panel = $(this).parent().parent().parent().parent();
        var eventid = $(timeline_panel.children('input')[0]).val();
        var subject = $("#subject"+eventid).text();
        var body = $("#body"+eventid).html();
        $("#editbroadcastid").val(eventid);
        $("#editbroadcastsubject").val(subject);
        $("#editbroadcastbody").html(body);

	$('textarea').each(function() {
		$(this).html($(this).text().split("<br>").join("\n"));
	});

        console.log(eventid);
        console.log(subject);
        console.log(body);
    });

    $(".editeventforJS").click(function() {
        var timeline_panel = $(this).parent().parent().parent().parent();
        var eventid = $(timeline_panel.children('input')[0]).val();
        var subject = $("#subject"+eventid).text();
        var body = $("#body"+eventid).html();
        var date = $("#rawdate"+eventid).val();
        var imgurl = $("#photourl"+eventid).attr("src");
        var lectorid = $("#lectorid"+eventid).val();
//        var attachments = $("#attachments"+eventid);
        /*get files from attachments*/
        
        $("#editeventid").val(eventid);
        $("#editeventsubject").val(subject);
        $("#editeventbody").html(body);
        $("#editeventdatepicker").val(date);
        $("#editeventimage").attr("src", imgurl);
        $("#editeventlector").val(lectorid);
        $("#newimageid").val(0);

	$('textarea').each(function() {
		$(this).html($(this).text().split("<br>").join("\n"));
	});

        console.log(eventid);
        console.log(subject);
        console.log(body);
        console.log(date);
        console.log(imgurl);
        console.log(lectorid);
    });

});
