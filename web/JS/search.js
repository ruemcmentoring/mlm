$(document).ready(function() {

	$("#search_button").click(function() {
		var counter = 0;

		var substring = $("#search_input").val();

		console.log("Search for: ", substring);

		var filter_selector = $("#role-dropdown").find("li");
		var selected = false;
		
		//searching for everything
		if (substring == "") {
			$('.search-result').each(function() {
				counter++;
				if (counter < 5) {
					var role_str = $(this).find(".role-class-for-search").text();
					var ok = false;

					filter_selector.each(function() {
						if ($(this).find("input").is(":checked")) {
							selected = true;
							if ($(this).find("a").text().indexOf(role_str) != -1) {
								ok = true;
								console.log("Found role: " + role_str);
							}
						}
					});
					if (ok || !selected) {
						counter++;
						$(this).show();
					} else {
						$(this).hide();
					}
				} else {
					$(this).hide();
				}
			});
			$("#results_counter").text("All");
			$("#search_substring").text("empty string");
			$("#have_searched").val(0);
		//searching for substring
		} else {
			$('.search-result').each(function() {
				var text = $(this).find('h3 > a').text();
				
				if (text.toLowerCase().indexOf(substring.toLowerCase()) != -1) {
					var role_str = $(this).find(".role-class-for-search").text();
					var ok = false;

					filter_selector.each(function() {
						if ($(this).find("input").is(":checked")) {
							selected = true;
							if ($(this).find("a").text().indexOf(role_str) != -1) {
								ok = true;
								console.log("Found role: " + role_str);
							}
						}
					});
					if (ok || !selected) {
						counter++;
						$(this).show();
					} else {
						$(this).hide();
					}
				} else {
					$(this).hide();
				}
			});
			$("#results_counter").text(counter);
			$("#search_substring").text(substring);
			$("#have_searched").val(1);
		}	

		counter = 0;

    });
});
