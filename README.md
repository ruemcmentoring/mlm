# Mentoring Lifecycle Managment System

Here is some information about how to run it

## Database
1. ./databaseInsertUtilities/CreateDataBase.bash  
2. if you want some test data: ./databaseInsertUtilities/testInserts.bash  
3. if you want to insert student: ./databaseInsertUtilities/InsertStudentsInDB.bash

## Application
1. Make sure you are having Golang 1.5.3 or higher  
2. go run server.go  
3. PROFIT!!! Now enter the browser and visit http://localhost:8080